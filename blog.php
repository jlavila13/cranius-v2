<?php /* Template name: Blog */ ?>
<?php get_header();?>
    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
    <div class="fixed-background" style="background-image: url(<?php print IMAGES; ?>assets/blog-cover.png);">
        <div class="banner-shortcode">
            <div class="banner-frame border-image b-frame" style="border-image-source: url(<?php print IMAGES; ?>/assets/frame-rojo.jpg);"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="align">
                            <h1 class="montserrat-regular texto-blanco">Conoce<p class="roboto-black">Nuestro Blog</p></h1>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container" id="blog">

      <div class="empty-space col-xs-b40 col-sm-b80"></div>
<!--       <?php if( have_posts() ) : while( have_posts() ) : the_post();?>
        <div class="row">
            <div class="col-md-12 text-center">
                <article class="sa">
                    <h3><?php the_title(); ?></h3>
                    <p><?php the_content(); ?></p>
                </article>
                <div class="empty-space col-xs-b35 col-sm-b70"></div>
            </div>
        </div> -->

        <div class="row">
            <div class="col-sm-9 col-xs-b30 col-sm-b0">
            <?php
              $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
              $blogpost = array("post_type" => "post", "paged" => $paged);
              $args = new WP_Query( $blogpost );
            ?>
            <?php if( $args -> have_posts() ) : while( $args -> have_posts() ) : $args -> the_post();?>
                <div class="blog-landing-entry">
                    <a class="blog-preview mouseover-1" href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail();?>
                    </a>
                    <div class="row vertical-aligned-columns">
                        <div class="col-sm-3 text-center">
                            <div class="h3 blog-day"><?php echo get_the_date( 'd' ); ?></div>
                            <div class="h6 blog-date"><?php echo get_the_date( 'M-y' ); ?></div>
                            <div class="sa xsmall grey">Por <?php the_author();?><br/> en <?php $cat = get_the_category(); echo $cat[0]->cat_name; ?></div>
                        </div>
                        <div class="col-sm-9">
                            <div class="sa small grey blog-data"></div>
                            <div class="h4 blog-title"><span class="ht-2"><a href="#"><?php the_title(); ?></a></span></div>
                            <div class="sa middle blog-description"><?php the_excerpt(); ?></div>
                            <a class="button" href="<?php the_permalink();?>">Leer más</a>
                        </div>
                    </div>
                </div>
            <?php endwhile; endif; wp_reset_query(); ?>
                <div class="empty-space col-xs-b15 col-sm-b30"></div>
                <div class="pager">
                <?php pagination_bar(); ?>

                    <!-- <a href="#">Prev</a>
                    <a href="#">1</a>
                    <a href="#">2</a>
                    <a href="#" class="active">3</a>
                    <a href="#">4</a>
                    <a href="#">5</a>
                    <a href="#">Next</a> -->
                </div>
            </div>
            <div class="col-sm-3">
                <?php dynamic_sidebar('main'); ?>
            </div>
        </div>

        <?php endwhile; endif; ?>
      <div class="empty-space col-xs-b45 col-sm-b90"></div>
    </div>
<?php get_footer();?>