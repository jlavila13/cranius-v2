<?php /*Template name: servicios*/ ?>
<?php /*Template name: servicios*/ ?>
<?php get_header(); ?>
  <?php if(have_posts() ) : while(have_posts() ) : the_post(); ?>
  <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
  <div class="fixed-background" style="background-image: url(<?php print IMAGES; ?>assets/blog-cover.png);">
      <div class="banner-shortcode">
          <div class="banner-frame border-image" style="border-image-source: url(<?php print IMAGES; ?>assets/frame-rojo.jpg);"></div>
          <div class="container">
              <div class="row">
                  <div class="col-md-12">
                      <div class="align">
                          <h1 class="montserrat-regular texto-blanco">Nuestro <p class="roboto-black">Blog</p></h1>
                          
                      </div>
                      
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="container">
      <div class="empty-space col-xs-b45 col-sm-b90"></div>
      <div class="row">
                <div class="col-sm-9 col-xs-b30 col-sm-b0">
                    <h2 class="h4 col-xs-b15 p-title"><?php the_title(); ?> </h2>
                    <div class="row col-xs-b15">
                        <div class="col-sm-6">
                            <div class="sa xsmall grey"><?php echo get_the_date( 'd-M-y' ); ?> by <?php the_author(); ?></div>
                        </div>
                        <div class="col-sm-6 col-sm-text-right">
                            <!-- <div class="sa small grey blog-data"><i class="fa fa-comment-o" aria-hidden="true"></i></div> -->
                        </div>
                    </div>
                    <div class="sa p-content">
                        <?php the_content(); ?>
                    </div>
                    <div class="empty-space col-xs-b30"></div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-b30 col-sm-b0">
                            <div class="tags-wrapper">
                                <div class="title h6">Tags</div>
                                <?php 
                                    $tags = get_tags(array(
                                    'hide_empty' => false
                                    ));
                                    echo '<ul>';
                                    foreach ($tags as $tag) {
                                    echo '<a>' . $tag->name . '</a>';
                                    }
                                    echo '</ul>';
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-6 col-sm-text-right">
                            <!-- <div class="follow style-1">
                                <div class="title h6">Share</div>
                                <a class="entry" href="https://www.instagram.com/somoscranius" target="_blank"><i class="fa fa-instagram"></i></a>
                                <a class="entry" href="https://www.facebook.com/somoscranius" target="_blank"><i class="fa fa-facebook"></i></a>
                                <a class="entry" href="https://twitter.com/somoscranius" target="_blank"><i class="fa fa-twitter"></i></a>
                                <a class="entry" href="https://plus.google.com/somoscranius" target="_blank"><i class="fa fa-google-plus"></i></a>
                            </div> -->
                            <?php echo do_shortcode( '[social_share]' ); ?>
                        </div>
                    </div>
                    <div class="empty-space col-xs-b45 col-sm-b90"></div>
                    <h4 class="h4 blog-column-title">Artículos Relacionados</h4>
                    <div class="row">
                    <?php
                        //for use in the loop, list 5 post titles related to first tag on current post
                        $tags = wp_get_post_tags($post->ID);
                        if ($tags) {
                        $first_tag = $tags[0]->term_id;
                        $args=array(
                        'tag__in' => array($first_tag),
                        'post__not_in' => array($post->ID),
                        'posts_per_page'=>5,
                        'caller_get_posts'=>1
                        );
                        $my_query = new WP_Query($args);
                        if( $my_query->have_posts() ) {
                        while ($my_query->have_posts()) : $my_query->the_post();
                    ?>
                    <div class="col-sm-6">
                        <div class="blog-small-entry size-2">
                            <a class="blog-small-preview mouseover-1" href="#">
                                <?php the_post_thumbnail(); ?> 
                                <?php the_post_thumbnail(); ?> 
                                
                            </a>
                            <div class="h6 blog-small-title"><span class="ht-2"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span></div>
                            <div class="row col-xs-b5">
                                <div class="col-sm-8">
                                    <div class="sa xsmall grey blog-small-data"><?php echo get_the_date( 'd-M-y' );?> by <?php the_author(); ?></div>  
                                </div>
                                <div class="col-sm-4 col-sm-text-right">
                                    <!-- <div class="sa small grey blog-data"><i class="fa fa-heart-o" aria-hidden="true"></i> 9 <i class="fa fa-comment-o" aria-hidden="true"></i> 5</div> -->
                                </div>
                            </div>
                            <div class="sa middle blog-small-description"> <?php the_content(); ?> </div>
                            <a class="button" href="<?php the_permalink();?>">Leer más</a>
                        </div>
                    </div>
                    <?php endwhile; } wp_reset_query();}?>
                    </div>
                    <div class="empty-space col-xs-b45 col-sm-b90"></div>
                  
                </div>
                <div class="col-sm-3">
                  <?php dynamic_sidebar('main');?>
                </div>
            </div>

  </div>

  <?php endwhile; endif;?>
<?php get_footer(); ?>
