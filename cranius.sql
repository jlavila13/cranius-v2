-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-01-2018 a las 21:46:48
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cranius`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2017-09-14 13:04:44', '2017-09-14 13:04:44', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/cranius', 'yes'),
(2, 'home', 'http://localhost/cranius', 'yes'),
(3, 'blogname', 'cranius', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'jose.leon.avila@hotmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:132:{s:7:\"post/?$\";s:26:\"index.php?post_type=slides\";s:37:\"post/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=slides&feed=$matches[1]\";s:32:\"post/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=slides&feed=$matches[1]\";s:24:\"post/page/([0-9]{1,})/?$\";s:44:\"index.php?post_type=slides&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:36:\"miembros/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"miembros/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"miembros/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"miembros/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"miembros/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"miembros/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"miembros/([^/]+)/embed/?$\";s:41:\"index.php?miembros=$matches[1]&embed=true\";s:29:\"miembros/([^/]+)/trackback/?$\";s:35:\"index.php?miembros=$matches[1]&tb=1\";s:49:\"miembros/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?miembros=$matches[1]&feed=$matches[2]\";s:44:\"miembros/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?miembros=$matches[1]&feed=$matches[2]\";s:37:\"miembros/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?miembros=$matches[1]&paged=$matches[2]\";s:44:\"miembros/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?miembros=$matches[1]&cpage=$matches[2]\";s:33:\"miembros/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?miembros=$matches[1]&page=$matches[2]\";s:25:\"miembros/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"miembros/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"miembros/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"miembros/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"miembros/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"miembros/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"slides/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"slides/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"slides/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"slides/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"slides/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"slides/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:23:\"slides/([^/]+)/embed/?$\";s:39:\"index.php?slides=$matches[1]&embed=true\";s:27:\"slides/([^/]+)/trackback/?$\";s:33:\"index.php?slides=$matches[1]&tb=1\";s:47:\"slides/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?slides=$matches[1]&feed=$matches[2]\";s:42:\"slides/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?slides=$matches[1]&feed=$matches[2]\";s:35:\"slides/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?slides=$matches[1]&paged=$matches[2]\";s:42:\"slides/([^/]+)/comment-page-([0-9]{1,})/?$\";s:46:\"index.php?slides=$matches[1]&cpage=$matches[2]\";s:31:\"slides/([^/]+)(?:/([0-9]+))?/?$\";s:45:\"index.php?slides=$matches[1]&page=$matches[2]\";s:23:\"slides/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:33:\"slides/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:53:\"slides/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"slides/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"slides/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:29:\"slides/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=2&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:3:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:36:\"contact-form-7/wp-contact-form-7.php\";i:2;s:31:\"wp-google-maps/wpGoogleMaps.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'cranius-v2', 'yes'),
(41, 'stylesheet', 'cranius-v2', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '2', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'cron', 'a:5:{i:1516410285;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1516453671;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1516466725;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1516480163;a:1:{s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(109, 'nonce_key', 'kVlF{!%!R`crg5Z1U{~a&3X0bTAhTjV(AqQ4ZgBY[c>QtvmW>q.232<{ArNu3)am', 'no'),
(110, 'nonce_salt', '2~A`3<b;LwWrFNUd~0(1X4k>#Yu{lS ?&Qwz0sFwxvC>}M}#Zm9^J>bvK`&NR>jR', 'no'),
(111, 'theme_mods_twentyseventeen', 'a:3:{s:18:\"custom_css_post_id\";i:-1;s:18:\"nav_menu_locations\";a:1:{s:3:\"top\";i:2;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1516321217;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(122, 'auth_key', 'zWW/9(Ktpf7%)wQ9BJAl49~4IYT;-#YG*+*dPw$DQjh/e+gd]Q+Wv/39=Y`QZt0P', 'no'),
(123, 'auth_salt', '81B0ME{?=CKzV/W2j/KCGBj]K:R4!;(r=p;2j!x!t~*+~YE;-kWEXV%g2|tZxR#A', 'no'),
(124, 'logged_in_key', 'J3tc2O<x*[D&pc4jefW:zsD%lc@U%5B67GS5qFZo=>QXhA2!7:i,;J^:;*Q.|(;(', 'no'),
(125, 'logged_in_salt', '+;$+~&Y4<9R3N]YG/g0Sv[lx1g^;(!7<OnbVXD97kpZ3F6/FucdMPYR1JGJ)1!w=', 'no'),
(131, 'can_compress_scripts', '1', 'no'),
(145, 'current_theme', 'Cranius theme', 'yes'),
(146, 'theme_mods_cranius-theme', 'a:4:{i:0;b:0;s:18:\"custom_css_post_id\";i:-1;s:18:\"nav_menu_locations\";a:1:{s:8:\"main-nav\";i:2;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1516218947;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:7:\"f-area1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-1\";a:0:{}}}}', 'yes'),
(147, 'theme_switched', '', 'yes'),
(152, 'recently_activated', 'a:2:{s:41:\"advanced-custom-fields-pro-master/acf.php\";i:1516221143;s:31:\"wp-google-maps/wpGoogleMaps.php\";i:1516221143;}', 'yes'),
(155, 'acf_version', '5.6.7', 'yes'),
(177, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"4.9.2\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1505489936;s:7:\"version\";s:3:\"4.9\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(178, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(229, 'wpgmza_temp_api', 'AIzaSyChPphumyabdfggISDNBuGOlGVBgEvZnGE', 'yes'),
(230, 'wpgmza_xml_location', '{uploads_dir}/wp-google-maps/', 'yes'),
(231, 'wpgmza_xml_url', '{uploads_url}/wp-google-maps/', 'yes'),
(232, 'wpgmza_db_version', '6.4.09', 'yes'),
(233, 'wpgmaps_current_version', '6.4.09', 'yes'),
(234, 'widget_wpgmza_map_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(235, 'WPGMZA_OTHER_SETTINGS', 'a:1:{s:27:\"wpgmza_settings_marker_pull\";s:1:\"0\";}', 'yes'),
(236, 'WPGMZA_FIRST_TIME', '6.4.09', 'yes'),
(256, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:27:\"jose.leon.avila@hotmail.com\";s:7:\"version\";s:5:\"4.8.5\";s:9:\"timestamp\";i:1516218674;}', 'no'),
(299, '_site_transient_timeout_browser_b876c8fd7fc402e60530b64622320f7a', '1516823723', 'no'),
(300, '_site_transient_browser_b876c8fd7fc402e60530b64622320f7a', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"63.0.3239.132\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(314, 'theme_mods_cranius-theme_old', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:8:\"main-nav\";i:2;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1516221115;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:7:\"f-area1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-1\";a:0:{}}}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(315, 'theme_mods_cranius-themev2', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:8:\"main-nav\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1516320918;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(319, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(321, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.2\";s:7:\"version\";s:5:\"4.9.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1516369642;s:15:\"version_checked\";s:5:\"4.9.2\";s:12:\"translations\";a:0:{}}', 'no'),
(327, '_transient_timeout_plugin_slugs', '1516460646', 'no'),
(328, '_transient_plugin_slugs', 'a:7:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:19:\"akismet/akismet.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:33:\"google-maps-ve/google-maps-ve.php\";i:4;s:9:\"hello.php\";i:5;s:31:\"wp-google-maps/wpGoogleMaps.php\";i:6;s:31:\"zajax-ajax-navigation/zajax.php\";}', 'no'),
(338, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1516373645;s:7:\"checked\";a:7:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.6.7\";s:19:\"akismet/akismet.php\";s:5:\"4.0.2\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"4.9.2\";s:33:\"google-maps-ve/google-maps-ve.php\";s:5:\"1.0.2\";s:9:\"hello.php\";s:3:\"1.6\";s:31:\"wp-google-maps/wpGoogleMaps.php\";s:6:\"6.4.09\";s:31:\"zajax-ajax-navigation/zajax.php\";s:3:\"0.4\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:6:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.0.2\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.0.2.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:7:\"default\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";s:7:\"default\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"4.9.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.4.9.2.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:7:\"default\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";}s:7:\"banners\";a:3:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";s:7:\"default\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"google-maps-ve/google-maps-ve.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/google-maps-ve\";s:4:\"slug\";s:14:\"google-maps-ve\";s:6:\"plugin\";s:33:\"google-maps-ve/google-maps-ve.php\";s:11:\"new_version\";s:5:\"1.0.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/google-maps-ve/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/google-maps-ve.zip\";s:5:\"icons\";a:2:{s:2:\"1x\";s:67:\"https://ps.w.org/google-maps-ve/assets/icon-128x128.png?rev=1129800\";s:7:\"default\";s:67:\"https://ps.w.org/google-maps-ve/assets/icon-128x128.png?rev=1129800\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:69:\"https://ps.w.org/google-maps-ve/assets/banner-772x250.png?rev=1129789\";s:7:\"default\";s:69:\"https://ps.w.org/google-maps-ve/assets/banner-772x250.png?rev=1129789\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:7:\"default\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";s:7:\"default\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}s:31:\"wp-google-maps/wpGoogleMaps.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/wp-google-maps\";s:4:\"slug\";s:14:\"wp-google-maps\";s:6:\"plugin\";s:31:\"wp-google-maps/wpGoogleMaps.php\";s:11:\"new_version\";s:6:\"6.4.09\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/wp-google-maps/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/wp-google-maps.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:66:\"https://ps.w.org/wp-google-maps/assets/icon-128x128.png?rev=970398\";s:2:\"2x\";s:66:\"https://ps.w.org/wp-google-maps/assets/icon-256x256.png?rev=970398\";s:7:\"default\";s:66:\"https://ps.w.org/wp-google-maps/assets/icon-256x256.png?rev=970398\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:68:\"https://ps.w.org/wp-google-maps/assets/banner-772x250.jpg?rev=792293\";s:7:\"default\";s:68:\"https://ps.w.org/wp-google-maps/assets/banner-772x250.jpg?rev=792293\";}s:11:\"banners_rtl\";a:0:{}}s:31:\"zajax-ajax-navigation/zajax.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:35:\"w.org/plugins/zajax-ajax-navigation\";s:4:\"slug\";s:21:\"zajax-ajax-navigation\";s:6:\"plugin\";s:31:\"zajax-ajax-navigation/zajax.php\";s:11:\"new_version\";s:3:\"0.4\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/zajax-ajax-navigation/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/zajax-ajax-navigation.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:74:\"https://ps.w.org/zajax-ajax-navigation/assets/icon-128x128.jpg?rev=1309662\";s:2:\"2x\";s:74:\"https://ps.w.org/zajax-ajax-navigation/assets/icon-256x256.jpg?rev=1309660\";s:7:\"default\";s:74:\"https://ps.w.org/zajax-ajax-navigation/assets/icon-256x256.jpg?rev=1309660\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:76:\"https://ps.w.org/zajax-ajax-navigation/assets/banner-772x250.jpg?rev=1308712\";s:7:\"default\";s:76:\"https://ps.w.org/zajax-ajax-navigation/assets/banner-772x250.jpg?rev=1308712\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(358, 'theme_mods_cranius-v2', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:8:\"main-nav\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(370, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1516369649;s:7:\"checked\";a:5:{s:17:\"cranius-theme_old\";s:3:\"1.0\";s:10:\"cranius-v2\";s:3:\"2.0\";s:13:\"twentyfifteen\";s:3:\"1.8\";s:15:\"twentyseventeen\";s:3:\"1.3\";s:13:\"twentysixteen\";s:3:\"1.3\";}s:8:\"response\";a:3:{s:13:\"twentyfifteen\";a:4:{s:5:\"theme\";s:13:\"twentyfifteen\";s:11:\"new_version\";s:3:\"1.9\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentyfifteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentyfifteen.1.9.zip\";}s:15:\"twentyseventeen\";a:4:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"1.4\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.1.4.zip\";}s:13:\"twentysixteen\";a:4:{s:5:\"theme\";s:13:\"twentysixteen\";s:11:\"new_version\";s:3:\"1.4\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentysixteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentysixteen.1.4.zip\";}}s:12:\"translations\";a:0:{}}', 'no'),
(371, 'WPGM_V6_FIRST_TIME', '1', 'yes'),
(372, 'wpgmza_stats', 'a:2:{s:15:\"list_maps_basic\";a:3:{s:5:\"views\";i:3;s:13:\"last_accessed\";s:19:\"2018-01-19 15:10:09\";s:14:\"first_accessed\";s:19:\"2018-01-19 14:54:12\";}s:9:\"dashboard\";a:3:{s:5:\"views\";i:12;s:13:\"last_accessed\";s:19:\"2018-01-19 15:13:46\";s:14:\"first_accessed\";s:19:\"2018-01-19 14:54:20\";}}', 'yes'),
(373, 'WPGMZA_SETTINGS', 'a:10:{s:24:\"map_default_starting_lat\";s:9:\"40.425567\";s:24:\"map_default_starting_lng\";s:19:\"-3.6973669999999856\";s:18:\"map_default_height\";s:2:\"70\";s:17:\"map_default_width\";s:3:\"100\";s:16:\"map_default_zoom\";i:17;s:20:\"map_default_max_zoom\";i:1;s:16:\"map_default_type\";i:1;s:21:\"map_default_alignment\";i:1;s:22:\"map_default_width_type\";s:2:\"\\%\";s:23:\"map_default_height_type\";s:2:\"\\%\";}', 'yes'),
(374, 'wpgmza_google_maps_api_key', 'AIzaSyCnbHRsfP_5oSk17Phu6R8dJ1Xk_XQx8DI', 'yes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'index.php'),
(4, 2, '_edit_lock', '1516220642:1'),
(5, 2, '_edit_last', '1'),
(6, 6, '_edit_last', '1'),
(7, 6, '_edit_lock', '1505480676:1'),
(8, 2, 'feat-text', 'Con más de 27 millones de piezas de contenido social publicados diariamente, reinventar la forma de hacer contenido se hace una necesidad. En solo diez años, el panorama ha cambiado drásticamente. El ritmo y la velocidad de los cambios es abrumador, lo sabemos Contenidos Inteligentes ofrece a las marcas el potencial de escala, alcance y orientación a través de un algoritmo sofisticado, para poner el contenido adecuado frente al público adecuado.'),
(9, 2, '_feat-text', 'field_59bab4a9a48f2'),
(10, 2, 'serv_pic', '12'),
(11, 2, '_serv_pic', 'field_59babdc345d85'),
(12, 11, 'feat-text', 'Con más de 27 millones de piezas de contenido social publicados diariamente, reinventar la forma de hacer contenido se hace una necesidad. En solo diez años, el panorama ha cambiado drásticamente. El ritmo y la velocidad de los cambios es abrumador, lo sabemos Contenidos Inteligentes ofrece a las marcas el potencial de escala, alcance y orientación a través de un algoritmo sofisticado, para poner el contenido adecuado frente al público adecuado.'),
(13, 11, '_feat-text', 'field_59bab4a9a48f2'),
(14, 11, 'serv_pic', ''),
(15, 11, '_serv_pic', 'field_59babdc345d85'),
(16, 12, '_wp_attached_file', '2017/09/serv_2.png'),
(17, 12, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:894;s:6:\"height\";i:656;s:4:\"file\";s:18:\"2017/09/serv_2.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"serv_2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"serv_2-300x220.png\";s:5:\"width\";i:300;s:6:\"height\";i:220;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"serv_2-768x564.png\";s:5:\"width\";i:768;s:6:\"height\";i:564;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(18, 13, 'feat-text', 'Con más de 27 millones de piezas de contenido social publicados diariamente, reinventar la forma de hacer contenido se hace una necesidad. En solo diez años, el panorama ha cambiado drásticamente. El ritmo y la velocidad de los cambios es abrumador, lo sabemos Contenidos Inteligentes ofrece a las marcas el potencial de escala, alcance y orientación a través de un algoritmo sofisticado, para poner el contenido adecuado frente al público adecuado.'),
(19, 13, '_feat-text', 'field_59bab4a9a48f2'),
(20, 13, 'serv_pic', '12'),
(21, 13, '_serv_pic', 'field_59babdc345d85'),
(22, 14, '_edit_last', '1'),
(23, 14, '_edit_lock', '1505411548:1'),
(24, 15, '_wp_attached_file', '2017/09/cover1.png'),
(25, 15, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1203;s:6:\"height\";i:674;s:4:\"file\";s:18:\"2017/09/cover1.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"cover1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"cover1-300x168.png\";s:5:\"width\";i:300;s:6:\"height\";i:168;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"cover1-768x430.png\";s:5:\"width\";i:768;s:6:\"height\";i:430;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"cover1-1024x574.png\";s:5:\"width\";i:1024;s:6:\"height\";i:574;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(26, 14, '_thumbnail_id', '15'),
(27, 17, '_edit_last', '1'),
(28, 17, '_edit_lock', '1505417619:1'),
(29, 18, '_wp_attached_file', '2017/09/slide2.png'),
(30, 18, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1201;s:6:\"height\";i:675;s:4:\"file\";s:18:\"2017/09/slide2.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"slide2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"slide2-300x169.png\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"slide2-768x432.png\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"slide2-1024x576.png\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(31, 17, '_thumbnail_id', '18'),
(32, 20, '_wp_attached_file', '2017/09/cocosette.png'),
(33, 20, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:824;s:6:\"height\";i:659;s:4:\"file\";s:21:\"2017/09/cocosette.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"cocosette-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"cocosette-300x240.png\";s:5:\"width\";i:300;s:6:\"height\";i:240;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"cocosette-768x614.png\";s:5:\"width\";i:768;s:6:\"height\";i:614;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(34, 19, '_edit_last', '1'),
(35, 19, '_edit_lock', '1505420416:1'),
(36, 19, '_thumbnail_id', '20'),
(37, 21, '_edit_last', '1'),
(38, 21, '_edit_lock', '1505420528:1'),
(39, 22, '_wp_attached_file', '2017/09/hipster.png'),
(40, 22, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:297;s:6:\"height\";i:297;s:4:\"file\";s:19:\"2017/09/hipster.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"hipster-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(41, 21, '_thumbnail_id', '22'),
(42, 23, '_edit_last', '1'),
(43, 23, '_edit_lock', '1505420547:1'),
(44, 24, '_wp_attached_file', '2017/09/asb.png'),
(45, 24, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:297;s:6:\"height\";i:297;s:4:\"file\";s:15:\"2017/09/asb.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"asb-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(46, 23, '_thumbnail_id', '24'),
(47, 26, '_wp_attached_file', '2017/09/thank.png'),
(48, 26, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:602;s:6:\"height\";i:353;s:4:\"file\";s:17:\"2017/09/thank.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"thank-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"thank-300x176.png\";s:5:\"width\";i:300;s:6:\"height\";i:176;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(49, 25, '_edit_last', '1'),
(50, 25, '_edit_lock', '1505421537:1'),
(51, 25, '_thumbnail_id', '26'),
(52, 27, 'feat-text', 'Con más de 27 millones de piezas de contenido social publicados diariamente, reinventar la forma de hacer contenido se hace una necesidad. En solo diez años, el panorama ha cambiado drásticamente. El ritmo y la velocidad de los cambios es abrumador, lo sabemos Contenidos Inteligentes ofrece a las marcas el potencial de escala, alcance y orientación a través de un algoritmo sofisticado, para poner el contenido adecuado frente al público adecuado.'),
(53, 27, '_feat-text', 'field_59bab4a9a48f2'),
(54, 27, 'serv_pic', '12'),
(55, 27, '_serv_pic', 'field_59babdc345d85'),
(56, 28, '_edit_last', '1'),
(57, 28, '_edit_lock', '1516308398:1'),
(58, 28, '_wp_page_template', 'metodologia.php'),
(59, 30, '_edit_last', '1'),
(60, 30, '_edit_lock', '1505482306:1'),
(61, 28, 'block1_quote', 'Hacemos contenidos inteligentes'),
(62, 28, '_block1_quote', 'field_59bbd3729a177'),
(63, 28, 'block2_content', '<h2>Usamos data accionable</h2>\r\nCompetir por la atención del consumidor es nuestra obsesión. Las marcas deben beneficiarse de la posibilidad de hipersegmentación para combinar ideas creativas con ejecuciones tailored o adaptadas, todo pilotado por datos, la nueva moneda de valor, todo para entregar valor de una manera inteligente.'),
(64, 28, '_block2_content', 'field_59bbd07f562c0'),
(65, 28, 'block3_quote', 'PERSEGUIMOS ROI'),
(66, 28, '_block3_quote', 'field_59bbd09910ba7'),
(67, 28, 'block3_content', 'Para aprovechar las nuevas oportunidades y optimizar la capacidad de impulsar los resultados de negocio a través de las estrategias de Social Media, nuestros algoritmos permiten una optimización en la inversión, obteniendo una mejora en el perfomance de los KPI’s en un 30%.'),
(68, 28, '_block3_content', 'field_59bbd40b83c53'),
(69, 36, 'block1_quote', 'Hacemos contenidos inteligentes'),
(70, 36, '_block1_quote', 'field_59bbd3729a177'),
(71, 36, 'block2_content', 'Usamos data accionable\r\nCompetir por la atención del consumidor es nuestra obsesión. Las marcas deben beneficiarse de la posibilidad de hipersegmentación para combinar ideas creativas con ejecuciones tailored o adaptadas, todo pilotado por datos, la nueva moneda de valor, todo para entregar valor de una manera inteligente.'),
(72, 36, '_block2_content', 'field_59bbd07f562c0'),
(73, 36, 'block3_quote', 'PERSEGUIMOS ROI'),
(74, 36, '_block3_quote', 'field_59bbd09910ba7'),
(75, 36, 'block3_content', 'Para aprovechar las nuevas oportunidades y optimizar la capacidad de impulsar los resultados de negocio a través de las estrategias de Social Media, nuestros algoritmos permiten una optimización en la inversión, obteniendo una mejora en el perfomance de los KPI’s en un 30%.'),
(76, 36, '_block3_content', 'field_59bbd40b83c53'),
(77, 37, 'block1_quote', 'Hacemos contenidos inteligentes'),
(78, 37, '_block1_quote', 'field_59bbd3729a177'),
(79, 37, 'block2_content', '<h2>Usamos data accionable</h2>\r\nCompetir por la atención del consumidor es nuestra obsesión. Las marcas deben beneficiarse de la posibilidad de hipersegmentación para combinar ideas creativas con ejecuciones tailored o adaptadas, todo pilotado por datos, la nueva moneda de valor, todo para entregar valor de una manera inteligente.'),
(80, 37, '_block2_content', 'field_59bbd07f562c0'),
(81, 37, 'block3_quote', 'PERSEGUIMOS ROI'),
(82, 37, '_block3_quote', 'field_59bbd09910ba7'),
(83, 37, 'block3_content', 'Para aprovechar las nuevas oportunidades y optimizar la capacidad de impulsar los resultados de negocio a través de las estrategias de Social Media, nuestros algoritmos permiten una optimización en la inversión, obteniendo una mejora en el perfomance de los KPI’s en un 30%.'),
(84, 37, '_block3_content', 'field_59bbd40b83c53'),
(85, 38, '_edit_last', '1'),
(86, 38, '_edit_lock', '1505500988:1'),
(87, 39, '_wp_attached_file', '2017/09/andrehyna.png'),
(88, 39, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:484;s:6:\"height\";i:484;s:4:\"file\";s:21:\"2017/09/andrehyna.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"andrehyna-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"andrehyna-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(89, 38, '_thumbnail_id', '39'),
(90, 40, '_edit_last', '1'),
(91, 40, '_edit_lock', '1505501081:1'),
(92, 40, '_thumbnail_id', '22'),
(93, 1, '_edit_lock', '1505485160:1'),
(94, 41, '_wp_attached_file', '2017/09/post-thumb.png'),
(95, 41, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:246;s:6:\"height\";i:247;s:4:\"file\";s:22:\"2017/09/post-thumb.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"post-thumb-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(96, 1, '_edit_last', '1'),
(97, 1, '_thumbnail_id', '41'),
(100, 43, '_edit_last', '1'),
(101, 43, '_edit_lock', '1516325616:1'),
(102, 43, '_wp_page_template', 'servicios.php'),
(103, 46, '_edit_last', '1'),
(104, 46, '_edit_lock', '1505486097:1'),
(105, 43, 'quote', 'Hacemos contenidos inteligentes'),
(106, 43, '_quote', 'field_59bbe4fd5d3f5'),
(107, 48, 'quote', 'Hacemos contenidos inteligentes'),
(108, 48, '_quote', 'field_59bbe4fd5d3f5'),
(109, 49, 'quote', 'Hacemos contenidos inteligentes'),
(110, 49, '_quote', 'field_59bbe4fd5d3f5'),
(111, 50, '_edit_last', '1'),
(112, 50, '_edit_lock', '1516394614:1'),
(113, 50, '_wp_page_template', 'contact.php'),
(114, 53, '_form', '<div class=\"f-col\">\n    <div class=\"entrada\">\n    [text nombre class:simple-input class:texto-gris2 class:roboto-regular placeholder \"Nombre\"]\n    </div>\n    <div class=\"entrada\">\n    [email* email class:simple-input class:texto-gris2 class:roboto-regular placeholder \"Email\"]\n    </div>\n</div>\n<div class=\"f-vol\">\n<div class=\"entrada\">\n[textarea textarea class:simple-input class:texto-gris2 class:roboto-regular placeholder \"Su Mensaje\"]\n</div>\n</div>\n<div class=\"send-button\">[submit class:texto-azul class:roboto-black class:align-left \"Enviar\"]</div>'),
(115, 53, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:24:\"cranius \"[your-subject]\"\";s:6:\"sender\";s:41:\"[your-name] <jose.leon.avila@hotmail.com>\";s:9:\"recipient\";s:27:\"jose.leon.avila@hotmail.com\";s:4:\"body\";s:170:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on cranius (http://localhost/cranius)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(116, 53, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:24:\"cranius \"[your-subject]\"\";s:6:\"sender\";s:37:\"cranius <jose.leon.avila@hotmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:112:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on cranius (http://localhost/cranius)\";s:18:\"additional_headers\";s:37:\"Reply-To: jose.leon.avila@hotmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(117, 53, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(118, 53, '_additional_settings', ''),
(119, 53, '_locale', 'en_US'),
(120, 55, '_edit_last', '1'),
(121, 55, '_edit_lock', '1505493642:1'),
(124, 50, 'pic', '87'),
(125, 50, '_pic', 'field_59bbf45d0d9ae'),
(126, 58, 'pic', '57'),
(127, 58, '_pic', 'field_59bbf45d0d9ae'),
(128, 59, '_menu_item_type', 'post_type'),
(129, 59, '_menu_item_menu_item_parent', '0'),
(130, 59, '_menu_item_object_id', '50'),
(131, 59, '_menu_item_object', 'page'),
(132, 59, '_menu_item_target', ''),
(133, 59, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(134, 59, '_menu_item_xfn', ''),
(135, 59, '_menu_item_url', ''),
(137, 60, '_menu_item_type', 'post_type'),
(138, 60, '_menu_item_menu_item_parent', '0'),
(139, 60, '_menu_item_object_id', '43'),
(140, 60, '_menu_item_object', 'page'),
(141, 60, '_menu_item_target', ''),
(142, 60, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(143, 60, '_menu_item_xfn', ''),
(144, 60, '_menu_item_url', ''),
(146, 61, '_menu_item_type', 'post_type'),
(147, 61, '_menu_item_menu_item_parent', '0'),
(148, 61, '_menu_item_object_id', '28'),
(149, 61, '_menu_item_object', 'page'),
(150, 61, '_menu_item_target', ''),
(151, 61, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(152, 61, '_menu_item_xfn', ''),
(153, 61, '_menu_item_url', ''),
(155, 62, '_menu_item_type', 'post_type'),
(156, 62, '_menu_item_menu_item_parent', '0'),
(157, 62, '_menu_item_object_id', '2'),
(158, 62, '_menu_item_object', 'page'),
(159, 62, '_menu_item_target', ''),
(160, 62, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(161, 62, '_menu_item_xfn', ''),
(162, 62, '_menu_item_url', ''),
(164, 63, '_edit_lock', '1516373139:1'),
(165, 63, '_edit_last', '1'),
(166, 63, '_wp_page_template', 'services.php'),
(167, 65, '_menu_item_type', 'post_type'),
(168, 65, '_menu_item_menu_item_parent', '0'),
(169, 65, '_menu_item_object_id', '63'),
(170, 65, '_menu_item_object', 'page'),
(171, 65, '_menu_item_target', ''),
(172, 65, '_menu_item_classes', 'a:1:{i:0;s:4:\"hide\";}'),
(173, 65, '_menu_item_xfn', ''),
(174, 65, '_menu_item_url', ''),
(176, 66, '_edit_lock', '1505509532:1'),
(177, 66, '_edit_last', '1'),
(178, 70, '_edit_lock', '1505852493:1'),
(179, 70, '_edit_last', '1'),
(180, 72, '_edit_lock', '1505509311:1'),
(181, 72, '_edit_last', '1'),
(182, 74, '_wp_attached_file', '2017/09/serv-thumb.png'),
(183, 74, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:602;s:6:\"height\";i:350;s:4:\"file\";s:22:\"2017/09/serv-thumb.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"serv-thumb-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"serv-thumb-300x174.png\";s:5:\"width\";i:300;s:6:\"height\";i:174;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(184, 70, '_thumbnail_id', '79'),
(185, 70, 'serv_subhead', 'Segmentación'),
(186, 70, '_serv_subhead', 'field_59bc344e6cf9a'),
(187, 63, 'bloque1_quote', 'Hacemos contenidos inteligentes'),
(188, 63, '_bloque1_quote', 'field_59bc2e2ebf73c'),
(189, 76, 'bloque1_quote', 'Hacemos contenidos inteligentes'),
(190, 76, '_bloque1_quote', 'field_59bc2e2ebf73c'),
(191, 77, '_edit_lock', '1505742437:1'),
(192, 77, '_edit_last', '1'),
(193, 78, '_wp_attached_file', '2017/09/s11.png'),
(194, 78, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:604;s:6:\"height\";i:352;s:4:\"file\";s:15:\"2017/09/s11.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"s11-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"s11-300x175.png\";s:5:\"width\";i:300;s:6:\"height\";i:175;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(195, 77, '_thumbnail_id', '78'),
(196, 77, 'serv_subhead', 'Analitica'),
(197, 77, '_serv_subhead', 'field_59bc344e6cf9a'),
(198, 79, '_wp_attached_file', '2017/09/s1.png'),
(199, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:604;s:6:\"height\";i:351;s:4:\"file\";s:14:\"2017/09/s1.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"s1-300x174.png\";s:5:\"width\";i:300;s:6:\"height\";i:174;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(200, 80, '_edit_lock', '1505742527:1'),
(201, 80, '_edit_last', '1'),
(202, 81, '_wp_attached_file', '2017/09/s3.png'),
(203, 81, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:603;s:6:\"height\";i:352;s:4:\"file\";s:14:\"2017/09/s3.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"s3-300x175.png\";s:5:\"width\";i:300;s:6:\"height\";i:175;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(204, 82, '_wp_attached_file', '2017/09/s2.png'),
(205, 82, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:605;s:6:\"height\";i:350;s:4:\"file\";s:14:\"2017/09/s2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"s2-300x174.png\";s:5:\"width\";i:300;s:6:\"height\";i:174;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(206, 80, '_thumbnail_id', '82'),
(207, 80, 'serv_subhead', 'Contenidos inteligentes'),
(208, 80, '_serv_subhead', 'field_59bc344e6cf9a'),
(209, 83, '_edit_lock', '1505742723:1'),
(210, 83, '_edit_last', '1'),
(211, 84, '_wp_attached_file', '2017/09/serv-thumb2.png'),
(212, 84, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:520;s:6:\"height\";i:658;s:4:\"file\";s:23:\"2017/09/serv-thumb2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"serv-thumb2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"serv-thumb2-237x300.png\";s:5:\"width\";i:237;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(213, 83, '_thumbnail_id', '84'),
(214, 83, 'serv_subhead', 'arte + ciencia '),
(215, 83, '_serv_subhead', 'field_59bc344e6cf9a'),
(216, 85, '_edit_lock', '1505742782:1'),
(217, 85, '_edit_last', '1'),
(218, 85, '_thumbnail_id', '74'),
(219, 85, 'serv_subhead', 'dirección '),
(220, 85, '_serv_subhead', 'field_59bc344e6cf9a'),
(221, 86, '_edit_lock', '1505843166:1'),
(222, 86, '_edit_last', '1'),
(223, 86, '_thumbnail_id', '81'),
(224, 86, 'serv_subhead', 'Brand strategy & Planning'),
(225, 86, '_serv_subhead', 'field_59bc344e6cf9a'),
(226, 87, '_wp_attached_file', '2017/09/c-img.png'),
(227, 87, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:886;s:6:\"height\";i:657;s:4:\"file\";s:17:\"2017/09/c-img.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"c-img-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"c-img-300x222.png\";s:5:\"width\";i:300;s:6:\"height\";i:222;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"c-img-768x569.png\";s:5:\"width\";i:768;s:6:\"height\";i:569;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(228, 88, 'pic', '87'),
(229, 88, '_pic', 'field_59bbf45d0d9ae'),
(230, 91, '_wp_attached_file', '2018/01/bombillo.png'),
(231, 91, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:366;s:6:\"height\";i:617;s:4:\"file\";s:20:\"2018/01/bombillo.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"bombillo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"bombillo-178x300.png\";s:5:\"width\";i:178;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(232, 90, '_edit_lock', '1516289635:1'),
(233, 90, '_edit_last', '1'),
(234, 90, '_thumbnail_id', '91'),
(235, 30, '_wp_trash_meta_status', 'publish'),
(236, 30, '_wp_trash_meta_time', '1516287997'),
(237, 30, '_wp_desired_post_slug', 'group_59bbd074e6cfd'),
(238, 33, '_wp_trash_meta_status', 'publish'),
(239, 33, '_wp_trash_meta_time', '1516287998'),
(240, 33, '_wp_desired_post_slug', 'field_59bbd3729a177'),
(241, 31, '_wp_trash_meta_status', 'publish'),
(242, 31, '_wp_trash_meta_time', '1516287998'),
(243, 31, '_wp_desired_post_slug', 'field_59bbd07f562c0'),
(244, 32, '_wp_trash_meta_status', 'publish'),
(245, 32, '_wp_trash_meta_time', '1516287999'),
(246, 32, '_wp_desired_post_slug', 'field_59bbd09910ba7'),
(247, 34, '_wp_trash_meta_status', 'publish'),
(248, 34, '_wp_trash_meta_time', '1516287999'),
(249, 34, '_wp_desired_post_slug', 'field_59bbd40b83c53'),
(250, 55, '_wp_trash_meta_status', 'publish'),
(251, 55, '_wp_trash_meta_time', '1516288000'),
(252, 55, '_wp_desired_post_slug', 'group_59bbf458f40d0'),
(253, 56, '_wp_trash_meta_status', 'publish'),
(254, 56, '_wp_trash_meta_time', '1516288000'),
(255, 56, '_wp_desired_post_slug', 'field_59bbf45d0d9ae'),
(256, 6, '_wp_trash_meta_status', 'publish'),
(257, 6, '_wp_trash_meta_time', '1516288001'),
(258, 6, '_wp_desired_post_slug', 'group_59bab2262e609'),
(259, 7, '_wp_trash_meta_status', 'publish'),
(260, 7, '_wp_trash_meta_time', '1516288002'),
(261, 7, '_wp_desired_post_slug', 'field_59bab4a9a48f2'),
(262, 8, '_wp_trash_meta_status', 'publish'),
(263, 8, '_wp_trash_meta_time', '1516288002'),
(264, 8, '_wp_desired_post_slug', 'field_59babdc345d85'),
(265, 46, '_wp_trash_meta_status', 'publish'),
(266, 46, '_wp_trash_meta_time', '1516288003'),
(267, 46, '_wp_desired_post_slug', 'group_59bbe4f4aec6d'),
(268, 47, '_wp_trash_meta_status', 'publish'),
(269, 47, '_wp_trash_meta_time', '1516288003'),
(270, 47, '_wp_desired_post_slug', 'field_59bbe4fd5d3f5'),
(271, 72, '_wp_trash_meta_status', 'publish'),
(272, 72, '_wp_trash_meta_time', '1516288004'),
(273, 72, '_wp_desired_post_slug', 'group_59bc3447c4506'),
(274, 73, '_wp_trash_meta_status', 'publish'),
(275, 73, '_wp_trash_meta_time', '1516288004'),
(276, 73, '_wp_desired_post_slug', 'field_59bc344e6cf9a'),
(277, 66, '_wp_trash_meta_status', 'publish'),
(278, 66, '_wp_trash_meta_time', '1516288005'),
(279, 66, '_wp_desired_post_slug', 'group_59bc2d0cafa7b'),
(280, 67, '_wp_trash_meta_status', 'publish'),
(281, 67, '_wp_trash_meta_time', '1516288006'),
(282, 67, '_wp_desired_post_slug', 'field_59bc2e2ebf73c'),
(283, 93, '_edit_lock', '1516297944:1'),
(284, 93, '_edit_last', '1'),
(285, 90, 'enlace', 'servicios'),
(286, 90, '_enlace', 'field_5a60b84fb1fc5'),
(287, 96, '_edit_lock', '1516289484:1'),
(288, 96, '_edit_last', '1'),
(289, 97, '_wp_attached_file', '2018/01/slide-1-home.png'),
(290, 97, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1080;s:4:\"file\";s:24:\"2018/01/slide-1-home.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"slide-1-home-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"slide-1-home-300x169.png\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"slide-1-home-768x432.png\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:25:\"slide-1-home-1024x576.png\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(291, 96, '_thumbnail_id', '97'),
(292, 96, 'enlace', 'equipo'),
(293, 96, '_enlace', 'field_5a60b84fb1fc5'),
(294, 98, '_edit_lock', '1516297835:1'),
(295, 98, '_edit_last', '1'),
(296, 99, '_wp_attached_file', '2018/01/rubik-mobile.png'),
(297, 99, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:494;s:6:\"height\";i:519;s:4:\"file\";s:24:\"2018/01/rubik-mobile.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"rubik-mobile-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"rubik-mobile-286x300.png\";s:5:\"width\";i:286;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(298, 98, '_thumbnail_id', '100'),
(299, 98, 'enlace', 'metodologia'),
(300, 98, '_enlace', 'field_5a60b84fb1fc5'),
(301, 100, '_wp_attached_file', '2018/01/slide-3-home.png'),
(302, 100, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:494;s:6:\"height\";i:519;s:4:\"file\";s:24:\"2018/01/slide-3-home.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"slide-3-home-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"slide-3-home-286x300.png\";s:5:\"width\";i:286;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(303, 101, 'block1_quote', 'Hacemos contenidos inteligentes'),
(304, 101, '_block1_quote', 'field_59bbd3729a177'),
(305, 101, 'block2_content', '<h2>Usamos data accionable</h2>\r\nCompetir por la atención del consumidor es nuestra obsesión. Las marcas deben beneficiarse de la posibilidad de hipersegmentación para combinar ideas creativas con ejecuciones tailored o adaptadas, todo pilotado por datos, la nueva moneda de valor, todo para entregar valor de una manera inteligente.'),
(306, 101, '_block2_content', 'field_59bbd07f562c0'),
(307, 101, 'block3_quote', 'PERSEGUIMOS ROI'),
(308, 101, '_block3_quote', 'field_59bbd09910ba7'),
(309, 101, 'block3_content', 'Para aprovechar las nuevas oportunidades y optimizar la capacidad de impulsar los resultados de negocio a través de las estrategias de Social Media, nuestros algoritmos permiten una optimización en la inversión, obteniendo una mejora en el perfomance de los KPI’s en un 30%.'),
(310, 101, '_block3_content', 'field_59bbd40b83c53'),
(311, 102, '_edit_lock', '1516298134:1'),
(312, 103, '_edit_lock', '1516298294:1'),
(313, 104, '_edit_lock', '1516298341:1'),
(314, 105, '_edit_lock', '1516308417:1'),
(315, 106, 'block1_quote', 'Hacemos contenidos inteligentes'),
(316, 106, '_block1_quote', 'field_59bbd3729a177'),
(317, 106, 'block2_content', '<h2>Usamos data accionable</h2>\r\nCompetir por la atención del consumidor es nuestra obsesión. Las marcas deben beneficiarse de la posibilidad de hipersegmentación para combinar ideas creativas con ejecuciones tailored o adaptadas, todo pilotado por datos, la nueva moneda de valor, todo para entregar valor de una manera inteligente.'),
(318, 106, '_block2_content', 'field_59bbd07f562c0'),
(319, 106, 'block3_quote', 'PERSEGUIMOS ROI'),
(320, 106, '_block3_quote', 'field_59bbd09910ba7'),
(321, 106, 'block3_content', 'Para aprovechar las nuevas oportunidades y optimizar la capacidad de impulsar los resultados de negocio a través de las estrategias de Social Media, nuestros algoritmos permiten una optimización en la inversión, obteniendo una mejora en el perfomance de los KPI’s en un 30%.'),
(322, 106, '_block3_content', 'field_59bbd40b83c53'),
(323, 107, '_wp_attached_file', '2018/01/banner-metodologia.png'),
(324, 107, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1800;s:6:\"height\";i:749;s:4:\"file\";s:30:\"2018/01/banner-metodologia.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"banner-metodologia-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"banner-metodologia-300x125.png\";s:5:\"width\";i:300;s:6:\"height\";i:125;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"banner-metodologia-768x320.png\";s:5:\"width\";i:768;s:6:\"height\";i:320;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:31:\"banner-metodologia-1024x426.png\";s:5:\"width\";i:1024;s:6:\"height\";i:426;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(325, 28, '_thumbnail_id', '107'),
(326, 105, '_edit_last', '1'),
(327, 113, '_wp_attached_file', '2018/01/computer.png'),
(328, 113, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:342;s:6:\"height\";i:326;s:4:\"file\";s:20:\"2018/01/computer.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"computer-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"computer-300x286.png\";s:5:\"width\";i:300;s:6:\"height\";i:286;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(329, 28, 'c-img', '113'),
(330, 28, '_c-img', 'field_5a60ea1b11b66'),
(331, 28, 'm-slide', '3'),
(332, 28, '_m-slide', 'field_5a60ea6db1e89'),
(333, 114, 'block1_quote', 'Hacemos contenidos inteligentes'),
(334, 114, '_block1_quote', 'field_59bbd3729a177'),
(335, 114, 'block2_content', '<h2>Usamos data accionable</h2>\r\nCompetir por la atención del consumidor es nuestra obsesión. Las marcas deben beneficiarse de la posibilidad de hipersegmentación para combinar ideas creativas con ejecuciones tailored o adaptadas, todo pilotado por datos, la nueva moneda de valor, todo para entregar valor de una manera inteligente.'),
(336, 114, '_block2_content', 'field_59bbd07f562c0'),
(337, 114, 'block3_quote', 'PERSEGUIMOS ROI'),
(338, 114, '_block3_quote', 'field_59bbd09910ba7'),
(339, 114, 'block3_content', 'Para aprovechar las nuevas oportunidades y optimizar la capacidad de impulsar los resultados de negocio a través de las estrategias de Social Media, nuestros algoritmos permiten una optimización en la inversión, obteniendo una mejora en el perfomance de los KPI’s en un 30%.'),
(340, 114, '_block3_content', 'field_59bbd40b83c53'),
(341, 114, 'c-img', '113'),
(342, 114, '_c-img', 'field_5a60ea1b11b66'),
(343, 114, 'm-slide', ''),
(344, 114, '_m-slide', 'field_5a60ea6db1e89'),
(345, 115, '_wp_attached_file', '2018/01/sublime-metodologia.png'),
(346, 115, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:691;s:6:\"height\";i:460;s:4:\"file\";s:31:\"2018/01/sublime-metodologia.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"sublime-metodologia-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"sublime-metodologia-300x200.png\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(347, 116, '_wp_attached_file', '2018/01/banner-2-abajo.png'),
(348, 116, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:688;s:6:\"height\";i:459;s:4:\"file\";s:26:\"2018/01/banner-2-abajo.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"banner-2-abajo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"banner-2-abajo-300x200.png\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(349, 117, '_wp_attached_file', '2018/01/banner-3-abajo.png'),
(350, 117, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:738;s:6:\"height\";i:522;s:4:\"file\";s:26:\"2018/01/banner-3-abajo.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"banner-3-abajo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"banner-3-abajo-300x212.png\";s:5:\"width\";i:300;s:6:\"height\";i:212;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(351, 28, 'm-slide_0_imagen', '115'),
(352, 28, '_m-slide_0_imagen', 'field_5a60ebe46fc44'),
(353, 28, 'm-slide_0_titulo', 'Usamos data accionable'),
(354, 28, '_m-slide_0_titulo', 'field_5a60ebf16fc45'),
(355, 28, 'm-slide_0_contenido', 'La primera agencia digital que utiliza algoritmos propios para crear y distribuir contenido.    \r\n\r\nCon una metodología integral, combinamos estrategia, análisis y creatividad para llevar el  mensaje correcto a la audiencia correcta y así maximizar el resultado.'),
(356, 28, '_m-slide_0_contenido', 'field_5a60ebf66fc46'),
(357, 28, 'm-slide_1_imagen', '116'),
(358, 28, '_m-slide_1_imagen', 'field_5a60ebe46fc44'),
(359, 28, 'm-slide_1_titulo', 'Competir por la atención del consumidor es nuestra obsesión'),
(360, 28, '_m-slide_1_titulo', 'field_5a60ebf16fc45'),
(361, 28, 'm-slide_1_contenido', 'La primera agencia digital que utiliza algoritmos propios para crear y distribuir contenido.    \r\n\r\nCon una metodología integral, combinamos estrategia, análisis y creatividad para llevar el  mensaje correcto a la audiencia correcta y así maximizar el resultado.'),
(362, 28, '_m-slide_1_contenido', 'field_5a60ebf66fc46'),
(363, 28, 'm-slide_2_imagen', '117'),
(364, 28, '_m-slide_2_imagen', 'field_5a60ebe46fc44'),
(365, 28, 'm-slide_2_titulo', 'ROI Social'),
(366, 28, '_m-slide_2_titulo', 'field_5a60ebf16fc45'),
(367, 28, 'm-slide_2_contenido', 'Para impulsar los resultados de negocio a través de las estrategias de Social Media, nuestros algoritmos permiten una optimización de la inversión, obteniendo un mejor desempeño de los KPI’s.'),
(368, 28, '_m-slide_2_contenido', 'field_5a60ebf66fc46'),
(369, 118, 'block1_quote', 'Hacemos contenidos inteligentes'),
(370, 118, '_block1_quote', 'field_59bbd3729a177'),
(371, 118, 'block2_content', '<h2>Usamos data accionable</h2>\r\nCompetir por la atención del consumidor es nuestra obsesión. Las marcas deben beneficiarse de la posibilidad de hipersegmentación para combinar ideas creativas con ejecuciones tailored o adaptadas, todo pilotado por datos, la nueva moneda de valor, todo para entregar valor de una manera inteligente.'),
(372, 118, '_block2_content', 'field_59bbd07f562c0'),
(373, 118, 'block3_quote', 'PERSEGUIMOS ROI'),
(374, 118, '_block3_quote', 'field_59bbd09910ba7'),
(375, 118, 'block3_content', 'Para aprovechar las nuevas oportunidades y optimizar la capacidad de impulsar los resultados de negocio a través de las estrategias de Social Media, nuestros algoritmos permiten una optimización en la inversión, obteniendo una mejora en el perfomance de los KPI’s en un 30%.'),
(376, 118, '_block3_content', 'field_59bbd40b83c53'),
(377, 118, 'c-img', '113'),
(378, 118, '_c-img', 'field_5a60ea1b11b66'),
(379, 118, 'm-slide', '3'),
(380, 118, '_m-slide', 'field_5a60ea6db1e89'),
(381, 118, 'm-slide_0_imagen', '115'),
(382, 118, '_m-slide_0_imagen', 'field_5a60ebe46fc44'),
(383, 118, 'm-slide_0_titulo', 'Usamos data accionable'),
(384, 118, '_m-slide_0_titulo', 'field_5a60ebf16fc45'),
(385, 118, 'm-slide_0_contenido', 'La primera agencia digital que utiliza algoritmos propios para crear y distribuir contenido.    \r\n\r\n                                    Con una metodología integral, combinamos estrategia, análisis y creatividad para llevar el  mensaje correcto a la audiencia correcta y así maximizar el resultado.'),
(386, 118, '_m-slide_0_contenido', 'field_5a60ebf66fc46'),
(387, 118, 'm-slide_1_imagen', '116'),
(388, 118, '_m-slide_1_imagen', 'field_5a60ebe46fc44'),
(389, 118, 'm-slide_1_titulo', 'Competir por la atención del consumidor es nuestra obsesión'),
(390, 118, '_m-slide_1_titulo', 'field_5a60ebf16fc45'),
(391, 118, 'm-slide_1_contenido', 'La primera agencia digital que utiliza algoritmos propios para crear y distribuir contenido.    \r\n\r\nCon una metodología integral, combinamos estrategia, análisis y creatividad para llevar el  mensaje correcto a la audiencia correcta y así maximizar el resultado.'),
(392, 118, '_m-slide_1_contenido', 'field_5a60ebf66fc46'),
(393, 118, 'm-slide_2_imagen', '117'),
(394, 118, '_m-slide_2_imagen', 'field_5a60ebe46fc44'),
(395, 118, 'm-slide_2_titulo', 'ROI Social'),
(396, 118, '_m-slide_2_titulo', 'field_5a60ebf16fc45'),
(397, 118, 'm-slide_2_contenido', 'Para impulsar los resultados de negocio a través de las estrategias de Social Media, nuestros algoritmos permiten una optimización de la inversión, obteniendo un mejor desempeño de los KPI’s.'),
(398, 118, '_m-slide_2_contenido', 'field_5a60ebf66fc46'),
(399, 119, 'block1_quote', 'Hacemos contenidos inteligentes'),
(400, 119, '_block1_quote', 'field_59bbd3729a177'),
(401, 119, 'block2_content', '<h2>Usamos data accionable</h2>\r\nCompetir por la atención del consumidor es nuestra obsesión. Las marcas deben beneficiarse de la posibilidad de hipersegmentación para combinar ideas creativas con ejecuciones tailored o adaptadas, todo pilotado por datos, la nueva moneda de valor, todo para entregar valor de una manera inteligente.'),
(402, 119, '_block2_content', 'field_59bbd07f562c0'),
(403, 119, 'block3_quote', 'PERSEGUIMOS ROI'),
(404, 119, '_block3_quote', 'field_59bbd09910ba7'),
(405, 119, 'block3_content', 'Para aprovechar las nuevas oportunidades y optimizar la capacidad de impulsar los resultados de negocio a través de las estrategias de Social Media, nuestros algoritmos permiten una optimización en la inversión, obteniendo una mejora en el perfomance de los KPI’s en un 30%.'),
(406, 119, '_block3_content', 'field_59bbd40b83c53'),
(407, 119, 'c-img', '113'),
(408, 119, '_c-img', 'field_5a60ea1b11b66'),
(409, 119, 'm-slide', '3'),
(410, 119, '_m-slide', 'field_5a60ea6db1e89'),
(411, 119, 'm-slide_0_imagen', '115'),
(412, 119, '_m-slide_0_imagen', 'field_5a60ebe46fc44'),
(413, 119, 'm-slide_0_titulo', 'Usamos data accionable'),
(414, 119, '_m-slide_0_titulo', 'field_5a60ebf16fc45'),
(415, 119, 'm-slide_0_contenido', 'La primera agencia digital que utiliza algoritmos propios para crear y distribuir contenido.    \r\n\r\nCon una metodología integral, combinamos estrategia, análisis y creatividad para llevar el  mensaje correcto a la audiencia correcta y así maximizar el resultado.'),
(416, 119, '_m-slide_0_contenido', 'field_5a60ebf66fc46'),
(417, 119, 'm-slide_1_imagen', '116'),
(418, 119, '_m-slide_1_imagen', 'field_5a60ebe46fc44'),
(419, 119, 'm-slide_1_titulo', 'Competir por la atención del consumidor es nuestra obsesión'),
(420, 119, '_m-slide_1_titulo', 'field_5a60ebf16fc45'),
(421, 119, 'm-slide_1_contenido', 'La primera agencia digital que utiliza algoritmos propios para crear y distribuir contenido.    \r\n\r\nCon una metodología integral, combinamos estrategia, análisis y creatividad para llevar el  mensaje correcto a la audiencia correcta y así maximizar el resultado.'),
(422, 119, '_m-slide_1_contenido', 'field_5a60ebf66fc46'),
(423, 119, 'm-slide_2_imagen', '117'),
(424, 119, '_m-slide_2_imagen', 'field_5a60ebe46fc44'),
(425, 119, 'm-slide_2_titulo', 'ROI Social'),
(426, 119, '_m-slide_2_titulo', 'field_5a60ebf16fc45'),
(427, 119, 'm-slide_2_contenido', 'Para impulsar los resultados de negocio a través de las estrategias de Social Media, nuestros algoritmos permiten una optimización de la inversión, obteniendo un mejor desempeño de los KPI’s.'),
(428, 119, '_m-slide_2_contenido', 'field_5a60ebf66fc46'),
(429, 120, '_edit_lock', '1516308448:1');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(430, 121, '_edit_lock', '1516308468:1'),
(431, 122, '_edit_lock', '1516309378:1'),
(432, 122, '_edit_last', '1'),
(433, 123, 'quote', 'Hacemos contenidos inteligentes'),
(434, 123, '_quote', 'field_59bbe4fd5d3f5'),
(435, 124, '_wp_attached_file', '2018/01/banner-servicios.jpg'),
(436, 124, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1800;s:6:\"height\";i:750;s:4:\"file\";s:28:\"2018/01/banner-servicios.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"banner-servicios-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"banner-servicios-300x125.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:125;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:28:\"banner-servicios-768x320.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:320;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:29:\"banner-servicios-1024x427.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:427;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(437, 43, '_thumbnail_id', '124'),
(438, 126, 'quote', 'Hacemos contenidos inteligentes'),
(439, 126, '_quote', 'field_59bbe4fd5d3f5'),
(440, 43, 'p-titulo', 'Hacemos contenidos inteligentes'),
(441, 43, '_p-titulo', 'field_5a61090fa2983'),
(442, 128, 'quote', 'Hacemos contenidos inteligentes'),
(443, 128, '_quote', 'field_59bbe4fd5d3f5'),
(444, 128, 'p-titulo', 'Hacemos contenidos inteligentes'),
(445, 128, '_p-titulo', 'field_5a61090fa2983'),
(446, 134, '_wp_attached_file', '2018/01/fichas-servicios.png'),
(447, 134, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:366;s:6:\"height\";i:218;s:4:\"file\";s:28:\"2018/01/fichas-servicios.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"fichas-servicios-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"fichas-servicios-300x179.png\";s:5:\"width\";i:300;s:6:\"height\";i:179;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(448, 43, 'serv-item_0_campo', 'Estrategia'),
(449, 43, '_serv-item_0_campo', 'field_5a6109796df52'),
(450, 43, 'serv-item_0_nombre', 'Pilares'),
(451, 43, '_serv-item_0_nombre', 'field_5a6109846df53'),
(452, 43, 'serv-item_0_descripcion', 'Sabemos que el entorno digital es complejo y cambiante, por eso nuestra metodología consiste en combinar herramientas propias para crear una visión de marca relevante y provocadora, a través de un profundo entendimiento del comportamiento de sus consumidores en los espacios digitales.'),
(453, 43, '_serv-item_0_descripcion', 'field_5a61098d6df54'),
(454, 43, 'serv-item_0_imagen', '134'),
(455, 43, '_serv-item_0_imagen', 'field_5a61099c6df55'),
(456, 43, 'serv-item', '6'),
(457, 43, '_serv-item', 'field_5a61093c6df51'),
(458, 135, 'quote', 'Hacemos contenidos inteligentes'),
(459, 135, '_quote', 'field_59bbe4fd5d3f5'),
(460, 135, 'p-titulo', 'Hacemos contenidos inteligentes'),
(461, 135, '_p-titulo', 'field_5a61090fa2983'),
(462, 135, 'serv-item_0_campo', 'Estrategia'),
(463, 135, '_serv-item_0_campo', 'field_5a6109796df52'),
(464, 135, 'serv-item_0_nombre', 'Pilares'),
(465, 135, '_serv-item_0_nombre', 'field_5a6109846df53'),
(466, 135, 'serv-item_0_descripcion', 'Sabemos que el entorno digital es complejo y cambiante, por eso nuestra metodología consiste en combinar herramientas propias para crear una visión de marca relevante y provocadora, a través de un profundo entendimiento del comportamiento de sus consumidores en los espacios digitales.'),
(467, 135, '_serv-item_0_descripcion', 'field_5a61098d6df54'),
(468, 135, 'serv-item_0_imagen', '134'),
(469, 135, '_serv-item_0_imagen', 'field_5a61099c6df55'),
(470, 135, 'serv-item', '1'),
(471, 135, '_serv-item', 'field_5a61093c6df51'),
(474, 43, 'serv-item_1_campo', 'Hipersegmentación'),
(475, 43, '_serv-item_1_campo', 'field_5a6109796df52'),
(476, 43, 'serv-item_1_nombre', 'Clusterización de Audiencias'),
(477, 43, '_serv-item_1_nombre', 'field_5a6109846df53'),
(478, 43, 'serv-item_1_descripcion', 'Ofrecemos una metodología 100% algorítmica, que gracias a nuestra herramienta GAIA reconoce dentro de las audiencias digitales, sub-targets específicos a los que podemos dirigir mensajes de manera personalizada.'),
(479, 43, '_serv-item_1_descripcion', 'field_5a61098d6df54'),
(480, 43, 'serv-item_1_imagen', '136'),
(481, 43, '_serv-item_1_imagen', 'field_5a61099c6df55'),
(482, 137, 'quote', 'Hacemos contenidos inteligentes'),
(483, 137, '_quote', 'field_59bbe4fd5d3f5'),
(484, 137, 'p-titulo', 'Hacemos contenidos inteligentes'),
(485, 137, '_p-titulo', 'field_5a61090fa2983'),
(486, 137, 'serv-item_0_campo', 'Estrategia'),
(487, 137, '_serv-item_0_campo', 'field_5a6109796df52'),
(488, 137, 'serv-item_0_nombre', 'Pilares'),
(489, 137, '_serv-item_0_nombre', 'field_5a6109846df53'),
(490, 137, 'serv-item_0_descripcion', 'Sabemos que el entorno digital es complejo y cambiante, por eso nuestra metodología consiste en combinar herramientas propias para crear una visión de marca relevante y provocadora, a través de un profundo entendimiento del comportamiento de sus consumidores en los espacios digitales.'),
(491, 137, '_serv-item_0_descripcion', 'field_5a61098d6df54'),
(492, 137, 'serv-item_0_imagen', '134'),
(493, 137, '_serv-item_0_imagen', 'field_5a61099c6df55'),
(494, 137, 'serv-item', '2'),
(495, 137, '_serv-item', 'field_5a61093c6df51'),
(496, 137, 'serv-item_1_campo', 'Hipersegmentación'),
(497, 137, '_serv-item_1_campo', 'field_5a6109796df52'),
(498, 137, 'serv-item_1_nombre', 'Clusterización de Audiencias'),
(499, 137, '_serv-item_1_nombre', 'field_5a6109846df53'),
(500, 137, 'serv-item_1_descripcion', 'Ofrecemos una metodología 100% algorítmica, que gracias a nuestra herramienta GAIA reconoce dentro de las audiencias digitales, sub-targets específicos a los que podemos dirigir mensajes de manera personalizada.'),
(501, 137, '_serv-item_1_descripcion', 'field_5a61098d6df54'),
(502, 137, 'serv-item_1_imagen', '136'),
(503, 137, '_serv-item_1_imagen', 'field_5a61099c6df55'),
(504, 138, '_edit_lock', '1516373130:1'),
(505, 138, '_edit_last', '1'),
(514, 43, 'serv-item_2_campo', 'Machine Learning - AI'),
(515, 43, '_serv-item_2_campo', 'field_5a6109796df52'),
(516, 43, 'serv-item_2_nombre', 'Semiótica de Clústeres'),
(517, 43, '_serv-item_2_nombre', 'field_5a6109846df53'),
(518, 43, 'serv-item_2_descripcion', 'Hoy sabemos más del consumidor de lo que jamás sabíamos. Nuestra metodología combina el reconocimiento del ecosistema digital y el análisis de comportamiento del contenido social para entender los aspectos culturales, simbólicos y sociales que se transmiten en las piezas de contenido y así aprender, conectar y persuadir con contenidos inteligentes.'),
(519, 43, '_serv-item_2_descripcion', 'field_5a61098d6df54'),
(520, 43, 'serv-item_2_imagen', '139'),
(521, 43, '_serv-item_2_imagen', 'field_5a61099c6df55'),
(522, 43, 'serv-item_3_campo', 'Creatividad'),
(523, 43, '_serv-item_3_campo', 'field_5a6109796df52'),
(524, 43, 'serv-item_3_nombre', 'Contenidos Inteligentes'),
(525, 43, '_serv-item_3_nombre', 'field_5a6109846df53'),
(526, 43, 'serv-item_3_descripcion', 'Nuestras historias buscan la manera de acercar la verdad de las marcas a la verdad de los consumidores, y lo hacemos guiados por los datos que obtenemos a través de las miles de interacciones diarias que los usuarios tienen en las plataformas sociales.\r\n\r\n'),
(527, 43, '_serv-item_3_descripcion', 'field_5a61098d6df54'),
(528, 43, 'serv-item_3_imagen', '140'),
(529, 43, '_serv-item_3_imagen', 'field_5a61099c6df55'),
(530, 43, 'serv-item_4_campo', 'Formato y Entrega de Contenido'),
(531, 43, '_serv-item_4_campo', 'field_5a6109796df52'),
(532, 43, 'serv-item_4_nombre', 'Distribución de Contenido'),
(533, 43, '_serv-item_4_nombre', 'field_5a6109846df53'),
(534, 43, 'serv-item_4_descripcion', 'La distribución del contenido en las plataformas digitales adecuadas es esencial para el éxito. Medios propios, pagados, y ganados se planifican juntos aunque se ejecuten por separado. Buscamos que cada pieza de contenido llegue a las audiencias correctas, a través de la plataforma, formato y momento ideal.\r\n\r\n'),
(535, 43, '_serv-item_4_descripcion', 'field_5a61098d6df54'),
(536, 43, 'serv-item_4_imagen', '141'),
(537, 43, '_serv-item_4_imagen', 'field_5a61099c6df55'),
(538, 43, 'serv-item_5_campo', 'Ánalisis de Resultados'),
(539, 43, '_serv-item_5_campo', 'field_5a6109796df52'),
(540, 43, 'serv-item_5_nombre', 'Métricas y Analíticas'),
(541, 43, '_serv-item_5_nombre', 'field_5a6109846df53'),
(542, 43, 'serv-item_5_descripcion', 'Estamos constantemente midiendo el desempeño de nuestras propuestas. El objetivo es optimizar el rendimiento de la inversión para mejorar los resultados.\r\n\r\n'),
(543, 43, '_serv-item_5_descripcion', 'field_5a61098d6df54'),
(544, 43, 'serv-item_5_imagen', '142'),
(545, 43, '_serv-item_5_imagen', 'field_5a61099c6df55'),
(546, 143, 'quote', 'Hacemos contenidos inteligentes'),
(547, 143, '_quote', 'field_59bbe4fd5d3f5'),
(548, 143, 'p-titulo', 'Hacemos contenidos inteligentes'),
(549, 143, '_p-titulo', 'field_5a61090fa2983'),
(550, 143, 'serv-item_0_campo', 'Estrategia'),
(551, 143, '_serv-item_0_campo', 'field_5a6109796df52'),
(552, 143, 'serv-item_0_nombre', 'Pilares'),
(553, 143, '_serv-item_0_nombre', 'field_5a6109846df53'),
(554, 143, 'serv-item_0_descripcion', 'Sabemos que el entorno digital es complejo y cambiante, por eso nuestra metodología consiste en combinar herramientas propias para crear una visión de marca relevante y provocadora, a través de un profundo entendimiento del comportamiento de sus consumidores en los espacios digitales.'),
(555, 143, '_serv-item_0_descripcion', 'field_5a61098d6df54'),
(556, 143, 'serv-item_0_imagen', '134'),
(557, 143, '_serv-item_0_imagen', 'field_5a61099c6df55'),
(558, 143, 'serv-item', '6'),
(559, 143, '_serv-item', 'field_5a61093c6df51'),
(560, 143, 'serv-item_1_campo', 'Hipersegmentación'),
(561, 143, '_serv-item_1_campo', 'field_5a6109796df52'),
(562, 143, 'serv-item_1_nombre', 'Clusterización de Audiencias'),
(563, 143, '_serv-item_1_nombre', 'field_5a6109846df53'),
(564, 143, 'serv-item_1_descripcion', 'Ofrecemos una metodología 100% algorítmica, que gracias a nuestra herramienta GAIA reconoce dentro de las audiencias digitales, sub-targets específicos a los que podemos dirigir mensajes de manera personalizada.'),
(565, 143, '_serv-item_1_descripcion', 'field_5a61098d6df54'),
(566, 143, 'serv-item_1_imagen', '136'),
(567, 143, '_serv-item_1_imagen', 'field_5a61099c6df55'),
(568, 143, 'serv-item_2_campo', 'Machine Learning - AI'),
(569, 143, '_serv-item_2_campo', 'field_5a6109796df52'),
(570, 143, 'serv-item_2_nombre', 'Semiótica de Clústeres'),
(571, 143, '_serv-item_2_nombre', 'field_5a6109846df53'),
(572, 143, 'serv-item_2_descripcion', 'Hoy sabemos más del consumidor de lo que jamás sabíamos. Nuestra metodología combina el reconocimiento del ecosistema digital y el análisis de comportamiento del contenido social para entender los aspectos culturales, simbólicos y sociales que se transmiten en las piezas de contenido y así aprender, conectar y persuadir con contenidos inteligentes.'),
(573, 143, '_serv-item_2_descripcion', 'field_5a61098d6df54'),
(574, 143, 'serv-item_2_imagen', '139'),
(575, 143, '_serv-item_2_imagen', 'field_5a61099c6df55'),
(576, 143, 'serv-item_3_campo', 'Creatividad'),
(577, 143, '_serv-item_3_campo', 'field_5a6109796df52'),
(578, 143, 'serv-item_3_nombre', 'Contenidos Inteligentes'),
(579, 143, '_serv-item_3_nombre', 'field_5a6109846df53'),
(580, 143, 'serv-item_3_descripcion', 'Nuestras historias buscan la manera de acercar la verdad de las marcas a la verdad de los consumidores, y lo hacemos guiados por los datos que obtenemos a través de las miles de interacciones diarias que los usuarios tienen en las plataformas sociales.\r\n\r\n'),
(581, 143, '_serv-item_3_descripcion', 'field_5a61098d6df54'),
(582, 143, 'serv-item_3_imagen', '140'),
(583, 143, '_serv-item_3_imagen', 'field_5a61099c6df55'),
(584, 143, 'serv-item_4_campo', 'Formato y Entrega de Contenido'),
(585, 143, '_serv-item_4_campo', 'field_5a6109796df52'),
(586, 143, 'serv-item_4_nombre', 'Distribución de Contenido'),
(587, 143, '_serv-item_4_nombre', 'field_5a6109846df53'),
(588, 143, 'serv-item_4_descripcion', 'La distribución del contenido en las plataformas digitales adecuadas es esencial para el éxito. Medios propios, pagados, y ganados se planifican juntos aunque se ejecuten por separado. Buscamos que cada pieza de contenido llegue a las audiencias correctas, a través de la plataforma, formato y momento ideal.\r\n\r\n'),
(589, 143, '_serv-item_4_descripcion', 'field_5a61098d6df54'),
(590, 143, 'serv-item_4_imagen', '141'),
(591, 143, '_serv-item_4_imagen', 'field_5a61099c6df55'),
(592, 143, 'serv-item_5_campo', 'Ánalisis de Resultados'),
(593, 143, '_serv-item_5_campo', 'field_5a6109796df52'),
(594, 143, 'serv-item_5_nombre', 'Métricas y Analíticas'),
(595, 143, '_serv-item_5_nombre', 'field_5a6109846df53'),
(596, 143, 'serv-item_5_descripcion', 'Estamos constantemente midiendo el desempeño de nuestras propuestas. El objetivo es optimizar el rendimiento de la inversión para mejorar los resultados.\r\n\r\n'),
(597, 143, '_serv-item_5_descripcion', 'field_5a61098d6df54'),
(598, 143, 'serv-item_5_imagen', '142'),
(599, 143, '_serv-item_5_imagen', 'field_5a61099c6df55'),
(600, 138, '_wp_page_template', 'team.php'),
(604, 147, '_menu_item_type', 'post_type'),
(605, 147, '_menu_item_menu_item_parent', '0'),
(606, 147, '_menu_item_object_id', '138'),
(607, 147, '_menu_item_object', 'page'),
(608, 147, '_menu_item_target', ''),
(609, 147, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(610, 147, '_menu_item_xfn', ''),
(611, 147, '_menu_item_url', ''),
(613, 148, '_edit_lock', '1516326551:1'),
(614, 148, '_edit_last', '1'),
(615, 138, 'equipo', '4'),
(616, 138, '_equipo', 'field_5a614e537af62'),
(617, 154, 'equipo', ''),
(618, 154, '_equipo', 'field_5a614e537af62'),
(629, 138, 'equipo_0_imagen', '160'),
(630, 138, '_equipo_0_imagen', 'field_5a614e987af63'),
(631, 138, 'equipo_0_nombre', 'Jhon da Silva'),
(632, 138, '_equipo_0_nombre', 'field_5a614ede7af64'),
(633, 138, 'equipo_0_descripcion', 'Miembro del Consejo Asesor de Cranius.'),
(634, 138, '_equipo_0_descripcion', 'field_5a614eea7af65'),
(635, 138, 'equipo_1_imagen', '161'),
(636, 138, '_equipo_1_imagen', 'field_5a614e987af63'),
(637, 138, 'equipo_1_nombre', 'Macarena Estevez'),
(638, 138, '_equipo_1_nombre', 'field_5a614ede7af64'),
(639, 138, 'equipo_1_descripcion', 'Miembro del Consejo Asesor de Cranius.'),
(640, 138, '_equipo_1_descripcion', 'field_5a614eea7af65'),
(641, 138, 'equipo_2_imagen', '162'),
(642, 138, '_equipo_2_imagen', 'field_5a614e987af63'),
(643, 138, 'equipo_2_nombre', 'Juan Carlos Martinez'),
(644, 138, '_equipo_2_nombre', 'field_5a614ede7af64'),
(645, 138, 'equipo_2_descripcion', 'Fundador y CEO de Cranius'),
(646, 138, '_equipo_2_descripcion', 'field_5a614eea7af65'),
(647, 138, 'equipo_3_imagen', '163'),
(648, 138, '_equipo_3_imagen', 'field_5a614e987af63'),
(649, 138, 'equipo_3_nombre', 'Juan Alegre'),
(650, 138, '_equipo_3_nombre', 'field_5a614ede7af64'),
(651, 138, 'equipo_3_descripcion', 'Director de Tecnología y Analytics'),
(652, 138, '_equipo_3_descripcion', 'field_5a614eea7af65'),
(653, 159, 'equipo', '4'),
(654, 159, '_equipo', 'field_5a614e537af62'),
(655, 159, 'equipo_0_imagen', '155'),
(656, 159, '_equipo_0_imagen', 'field_5a614e987af63'),
(657, 159, 'equipo_0_nombre', 'Jhon da Silva'),
(658, 159, '_equipo_0_nombre', 'field_5a614ede7af64'),
(659, 159, 'equipo_0_descripcion', 'Miembro del Consejo Asesor de Cranius.'),
(660, 159, '_equipo_0_descripcion', 'field_5a614eea7af65'),
(661, 159, 'equipo_1_imagen', '156'),
(662, 159, '_equipo_1_imagen', 'field_5a614e987af63'),
(663, 159, 'equipo_1_nombre', 'Macarena Estevez'),
(664, 159, '_equipo_1_nombre', 'field_5a614ede7af64'),
(665, 159, 'equipo_1_descripcion', 'Miembro del Consejo Asesor de Cranius.'),
(666, 159, '_equipo_1_descripcion', 'field_5a614eea7af65'),
(667, 159, 'equipo_2_imagen', '157'),
(668, 159, '_equipo_2_imagen', 'field_5a614e987af63'),
(669, 159, 'equipo_2_nombre', 'Juan Carlos Martinez'),
(670, 159, '_equipo_2_nombre', 'field_5a614ede7af64'),
(671, 159, 'equipo_2_descripcion', 'Fundador y CEO de Cranius'),
(672, 159, '_equipo_2_descripcion', 'field_5a614eea7af65'),
(673, 159, 'equipo_3_imagen', '158'),
(674, 159, '_equipo_3_imagen', 'field_5a614e987af63'),
(675, 159, 'equipo_3_nombre', 'Juan Alegre'),
(676, 159, '_equipo_3_nombre', 'field_5a614ede7af64'),
(677, 159, 'equipo_3_descripcion', 'Director de Tecnología y Analytics'),
(678, 159, '_equipo_3_descripcion', 'field_5a614eea7af65'),
(679, 160, '_wp_attached_file', '2018/01/Jhon.png'),
(680, 160, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:409;s:6:\"height\";i:522;s:4:\"file\";s:16:\"2018/01/Jhon.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"Jhon-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"Jhon-235x300.png\";s:5:\"width\";i:235;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(681, 161, '_wp_attached_file', '2018/01/Macarena.png'),
(682, 161, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:396;s:6:\"height\";i:497;s:4:\"file\";s:20:\"2018/01/Macarena.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"Macarena-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"Macarena-239x300.png\";s:5:\"width\";i:239;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(683, 162, '_wp_attached_file', '2018/01/Juan.png'),
(684, 162, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:411;s:6:\"height\";i:479;s:4:\"file\";s:16:\"2018/01/Juan.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"Juan-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"Juan-257x300.png\";s:5:\"width\";i:257;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(685, 163, '_wp_attached_file', '2018/01/Juan2.png'),
(686, 163, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:392;s:6:\"height\";i:479;s:4:\"file\";s:17:\"2018/01/Juan2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"Juan2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"Juan2-246x300.png\";s:5:\"width\";i:246;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(687, 164, '_wp_attached_file', '2018/01/banner-servicios-1.jpg'),
(688, 164, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1800;s:6:\"height\";i:750;s:4:\"file\";s:30:\"2018/01/banner-servicios-1.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"banner-servicios-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"banner-servicios-1-300x125.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:125;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"banner-servicios-1-768x320.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:320;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:31:\"banner-servicios-1-1024x427.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:427;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(689, 138, '_thumbnail_id', '164'),
(690, 165, 'equipo', '4'),
(691, 165, '_equipo', 'field_5a614e537af62'),
(692, 165, 'equipo_0_imagen', '160'),
(693, 165, '_equipo_0_imagen', 'field_5a614e987af63'),
(694, 165, 'equipo_0_nombre', 'Jhon da Silva'),
(695, 165, '_equipo_0_nombre', 'field_5a614ede7af64'),
(696, 165, 'equipo_0_descripcion', 'Miembro del Consejo Asesor de Cranius.'),
(697, 165, '_equipo_0_descripcion', 'field_5a614eea7af65'),
(698, 165, 'equipo_1_imagen', '161'),
(699, 165, '_equipo_1_imagen', 'field_5a614e987af63'),
(700, 165, 'equipo_1_nombre', 'Macarena Estevez'),
(701, 165, '_equipo_1_nombre', 'field_5a614ede7af64'),
(702, 165, 'equipo_1_descripcion', 'Miembro del Consejo Asesor de Cranius.'),
(703, 165, '_equipo_1_descripcion', 'field_5a614eea7af65'),
(704, 165, 'equipo_2_imagen', '162'),
(705, 165, '_equipo_2_imagen', 'field_5a614e987af63'),
(706, 165, 'equipo_2_nombre', 'Juan Carlos Martinez'),
(707, 165, '_equipo_2_nombre', 'field_5a614ede7af64'),
(708, 165, 'equipo_2_descripcion', 'Fundador y CEO de Cranius'),
(709, 165, '_equipo_2_descripcion', 'field_5a614eea7af65'),
(710, 165, 'equipo_3_imagen', '163'),
(711, 165, '_equipo_3_imagen', 'field_5a614e987af63'),
(712, 165, 'equipo_3_nombre', 'Juan Alegre'),
(713, 165, '_equipo_3_nombre', 'field_5a614ede7af64'),
(714, 165, 'equipo_3_descripcion', 'Director de Tecnología y Analytics'),
(715, 165, '_equipo_3_descripcion', 'field_5a614eea7af65'),
(716, 166, 'pic', '87'),
(717, 166, '_pic', 'field_59bbf45d0d9ae'),
(718, 167, '_edit_lock', '1516373432:1'),
(719, 167, '_edit_last', '1'),
(720, 170, 'pic', '87'),
(721, 170, '_pic', 'field_59bbf45d0d9ae'),
(722, 50, 'formulario', '[contact-form-7 id=\"53\" title=\"Contact form 1\"]'),
(723, 50, '_formulario', 'field_5a62055a84a96'),
(724, 50, 'mapa', ''),
(725, 50, '_mapa', 'field_5a6205a584a97'),
(726, 171, 'pic', '87'),
(727, 171, '_pic', 'field_59bbf45d0d9ae'),
(728, 171, 'formulario', '[contact-form-7 id=\"53\" title=\"Contact form 1\"]'),
(729, 171, '_formulario', 'field_5a62055a84a96'),
(730, 171, 'mapa', ''),
(731, 171, '_mapa', 'field_5a6205a584a97'),
(736, 167, '_wp_trash_meta_status', 'publish'),
(737, 167, '_wp_trash_meta_time', '1516373575'),
(738, 167, '_wp_desired_post_slug', 'group_5a620555de9f1'),
(739, 168, '_wp_trash_meta_status', 'publish'),
(740, 168, '_wp_trash_meta_time', '1516373575'),
(741, 168, '_wp_desired_post_slug', 'field_5a62055a84a96'),
(742, 169, '_wp_trash_meta_status', 'publish'),
(743, 169, '_wp_trash_meta_time', '1516373576'),
(744, 169, '_wp_desired_post_slug', 'field_5a6205a584a97'),
(752, 53, '_config_errors', 'a:1:{s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:51:\"Invalid mailbox syntax is used in the %name% field.\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(753, 173, 'pic', '87'),
(754, 173, '_pic', 'field_59bbf45d0d9ae'),
(755, 173, 'formulario', '[contact-form-7 id=\"53\" title=\"Contact form 1\"]'),
(756, 173, '_formulario', 'field_5a62055a84a96'),
(757, 173, 'mapa', ''),
(758, 173, '_mapa', 'field_5a6205a584a97');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2017-09-14 13:04:44', '2017-09-14 13:04:44', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2017-09-15 14:12:48', '2017-09-15 14:12:48', '', 0, 'http://localhost/cranius/?p=1', 0, 'post', '', 1),
(2, 1, '2017-09-14 13:04:44', '2017-09-14 13:04:44', '<h1>Competir por la atención del consumidor es nuestra obsesión.</h1>\r\nContenidos Inteligentes que aprenden conectan y persuaden a la audiencias en medios sociales, haciendo los mensajes más relevantes y atractivos a los posibles clientes.', 'Inicio', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2018-01-17 20:19:04', '2018-01-17 20:19:04', '', 0, 'http://localhost/cranius/?page_id=2', 0, 'page', '', 0),
(5, 1, '2017-09-14 14:40:44', '2017-09-14 14:40:44', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\r\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</blockquote>\r\n...or something like this:\r\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\r\nAs a new WordPress user, you should go to <a href=\"/cranius/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-09-14 14:40:44', '2017-09-14 14:40:44', '', 2, 'http://localhost/cranius/2017/09/14/2-revision-v1/', 0, 'revision', '', 0),
(6, 1, '2017-09-14 16:45:31', '2017-09-14 16:45:31', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:9:\"index.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'homepage', 'homepage', 'trash', 'closed', 'closed', '', 'group_59bab2262e609__trashed', '', '', '2018-01-18 15:06:41', '2018-01-18 15:06:41', '', 0, 'http://localhost/cranius/?post_type=acf-field-group&#038;p=6', 0, 'acf-field-group', '', 0),
(7, 1, '2017-09-14 17:12:40', '2017-09-14 17:12:40', 'a:9:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:50:\"Texto correspondiente al recuadro debajo del slide\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:13:\"default_value\";s:0:\"\";}', 'feat-text', 'feat-text', 'trash', 'closed', 'closed', '', 'field_59bab4a9a48f2__trashed', '', '', '2018-01-18 15:06:42', '2018-01-18 15:06:42', '', 6, 'http://localhost/cranius/?post_type=acf-field&#038;p=7', 0, 'acf-field', '', 0),
(8, 1, '2017-09-14 17:35:58', '2017-09-14 17:35:58', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'serv_pic', 'serv_pic', 'trash', 'closed', 'closed', '', 'field_59babdc345d85__trashed', '', '', '2018-01-18 15:06:42', '2018-01-18 15:06:42', '', 6, 'http://localhost/cranius/?post_type=acf-field&#038;p=8', 1, 'acf-field', '', 0),
(10, 1, '2017-09-14 17:41:43', '2017-09-14 17:41:43', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</blockquote>\n...or something like this:\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\nAs a new WordPress user, you should go to <a href=\"/cranius/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-autosave-v1', '', '', '2017-09-14 17:41:43', '2017-09-14 17:41:43', '', 2, 'http://localhost/cranius/2017/09/14/2-autosave-v1/', 0, 'revision', '', 0),
(11, 1, '2017-09-14 17:43:23', '2017-09-14 17:43:23', '<h1>Competir por la atención del consumidor es nuestra obsesión.</h1>\r\nContenidos Inteligentes que aprenden conectan y persuaden a la audiencias en medios sociales, haciendo los mensajes más relevantes y atractivos a los posibles clientes.', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-09-14 17:43:23', '2017-09-14 17:43:23', '', 2, 'http://localhost/cranius/2017/09/14/2-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2017-09-14 17:43:39', '2017-09-14 17:43:39', '', 'serv_2', '', 'inherit', 'open', 'closed', '', 'serv_2', '', '', '2017-09-14 17:43:42', '2017-09-14 17:43:42', '', 2, 'http://localhost/cranius/wp-content/uploads/2017/09/serv_2.png', 0, 'attachment', 'image/png', 0),
(13, 1, '2017-09-14 17:43:47', '2017-09-14 17:43:47', '<h1>Competir por la atención del consumidor es nuestra obsesión.</h1>\r\nContenidos Inteligentes que aprenden conectan y persuaden a la audiencias en medios sociales, haciendo los mensajes más relevantes y atractivos a los posibles clientes.', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-09-14 17:43:47', '2017-09-14 17:43:47', '', 2, 'http://localhost/cranius/2017/09/14/2-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2017-09-14 17:48:01', '2017-09-14 17:48:01', 'Nuestra metodologia es un juego entre matematica y creatividad.', 'slide1', '', 'publish', 'closed', 'closed', '', 'slide1', '', '', '2017-09-14 17:53:16', '2017-09-14 17:53:16', '', 0, 'http://localhost/cranius/?post_type=slide&#038;p=14', 0, 'slide', '', 0),
(15, 1, '2017-09-14 17:47:55', '2017-09-14 17:47:55', '', 'cover1', '', 'inherit', 'open', 'closed', '', 'cover1', '', '', '2017-09-14 17:47:55', '2017-09-14 17:47:55', '', 14, 'http://localhost/cranius/wp-content/uploads/2017/09/cover1.png', 0, 'attachment', 'image/png', 0),
(16, 1, '2017-09-14 17:53:23', '2017-09-14 17:53:23', 'Nuestra metodologia es un juego entre matematica y creatividad.', 'slide1', '', 'inherit', 'closed', 'closed', '', '14-autosave-v1', '', '', '2017-09-14 17:53:23', '2017-09-14 17:53:23', '', 14, 'http://localhost/cranius/2017/09/14/14-autosave-v1/', 0, 'revision', '', 0),
(17, 1, '2017-09-14 18:00:53', '2017-09-14 18:00:53', 'Somos una agencia de contenidos guebisticos', 'silde2', '', 'publish', 'closed', 'closed', '', 'silde2', '', '', '2017-09-14 19:24:38', '2017-09-14 19:24:38', '', 0, 'http://localhost/cranius/?post_type=slide&#038;p=17', 0, 'slide', '', 0),
(18, 1, '2017-09-14 18:00:47', '2017-09-14 18:00:47', '', 'slide2', '', 'inherit', 'open', 'closed', '', 'slide2', '', '', '2017-09-14 18:00:47', '2017-09-14 18:00:47', '', 17, 'http://localhost/cranius/wp-content/uploads/2017/09/slide2.png', 0, 'attachment', 'image/png', 0),
(19, 1, '2017-09-14 19:50:36', '2017-09-14 19:50:36', '<div>\r\n<div>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facere atque doloremque quae adipisci ab facilis voluptate ea in possimus culpa, numquam itaque eligendi deleniti rerum, sed ducimus iure amet laboriosam?</div>\r\n</div>', 'Cocosette fudge', '', 'publish', 'closed', 'closed', '', 'cocosette-fudge', '', '', '2017-09-14 19:50:36', '2017-09-14 19:50:36', '', 0, 'http://localhost/cranius/?post_type=trabajos&#038;p=19', 0, 'trabajos', '', 0),
(20, 1, '2017-09-14 19:50:16', '2017-09-14 19:50:16', '', 'cocosette', '', 'inherit', 'open', 'closed', '', 'cocosette', '', '', '2017-09-14 19:50:16', '2017-09-14 19:50:16', '', 19, 'http://localhost/cranius/wp-content/uploads/2017/09/cocosette.png', 0, 'attachment', 'image/png', 0),
(21, 1, '2017-09-14 20:24:30', '2017-09-14 20:24:30', '<div>\r\n<div>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facere atque doloremque quae adipisci ab facilis voluptate ea in possimus culpa, numquam itaque eligendi deleniti rerum, sed ducimus iure amet laboriosam?</div>\r\n</div>', 'hipster', '', 'publish', 'closed', 'closed', '', 'hipster', '', '', '2017-09-14 20:24:30', '2017-09-14 20:24:30', '', 0, 'http://localhost/cranius/?post_type=trabajos&#038;p=21', 0, 'trabajos', '', 0),
(22, 1, '2017-09-14 20:24:25', '2017-09-14 20:24:25', '', 'hipster', '', 'inherit', 'open', 'closed', '', 'hipster', '', '', '2017-09-14 20:24:25', '2017-09-14 20:24:25', '', 21, 'http://localhost/cranius/wp-content/uploads/2017/09/hipster.png', 0, 'attachment', 'image/png', 0),
(23, 1, '2017-09-14 20:24:47', '2017-09-14 20:24:47', '<div>\r\n<div>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facere atque doloremque quae adipisci ab facilis voluptate ea in possimus culpa, numquam itaque eligendi deleniti rerum, sed ducimus iure amet laboriosam?</div>\r\n</div>', 'a sentirse bien', '', 'publish', 'closed', 'closed', '', 'a-sentirse-bien', '', '', '2017-09-14 20:24:47', '2017-09-14 20:24:47', '', 0, 'http://localhost/cranius/?post_type=trabajos&#038;p=23', 0, 'trabajos', '', 0),
(24, 1, '2017-09-14 20:24:44', '2017-09-14 20:24:44', '', 'asb', '', 'inherit', 'open', 'closed', '', 'asb', '', '', '2017-09-14 20:24:44', '2017-09-14 20:24:44', '', 23, 'http://localhost/cranius/wp-content/uploads/2017/09/asb.png', 0, 'attachment', 'image/png', 0),
(25, 1, '2017-09-14 20:25:09', '2017-09-14 20:25:09', '<div>\r\n<div>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facere atque doloremque quae adipisci ab facilis voluptate ea in possimus culpa, numquam itaque eligendi deleniti rerum, sed ducimus iure amet laboriosam?</div>\r\n</div>', 'thanks', '', 'publish', 'closed', 'closed', '', 'thanks', '', '', '2017-09-14 20:25:09', '2017-09-14 20:25:09', '', 0, 'http://localhost/cranius/?post_type=trabajos&#038;p=25', 0, 'trabajos', '', 0),
(26, 1, '2017-09-14 20:24:55', '2017-09-14 20:24:55', '', 'thank', '', 'inherit', 'open', 'closed', '', 'thank', '', '', '2017-09-14 20:24:55', '2017-09-14 20:24:55', '', 25, 'http://localhost/cranius/wp-content/uploads/2017/09/thank.png', 0, 'attachment', 'image/png', 0),
(27, 1, '2017-09-14 20:49:23', '2017-09-14 20:49:23', '<h1>Competir por la atención del consumidor es nuestra obsesión.</h1>\r\nContenidos Inteligentes que aprenden conectan y persuaden a la audiencias en medios sociales, haciendo los mensajes más relevantes y atractivos a los posibles clientes.', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-09-14 20:49:23', '2017-09-14 20:49:23', '', 2, 'http://localhost/cranius/2017/09/14/2-revision-v1/', 0, 'revision', '', 0),
(28, 1, '2017-09-14 20:52:00', '2017-09-14 20:52:00', 'GAIA © Nuestra herramienta que descubre cuantos subtargets se esconden en la audiencia.', 'Nuestra Metodología', '', 'publish', 'closed', 'closed', '', 'metodologia', '', '', '2018-01-18 20:36:27', '2018-01-18 20:36:27', '', 0, 'http://localhost/cranius/?page_id=28', 0, 'page', '', 0),
(29, 1, '2017-09-14 20:52:00', '2017-09-14 20:52:00', '', 'nosotros', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2017-09-14 20:52:00', '2017-09-14 20:52:00', '', 28, 'http://localhost/cranius/2017/09/14/28-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2017-09-15 13:07:21', '2017-09-15 13:07:21', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:9:\"about.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'about', 'about', 'trash', 'closed', 'closed', '', 'group_59bbd074e6cfd__trashed', '', '', '2018-01-18 15:06:38', '2018-01-18 15:06:38', '', 0, 'http://localhost/cranius/?post_type=acf-field-group&#038;p=30', 0, 'acf-field-group', '', 0),
(31, 1, '2017-09-15 13:07:21', '2017-09-15 13:07:21', 'a:9:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;}', 'block2_content', 'block2_content', 'trash', 'closed', 'closed', '', 'field_59bbd07f562c0__trashed', '', '', '2018-01-18 15:06:38', '2018-01-18 15:06:38', '', 30, 'http://localhost/cranius/?post_type=acf-field&#038;p=31', 1, 'acf-field', '', 0),
(32, 1, '2017-09-15 13:10:59', '2017-09-15 13:10:59', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'block3_quote', 'block3_quote', 'trash', 'closed', 'closed', '', 'field_59bbd09910ba7__trashed', '', '', '2018-01-18 15:06:39', '2018-01-18 15:06:39', '', 30, 'http://localhost/cranius/?post_type=acf-field&#038;p=32', 2, 'acf-field', '', 0),
(33, 1, '2017-09-15 13:20:18', '2017-09-15 13:20:18', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'block1_quote', 'block1_quote', 'trash', 'closed', 'closed', '', 'field_59bbd3729a177__trashed', '', '', '2018-01-18 15:06:38', '2018-01-18 15:06:38', '', 30, 'http://localhost/cranius/?post_type=acf-field&#038;p=33', 0, 'acf-field', '', 0),
(34, 1, '2017-09-15 13:22:38', '2017-09-15 13:22:38', 'a:9:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;}', 'block3_content', 'block3_content', 'trash', 'closed', 'closed', '', 'field_59bbd40b83c53__trashed', '', '', '2018-01-18 15:06:39', '2018-01-18 15:06:39', '', 30, 'http://localhost/cranius/?post_type=acf-field&#038;p=34', 3, 'acf-field', '', 0),
(35, 1, '2017-09-15 13:26:34', '2017-09-15 13:26:34', 'Con más de 27 millones de piezas de contenido social publicados diariamente, reinventar la forma de hacer contenido se hace una necesidad. En solo diez años, el panorama ha cambiado drásticamente. El ritmo y la velocidad de los cambios es abrumador, lo sabemos Contenidos Inteligentes ofrece a las marcas el potencial de escala, alcance y orientación a través de un algoritmo sofisticado, para poner el contenido adecuado frente al público adecuado.', 'Somos Cranius', '', 'inherit', 'closed', 'closed', '', '28-autosave-v1', '', '', '2017-09-15 13:26:34', '2017-09-15 13:26:34', '', 28, 'http://localhost/cranius/2017/09/15/28-autosave-v1/', 0, 'revision', '', 0),
(36, 1, '2017-09-15 13:26:49', '2017-09-15 13:26:49', 'Con más de 27 millones de piezas de contenido social publicados diariamente, reinventar la forma de hacer contenido se hace una necesidad. En solo diez años, el panorama ha cambiado drásticamente. El ritmo y la velocidad de los cambios es abrumador, lo sabemos Contenidos Inteligentes ofrece a las marcas el potencial de escala, alcance y orientación a través de un algoritmo sofisticado, para poner el contenido adecuado frente al público adecuado.', 'Somos Cranius', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2017-09-15 13:26:49', '2017-09-15 13:26:49', '', 28, 'http://localhost/cranius/2017/09/15/28-revision-v1/', 0, 'revision', '', 0),
(37, 1, '2017-09-15 13:28:10', '2017-09-15 13:28:10', 'Con más de 27 millones de piezas de contenido social publicados diariamente, reinventar la forma de hacer contenido se hace una necesidad. En solo diez años, el panorama ha cambiado drásticamente. El ritmo y la velocidad de los cambios es abrumador, lo sabemos Contenidos Inteligentes ofrece a las marcas el potencial de escala, alcance y orientación a través de un algoritmo sofisticado, para poner el contenido adecuado frente al público adecuado.', 'Somos Cranius', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2017-09-15 13:28:10', '2017-09-15 13:28:10', '', 28, 'http://localhost/cranius/2017/09/15/28-revision-v1/', 0, 'revision', '', 0),
(38, 1, '2017-09-15 13:36:18', '2017-09-15 13:36:18', 'Juan Carlos es un early adopter y su trayectoria profesional lo demuestra. Es el CCO de La Web, liderando un equipo de más de 50 personas que trabajan con marca nacionales e internacionales. Es responsable de marcas como P.A.N Internacional, Oscar Mayer, Nestlé, Nestea, Savoy, Movistar, Fundación Telefónica. Previo a esto, Juan Carlos trabajó en 2003 Gloo Studio, el primer estudio de desarrollo web en Caracas, que trabajó con las principales agencias de publicidad, como Leo Burnett, DDB, Publicis Groupe. De esas colaboraciones, pudo trabajar en grandes proyectos .com, lanzando al mercado las primeras páginas web del mercado nacional y latinoamericano, como La Electricidad de Caracas, McDonald’s, Pringles, Naturella. Eventualmente, este estudio evoluciona y se convierte agencia digital, en el 2005, cambiando el nombre a El Sofá y junto con Imán, una agencia de publicidad tradicional, desarrolla campañas para Cadbury Adam’s, Chiclets Adam’s, Halls y clientes para los que había trabajado esporádicamente, entran en el rooster de clientes fijos de El Sofa. En el 2007, deciden, junto a su socio jhon Da Silva, emprender la Web. Sus últimos proyectos personales, están un E-Commerce internacional, enfocado en el mercado de Canada y Mexico. Y en 2017, entusiasta de las posibilidades que suponen el Big Data y Machine Learning, lanza Cranius©', 'Andrehyna apellido raro', '', 'publish', 'closed', 'closed', '', 'andrehyna-apellido-raro', '', '', '2017-09-15 13:36:18', '2017-09-15 13:36:18', '', 0, 'http://localhost/cranius/?post_type=equipo&#038;p=38', 0, 'equipo', '', 0),
(39, 1, '2017-09-15 13:36:13', '2017-09-15 13:36:13', '', 'andrehyna', '', 'inherit', 'open', 'closed', '', 'andrehyna', '', '', '2017-09-15 13:36:13', '2017-09-15 13:36:13', '', 38, 'http://localhost/cranius/wp-content/uploads/2017/09/andrehyna.png', 0, 'attachment', 'image/png', 0),
(40, 1, '2017-09-15 13:36:39', '2017-09-15 13:36:39', 'Juan Carlos es un early adopter y su trayectoria profesional lo demuestra. Es el CCO de La Web, liderando un equipo de más de 50 personas que trabajan con marca nacionales e internacionales. Es responsable de marcas como P.A.N Internacional, Oscar Mayer, Nestlé, Nestea, Savoy, Movistar, Fundación Telefónica. Previo a esto, Juan Carlos trabajó en 2003 Gloo Studio, el primer estudio de desarrollo web en Caracas, que trabajó con las principales agencias de publicidad, como Leo Burnett, DDB, Publicis Groupe. De esas colaboraciones, pudo trabajar en grandes proyectos .com, lanzando al mercado las primeras páginas web del mercado nacional y latinoamericano, como La Electricidad de Caracas, McDonald’s, Pringles, Naturella. Eventualmente, este estudio evoluciona y se convierte agencia digital, en el 2005, cambiando el nombre a El Sofá y junto con Imán, una agencia de publicidad tradicional, desarrolla campañas para Cadbury Adam’s, Chiclets Adam’s, Halls y clientes para los que había trabajado esporádicamente, entran en el rooster de clientes fijos de El Sofa. En el 2007, deciden, junto a su socio jhon Da Silva, emprender la Web. Sus últimos proyectos personales, están un E-Commerce internacional, enfocado en el mercado de Canada y Mexico. Y en 2017, entusiasta de las posibilidades que suponen el Big Data y Machine Learning, lanza Cranius©', 'El hipster intenso', '', 'publish', 'closed', 'closed', '', 'el-hipster-intenso', '', '', '2017-09-15 13:36:51', '2017-09-15 13:36:51', '', 0, 'http://localhost/cranius/?post_type=equipo&#038;p=40', 0, 'equipo', '', 0),
(41, 1, '2017-09-15 14:12:39', '2017-09-15 14:12:39', '', 'post-thumb', '', 'inherit', 'open', 'closed', '', 'post-thumb', '', '', '2017-09-15 14:12:39', '2017-09-15 14:12:39', '', 1, 'http://localhost/cranius/wp-content/uploads/2017/09/post-thumb.png', 0, 'attachment', 'image/png', 0),
(42, 1, '2017-09-15 14:12:48', '2017-09-15 14:12:48', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2017-09-15 14:12:48', '2017-09-15 14:12:48', '', 1, 'http://localhost/cranius/2017/09/15/1-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2017-09-15 14:24:48', '2017-09-15 14:24:48', 'En Cranius creamos Contenidos Inteligentes con la ayuda de inteligencia algorítmica, que permite a través de datos, dirigir, canalizar e incrementar la personalización de contenido en las redes sociales de forma eficiente', 'Nuestros Servicios', '', 'publish', 'closed', 'closed', '', 'servicios', '', '', '2018-01-19 01:24:17', '2018-01-19 01:24:17', '', 0, 'http://localhost/cranius/?page_id=43', 0, 'page', '', 0),
(44, 1, '2017-09-15 14:24:48', '2017-09-15 14:24:48', '', 'works', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2017-09-15 14:24:48', '2017-09-15 14:24:48', '', 43, 'http://localhost/cranius/2017/09/15/43-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2017-09-15 14:34:17', '2017-09-15 14:34:17', 'Con más de 27 millones de piezas de contenido social publicados diariamente, reinventar la forma de hacer contenido se hace una necesidad. En solo diez años, el panorama ha cambiado drásticamente. El ritmo y la velocidad de los cambios es abrumador, lo sabemos Contenidos Inteligentes ofrece a las marcas el potencial de escala, alcance y orientación a través de un algoritmo sofisticado, para poner el contenido adecuado frente al público adecuado.', 'works', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2017-09-15 14:34:17', '2017-09-15 14:34:17', '', 43, 'http://localhost/cranius/2017/09/15/43-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2017-09-15 14:37:18', '2017-09-15 14:37:18', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:13:\"portfolio.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'portfolio', 'portfolio', 'trash', 'closed', 'closed', '', 'group_59bbe4f4aec6d__trashed', '', '', '2018-01-18 15:06:43', '2018-01-18 15:06:43', '', 0, 'http://localhost/cranius/?post_type=acf-field-group&#038;p=46', 0, 'acf-field-group', '', 0),
(47, 1, '2017-09-15 14:37:18', '2017-09-15 14:37:18', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'quote', 'quote', 'trash', 'closed', 'closed', '', 'field_59bbe4fd5d3f5__trashed', '', '', '2018-01-18 15:06:43', '2018-01-18 15:06:43', '', 46, 'http://localhost/cranius/?post_type=acf-field&#038;p=47', 0, 'acf-field', '', 0),
(48, 1, '2017-09-15 14:37:39', '2017-09-15 14:37:39', 'Con más de 27 millones de piezas de contenido social publicados diariamente, reinventar la forma de hacer contenido se hace una necesidad. En solo diez años, el panorama ha cambiado drásticamente. El ritmo y la velocidad de los cambios es abrumador, lo sabemos Contenidos Inteligentes ofrece a las marcas el potencial de escala, alcance y orientación a través de un algoritmo sofisticado, para poner el contenido adecuado frente al público adecuado.', 'works', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2017-09-15 14:37:39', '2017-09-15 14:37:39', '', 43, 'http://localhost/cranius/2017/09/15/43-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2017-09-15 14:52:31', '2017-09-15 14:52:31', 'Con más de 27 millones de piezas de contenido social publicados diariamente, reinventar la forma de hacer contenido se hace una necesidad. En solo diez años, el panorama ha cambiado drásticamente. El ritmo y la velocidad de los cambios es abrumador, lo sabemos Contenidos Inteligentes ofrece a las marcas el potencial de escala, alcance y orientación a través de un algoritmo sofisticado, para poner el contenido adecuado frente al público adecuado.', 'portfolio', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2017-09-15 14:52:31', '2017-09-15 14:52:31', '', 43, 'http://localhost/cranius/2017/09/15/43-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2017-09-15 15:09:31', '2017-09-15 15:09:31', '<h3 class=\"roboto-black texto-verde align-center\">¿Curioso?</h3>\r\n<div class=\"empty-space col-xs-b25 col-sm-b50\"></div>\r\n<div class=\"sa col-xs-b10\"><b class=\"roboto-black texto-azul\">Tel.:</b> <a class=\"roboto-regular texto-azul\" href=\"tel:+34 637 54 89\">+34 637 54 89</a></div>\r\n<div class=\"sa col-xs-b10\"><b class=\"roboto-black texto-azul\">Dirección:</b> <span class=\"roboto-regular texto-azul\">Calle Hortaleza, 108 (DCollab) 28004 Madrid </span></div>\r\n<div class=\"sa col-xs-b10\"><b class=\"roboto-black texto-azul\">Email:</b> <a class=\"roboto-regular texto-azul\" href=\"andrehyna@cranius.es\"> andrehyna@cranius.es</a></div>', 'hablemos', '', 'publish', 'closed', 'closed', '', 'hablemos', '', '', '2018-01-19 19:12:37', '2018-01-19 19:12:37', '', 0, 'http://localhost/cranius/?page_id=50', 0, 'page', '', 0),
(51, 1, '2017-09-15 14:54:10', '2017-09-15 14:54:10', '<h1>¿curioso?</h1>\r\n&nbsp;', 'hablemos', '', 'inherit', 'closed', 'closed', '', '50-revision-v1', '', '', '2017-09-15 14:54:10', '2017-09-15 14:54:10', '', 50, 'http://localhost/cranius/2017/09/15/50-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2017-09-15 15:38:56', '2017-09-15 15:38:56', '<div class=\"f-col\">\r\n    <div class=\"entrada\">\r\n    [text nombre class:simple-input class:texto-gris2 class:roboto-regular placeholder \"Nombre\"]\r\n    </div>\r\n    <div class=\"entrada\">\r\n    [email* email class:simple-input class:texto-gris2 class:roboto-regular placeholder \"Email\"]\r\n    </div>\r\n</div>\r\n<div class=\"f-vol\">\r\n<div class=\"entrada\">\r\n[textarea textarea class:simple-input class:texto-gris2 class:roboto-regular placeholder \"Su Mensaje\"]\r\n</div>\r\n</div>\r\n<div class=\"send-button\">[submit class:texto-azul class:roboto-black class:align-left \"Enviar\"]</div>\n1\ncranius \"[your-subject]\"\n[your-name] <jose.leon.avila@hotmail.com>\njose.leon.avila@hotmail.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on cranius (http://localhost/cranius)\nReply-To: [your-email]\n\n\n\n\ncranius \"[your-subject]\"\ncranius <jose.leon.avila@hotmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on cranius (http://localhost/cranius)\nReply-To: jose.leon.avila@hotmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2018-01-19 18:55:27', '2018-01-19 18:55:27', '', 0, 'http://localhost/cranius/?post_type=wpcf7_contact_form&#038;p=53', 0, 'wpcf7_contact_form', '', 0),
(54, 1, '2017-09-15 15:39:17', '2017-09-15 15:39:17', '<h1>¿curioso?</h1>\r\n[contact-form-7 id=\"53\" title=\"Contact form 1\"]', 'hablemos', '', 'inherit', 'closed', 'closed', '', '50-revision-v1', '', '', '2017-09-15 15:39:17', '2017-09-15 15:39:17', '', 50, 'http://localhost/cranius/2017/09/15/50-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2017-09-15 15:40:25', '2017-09-15 15:40:25', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:11:\"contact.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'contact', 'contact', 'trash', 'closed', 'closed', '', 'group_59bbf458f40d0__trashed', '', '', '2018-01-18 15:06:40', '2018-01-18 15:06:40', '', 0, 'http://localhost/cranius/?post_type=acf-field-group&#038;p=55', 0, 'acf-field-group', '', 0),
(56, 1, '2017-09-15 15:40:25', '2017-09-15 15:40:25', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'pic', 'pic', 'trash', 'closed', 'closed', '', 'field_59bbf45d0d9ae__trashed', '', '', '2018-01-18 15:06:40', '2018-01-18 15:06:40', '', 55, 'http://localhost/cranius/?post_type=acf-field&#038;p=56', 0, 'acf-field', '', 0),
(58, 1, '2017-09-15 15:41:15', '2017-09-15 15:41:15', '<h1>¿curioso?</h1>\r\n[contact-form-7 id=\"53\" title=\"Contact form 1\"]', 'hablemos', '', 'inherit', 'closed', 'closed', '', '50-revision-v1', '', '', '2017-09-15 15:41:15', '2017-09-15 15:41:15', '', 50, 'http://localhost/cranius/2017/09/15/50-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2017-09-15 16:43:40', '2017-09-15 16:43:40', ' ', '', '', 'publish', 'closed', 'closed', '', '59', '', '', '2018-01-19 01:45:19', '2018-01-19 01:45:19', '', 0, 'http://localhost/cranius/?p=59', 6, 'nav_menu_item', '', 0),
(60, 1, '2017-09-15 16:43:40', '2017-09-15 16:43:40', '', 'Servicios', '', 'publish', 'closed', 'closed', '', '60', '', '', '2018-01-19 01:45:19', '2018-01-19 01:45:19', '', 0, 'http://localhost/cranius/?p=60', 5, 'nav_menu_item', '', 0),
(61, 1, '2017-09-15 16:43:39', '2017-09-15 16:43:39', '', 'Metodología', '', 'publish', 'closed', 'closed', '', '61', '', '', '2018-01-19 01:45:18', '2018-01-19 01:45:18', '', 0, 'http://localhost/cranius/?p=61', 2, 'nav_menu_item', '', 0),
(62, 1, '2017-09-15 16:43:39', '2017-09-15 16:43:39', ' ', '', '', 'publish', 'closed', 'closed', '', '62', '', '', '2018-01-19 01:45:18', '2018-01-19 01:45:18', '', 0, 'http://localhost/cranius/?p=62', 1, 'nav_menu_item', '', 0),
(63, 1, '2017-09-15 19:41:22', '2017-09-15 19:41:22', 'Con más de 27 millones de piezas de contenido social publicados diariamente, reinventar la forma de hacer contenido se hace una necesidad. En solo diez años, el panorama ha cambiado drásticamente. El ritmo y la velocidad de los cambios es abrumador, lo sabemos Contenidos Inteligentes ofrece a las marcas el potencial de escala, alcance y orientación a través de un algoritmo sofisticado, para poner el contenido adecuado frente al público adecuado.', 'hacemos', '', 'publish', 'closed', 'closed', '', 'hacemos', '', '', '2017-09-15 20:31:47', '2017-09-15 20:31:47', '', 0, 'http://localhost/cranius/?page_id=63', 0, 'page', '', 0),
(64, 1, '2017-09-15 19:41:22', '2017-09-15 19:41:22', '', 'hacemos', '', 'inherit', 'closed', 'closed', '', '63-revision-v1', '', '', '2017-09-15 19:41:22', '2017-09-15 19:41:22', '', 63, 'http://localhost/cranius/2017/09/15/63-revision-v1/', 0, 'revision', '', 0),
(65, 1, '2017-09-15 19:41:51', '2017-09-15 19:41:51', ' ', '', '', 'publish', 'closed', 'closed', '', '65', '', '', '2018-01-19 01:45:19', '2018-01-19 01:45:19', '', 0, 'http://localhost/cranius/?p=65', 4, 'nav_menu_item', '', 0),
(66, 1, '2017-09-15 19:43:07', '2017-09-15 19:43:07', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:12:\"services.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'services', 'services', 'trash', 'closed', 'closed', '', 'group_59bc2d0cafa7b__trashed', '', '', '2018-01-18 15:06:45', '2018-01-18 15:06:45', '', 0, 'http://localhost/cranius/?post_type=acf-field-group&#038;p=66', 0, 'acf-field-group', '', 0),
(67, 1, '2017-09-15 19:47:47', '2017-09-15 19:47:47', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'bloque1_quote', 'bloque1_quote', 'trash', 'closed', 'closed', '', 'field_59bc2e2ebf73c__trashed', '', '', '2018-01-18 15:06:46', '2018-01-18 15:06:46', '', 66, 'http://localhost/cranius/?post_type=acf-field&#038;p=67', 0, 'acf-field', '', 0),
(70, 1, '2017-09-15 20:11:19', '2017-09-15 20:11:19', '\r\n\r\nCrear una visión de marca relevantes y provocadoras en los contextos actuales.\r\n\r\n', 'segmentación semiotica', '', 'publish', 'closed', 'closed', '', 'segmentacion-semiotica', '', '', '2017-09-19 19:33:56', '2017-09-19 19:33:56', '', 0, 'http://localhost/cranius/?post_type=servicios&#038;p=70', 0, 'servicios', '', 0),
(72, 1, '2017-09-15 20:14:02', '2017-09-15 20:14:02', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:9:\"servicios\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'service-detail', 'service-detail', 'trash', 'closed', 'closed', '', 'group_59bc3447c4506__trashed', '', '', '2018-01-18 15:06:44', '2018-01-18 15:06:44', '', 0, 'http://localhost/cranius/?post_type=acf-field-group&#038;p=72', 0, 'acf-field-group', '', 0),
(73, 1, '2017-09-15 20:14:02', '2017-09-15 20:14:02', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'serv_subhead', 'serv_subhead', 'trash', 'closed', 'closed', '', 'field_59bc344e6cf9a__trashed', '', '', '2018-01-18 15:06:45', '2018-01-18 15:06:45', '', 72, 'http://localhost/cranius/?post_type=acf-field&#038;p=73', 0, 'acf-field', '', 0),
(74, 1, '2017-09-15 20:15:39', '2017-09-15 20:15:39', '', 'serv-thumb', '', 'inherit', 'open', 'closed', '', 'serv-thumb', '', '', '2017-09-15 20:15:39', '2017-09-15 20:15:39', '', 70, 'http://localhost/cranius/wp-content/uploads/2017/09/serv-thumb.png', 0, 'attachment', 'image/png', 0),
(75, 1, '2017-09-15 20:24:46', '2017-09-15 20:24:46', '<article>\n<div class=\"content\">\n\nCrear una visión de marca relevantes y provocadoras en los contextos actuales.\n\n</div>\n</article><article></article>', 'segmentación semiotica', '', 'inherit', 'closed', 'closed', '', '70-autosave-v1', '', '', '2017-09-15 20:24:46', '2017-09-15 20:24:46', '', 70, 'http://localhost/cranius/2017/09/15/70-autosave-v1/', 0, 'revision', '', 0),
(76, 1, '2017-09-15 20:27:27', '2017-09-15 20:27:27', 'Con más de 27 millones de piezas de contenido social publicados diariamente, reinventar la forma de hacer contenido se hace una necesidad. En solo diez años, el panorama ha cambiado drásticamente. El ritmo y la velocidad de los cambios es abrumador, lo sabemos Contenidos Inteligentes ofrece a las marcas el potencial de escala, alcance y orientación a través de un algoritmo sofisticado, para poner el contenido adecuado frente al público adecuado.', 'hacemos', '', 'inherit', 'closed', 'closed', '', '63-revision-v1', '', '', '2017-09-15 20:27:27', '2017-09-15 20:27:27', '', 63, 'http://localhost/cranius/2017/09/15/63-revision-v1/', 0, 'revision', '', 0),
(77, 1, '2017-09-18 13:48:47', '2017-09-18 13:48:47', 'Medición y presentación de informes del rendimiento positivo (y negativo) de las marcas en los espacios digitales.', 'Metricas y audiencia', '', 'publish', 'closed', 'closed', '', 'contenidos-intelientes', '', '', '2017-09-18 13:49:31', '2017-09-18 13:49:31', '', 0, 'http://localhost/cranius/?post_type=servicios&#038;p=77', 0, 'servicios', '', 0),
(78, 1, '2017-09-18 13:48:39', '2017-09-18 13:48:39', '', 's11', '', 'inherit', 'open', 'closed', '', 's11', '', '', '2017-09-18 13:48:39', '2017-09-18 13:48:39', '', 77, 'http://localhost/cranius/wp-content/uploads/2017/09/s11.png', 0, 'attachment', 'image/png', 0),
(79, 1, '2017-09-18 13:50:15', '2017-09-18 13:50:15', '', 's1', '', 'inherit', 'open', 'closed', '', 's1', '', '', '2017-09-18 13:50:15', '2017-09-18 13:50:15', '', 70, 'http://localhost/cranius/wp-content/uploads/2017/09/s1.png', 0, 'attachment', 'image/png', 0),
(80, 1, '2017-09-18 13:51:07', '2017-09-18 13:51:07', 'Medición y presentación de informes del rendimiento positivo (y negativo) de las marcas en los espacios digitales.', 'Distribución de contenidos', '', 'publish', 'closed', 'closed', '', 'distribucion-de-contenidos', '', '', '2017-09-18 13:51:07', '2017-09-18 13:51:07', '', 0, 'http://localhost/cranius/?post_type=servicios&#038;p=80', 0, 'servicios', '', 0),
(81, 1, '2017-09-18 13:51:00', '2017-09-18 13:51:00', '', 's3', '', 'inherit', 'open', 'closed', '', 's3', '', '', '2017-09-18 13:51:00', '2017-09-18 13:51:00', '', 80, 'http://localhost/cranius/wp-content/uploads/2017/09/s3.png', 0, 'attachment', 'image/png', 0),
(82, 1, '2017-09-18 13:51:00', '2017-09-18 13:51:00', '', 's2', '', 'inherit', 'open', 'closed', '', 's2', '', '', '2017-09-18 13:51:00', '2017-09-18 13:51:00', '', 80, 'http://localhost/cranius/wp-content/uploads/2017/09/s2.png', 0, 'attachment', 'image/png', 0),
(83, 1, '2017-09-18 13:53:33', '2017-09-18 13:53:33', 'Content is king but Data is Kingdom. Ayudamos a nuestros clientes a encontrar historias relevantes que contar a los consumidores desde la perspectiva de los mismos consumidores, por eso, todo empieza en el research, hurgamos en la data para encontrar insights, que nos ayuden al momento de la creación, que luego se retroalimenta con la data de los resultados obtenidos. Nuestras historias buscan la manera de acercar la verdad de las marcas a la verdad de los consumidores', 'Creatividad', '', 'publish', 'closed', 'closed', '', 'creatividad', '', '', '2017-09-18 13:53:33', '2017-09-18 13:53:33', '', 0, 'http://localhost/cranius/?post_type=servicios&#038;p=83', 0, 'servicios', '', 0),
(84, 1, '2017-09-18 13:53:28', '2017-09-18 13:53:28', '', 'serv-thumb2', '', 'inherit', 'open', 'closed', '', 'serv-thumb2', '', '', '2017-09-18 13:53:28', '2017-09-18 13:53:28', '', 83, 'http://localhost/cranius/wp-content/uploads/2017/09/serv-thumb2.png', 0, 'attachment', 'image/png', 0),
(85, 1, '2017-09-18 13:55:20', '2017-09-18 13:55:20', 'Crear una visión de marca relevantes y provocadoras en los contextos actuales.Crear una visión de marca relevantes y provocadoras en los contextos actuales.', 'Clusterización de audiencias', '', 'publish', 'closed', 'closed', '', 'clusterizacion-de-audiencias', '', '', '2017-09-18 13:55:20', '2017-09-18 13:55:20', '', 0, 'http://localhost/cranius/?post_type=servicios&#038;p=85', 0, 'servicios', '', 0),
(86, 1, '2017-09-18 13:55:58', '2017-09-18 13:55:58', 'Crear una visión de marca relevantes y provocadoras en los contextos actuales.Crear una visión de marca relevantes y provocadoras en los contextos actuales.', 'Estrategia', '', 'publish', 'closed', 'closed', '', 'estrategia', '', '', '2017-09-18 13:55:58', '2017-09-18 13:55:58', '', 0, 'http://localhost/cranius/?post_type=servicios&#038;p=86', 0, 'servicios', '', 0),
(87, 1, '2017-09-19 17:56:47', '2017-09-19 17:56:47', '', 'c-img', '', 'inherit', 'open', 'closed', '', 'c-img', '', '', '2017-09-19 17:56:53', '2017-09-19 17:56:53', '', 50, 'http://localhost/cranius/wp-content/uploads/2017/09/c-img.png', 0, 'attachment', 'image/png', 0),
(88, 1, '2017-09-19 17:56:55', '2017-09-19 17:56:55', '<h1>¿curioso?</h1>\r\n[contact-form-7 id=\"53\" title=\"Contact form 1\"]', 'hablemos', '', 'inherit', 'closed', 'closed', '', '50-revision-v1', '', '', '2017-09-19 17:56:55', '2017-09-19 17:56:55', '', 50, 'http://localhost/cranius/2017/09/19/50-revision-v1/', 0, 'revision', '', 0),
(89, 1, '2018-01-17 19:55:24', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-01-17 19:55:24', '0000-00-00 00:00:00', '', 0, 'http://localhost/cranius/?p=89', 0, 'post', '', 0),
(90, 1, '2018-01-18 14:51:41', '2018-01-18 14:51:41', 'Hacemos\r\ncontenidos\r\ninteligentes.', 'Servicios', '', 'publish', 'open', 'closed', '', 'servicios', '', '', '2018-01-18 15:33:55', '2018-01-18 15:33:55', '', 0, 'http://localhost/cranius/?post_type=slides&#038;p=90', 2, 'slides', '', 0),
(91, 1, '2018-01-18 14:45:22', '2018-01-18 14:45:22', '', 'bombillo', '', 'inherit', 'open', 'closed', '', 'bombillo', '', '', '2018-01-18 14:45:22', '2018-01-18 14:45:22', '', 90, 'http://localhost/cranius/wp-content/uploads/2018/01/bombillo.png', 0, 'attachment', 'image/png', 0),
(92, 1, '2018-01-18 14:51:41', '2018-01-18 14:51:41', '<a href=\"#\">Hacemos\r\ncontenidos\r\ninteligentes.</a>', 'Servicios', '', 'inherit', 'closed', 'closed', '', '90-revision-v1', '', '', '2018-01-18 14:51:41', '2018-01-18 14:51:41', '', 90, 'http://localhost/cranius/2018/01/18/90-revision-v1/', 0, 'revision', '', 0),
(93, 1, '2018-01-18 15:12:27', '2018-01-18 15:12:27', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"slides\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'inicio', 'inicio', 'publish', 'closed', 'closed', '', 'group_5a60b81d31417', '', '', '2018-01-18 15:24:23', '2018-01-18 15:24:23', '', 0, 'http://localhost/cranius/?post_type=acf-field-group&#038;p=93', 0, 'acf-field-group', '', 0),
(94, 1, '2018-01-18 15:12:27', '2018-01-18 15:12:27', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:40:\"Indicar el nombre de la pagina a enlazar\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'enlace', 'enlace', 'publish', 'closed', 'closed', '', 'field_5a60b84fb1fc5', '', '', '2018-01-18 15:12:27', '2018-01-18 15:12:27', '', 93, 'http://localhost/cranius/?post_type=acf-field&p=94', 0, 'acf-field', '', 0),
(95, 1, '2018-01-18 15:22:59', '2018-01-18 15:22:59', 'Hacemos\r\ncontenidos\r\ninteligentes.', 'Servicios', '', 'inherit', 'closed', 'closed', '', '90-revision-v1', '', '', '2018-01-18 15:22:59', '2018-01-18 15:22:59', '', 90, 'http://localhost/cranius/2018/01/18/90-revision-v1/', 0, 'revision', '', 0),
(96, 1, '2018-01-18 15:33:36', '2018-01-18 15:33:36', 'Somos una agencia digital que utiliza inteligencia algorítmica para crear contenido.', 'Equipo', '', 'publish', 'open', 'closed', '', 'equipo', '', '', '2018-01-18 15:33:36', '2018-01-18 15:33:36', '', 0, 'http://localhost/cranius/?post_type=slides&#038;p=96', 1, 'slides', '', 0),
(97, 1, '2018-01-18 15:31:56', '2018-01-18 15:31:56', '', 'slide-1-home', '', 'inherit', 'open', 'closed', '', 'slide-1-home', '', '', '2018-01-18 15:31:56', '2018-01-18 15:31:56', '', 96, 'http://localhost/cranius/wp-content/uploads/2018/01/slide-1-home.png', 0, 'attachment', 'image/png', 0),
(98, 1, '2018-01-18 17:45:53', '2018-01-18 17:45:53', 'Nuestra metodología es un juego entre matemáticas, creatividad, y sentido común.', 'Metodología', '', 'publish', 'open', 'closed', '', 'metodologia', '', '', '2018-01-18 17:46:29', '2018-01-18 17:46:29', '', 0, 'http://localhost/cranius/?post_type=slides&#038;p=98', 3, 'slides', '', 0),
(99, 1, '2018-01-18 17:45:39', '2018-01-18 17:45:39', '', 'rubik-mobile', '', 'inherit', 'open', 'closed', '', 'rubik-mobile', '', '', '2018-01-18 17:45:39', '2018-01-18 17:45:39', '', 98, 'http://localhost/cranius/wp-content/uploads/2018/01/rubik-mobile.png', 0, 'attachment', 'image/png', 0),
(100, 1, '2018-01-18 17:46:23', '2018-01-18 17:46:23', '', 'slide-3-home', '', 'inherit', 'open', 'closed', '', 'slide-3-home', '', '', '2018-01-18 17:46:23', '2018-01-18 17:46:23', '', 98, 'http://localhost/cranius/wp-content/uploads/2018/01/slide-3-home.png', 0, 'attachment', 'image/png', 0),
(101, 1, '2018-01-18 17:54:06', '2018-01-18 17:54:06', '<div class=\"col-sm-7 col-sm-push-5 col-xs-b30 col-sm-b0\">\r\n<div class=\"thumbnail-shortcode-6\">\r\n<div class=\"content\">\r\n<div class=\"layer-2 background\"></div>\r\n</div>\r\n</div>\r\n</div>\r\nGAIA © Nuestra herramienta que descubre cuantos subtargets se esconden en la audiencia.', 'Nuestra Metodología', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-01-18 17:54:06', '2018-01-18 17:54:06', '', 28, 'http://localhost/cranius/2018/01/18/28-revision-v1/', 0, 'revision', '', 0),
(102, 1, '2018-01-18 17:54:48', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-01-18 17:54:48', '0000-00-00 00:00:00', '', 0, 'http://localhost/cranius/?post_type=acf-field-group&p=102', 0, 'acf-field-group', '', 0),
(103, 1, '2018-01-18 17:55:56', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-01-18 17:55:56', '0000-00-00 00:00:00', '', 0, 'http://localhost/cranius/?post_type=acf-field-group&p=103', 0, 'acf-field-group', '', 0),
(104, 1, '2018-01-18 17:58:58', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-01-18 17:58:58', '0000-00-00 00:00:00', '', 0, 'http://localhost/cranius/?post_type=acf-field-group&p=104', 0, 'acf-field-group', '', 0),
(105, 1, '2018-01-18 18:33:12', '2018-01-18 18:33:12', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:15:\"metodologia.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'metodologia', 'metodologia', 'publish', 'closed', 'closed', '', 'group_5a60e076337cf', '', '', '2018-01-18 19:04:40', '2018-01-18 19:04:40', '', 0, 'http://localhost/cranius/?post_type=acf-field-group&#038;p=105', 0, 'acf-field-group', '', 0),
(106, 1, '2018-01-18 18:10:02', '2018-01-18 18:10:02', 'GAIA © Nuestra herramienta que descubre cuantos subtargets se esconden en la audiencia.', 'Nuestra Metodología', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-01-18 18:10:02', '2018-01-18 18:10:02', '', 28, 'http://localhost/cranius/2018/01/18/28-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(107, 1, '2018-01-18 18:23:49', '2018-01-18 18:23:49', '', 'banner-metodologia', '', 'inherit', 'open', 'closed', '', 'banner-metodologia', '', '', '2018-01-18 18:23:49', '2018-01-18 18:23:49', '', 28, 'http://localhost/cranius/wp-content/uploads/2018/01/banner-metodologia.png', 0, 'attachment', 'image/png', 0),
(108, 1, '2018-01-18 18:40:47', '2018-01-18 18:40:47', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'c-img', 'c-img', 'publish', 'closed', 'closed', '', 'field_5a60ea1b11b66', '', '', '2018-01-18 18:40:47', '2018-01-18 18:40:47', '', 105, 'http://localhost/cranius/?post_type=acf-field&p=108', 0, 'acf-field', '', 0),
(109, 1, '2018-01-18 18:47:25', '2018-01-18 18:47:25', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'm-slidem-slide', 'm-slide', 'publish', 'closed', 'closed', '', 'field_5a60ea6db1e89', '', '', '2018-01-18 18:47:25', '2018-01-18 18:47:25', '', 105, 'http://localhost/cranius/?post_type=acf-field&p=109', 1, 'acf-field', '', 0),
(110, 1, '2018-01-18 18:48:34', '2018-01-18 18:48:34', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'imagen', 'imagen', 'publish', 'closed', 'closed', '', 'field_5a60ebe46fc44', '', '', '2018-01-18 19:04:40', '2018-01-18 19:04:40', '', 109, 'http://localhost/cranius/?post_type=acf-field&#038;p=110', 0, 'acf-field', '', 0),
(111, 1, '2018-01-18 18:48:34', '2018-01-18 18:48:34', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'titulo', 'titulo', 'publish', 'closed', 'closed', '', 'field_5a60ebf16fc45', '', '', '2018-01-18 18:48:34', '2018-01-18 18:48:34', '', 109, 'http://localhost/cranius/?post_type=acf-field&p=111', 1, 'acf-field', '', 0),
(112, 1, '2018-01-18 18:48:34', '2018-01-18 18:48:34', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'contenido', 'contenido', 'publish', 'closed', 'closed', '', 'field_5a60ebf66fc46', '', '', '2018-01-18 18:48:34', '2018-01-18 18:48:34', '', 109, 'http://localhost/cranius/?post_type=acf-field&p=112', 2, 'acf-field', '', 0),
(113, 1, '2018-01-18 18:50:53', '2018-01-18 18:50:53', '', 'computer', '', 'inherit', 'open', 'closed', '', 'computer', '', '', '2018-01-18 18:50:55', '2018-01-18 18:50:55', '', 28, 'http://localhost/cranius/wp-content/uploads/2018/01/computer.png', 0, 'attachment', 'image/png', 0),
(114, 1, '2018-01-18 18:50:58', '2018-01-18 18:50:58', 'GAIA © Nuestra herramienta que descubre cuantos subtargets se esconden en la audiencia.', 'Nuestra Metodología', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-01-18 18:50:58', '2018-01-18 18:50:58', '', 28, 'http://localhost/cranius/2018/01/18/28-revision-v1/', 0, 'revision', '', 0),
(115, 1, '2018-01-18 18:53:25', '2018-01-18 18:53:25', '', 'sublime-metodologia', '', 'inherit', 'open', 'closed', '', 'sublime-metodologia', '', '', '2018-01-18 18:53:29', '2018-01-18 18:53:29', '', 28, 'http://localhost/cranius/wp-content/uploads/2018/01/sublime-metodologia.png', 0, 'attachment', 'image/png', 0),
(116, 1, '2018-01-18 18:54:56', '2018-01-18 18:54:56', '', 'banner-2-abajo', '', 'inherit', 'open', 'closed', '', 'banner-2-abajo', '', '', '2018-01-18 18:55:04', '2018-01-18 18:55:04', '', 28, 'http://localhost/cranius/wp-content/uploads/2018/01/banner-2-abajo.png', 0, 'attachment', 'image/png', 0),
(117, 1, '2018-01-18 18:56:04', '2018-01-18 18:56:04', '', 'banner-3-abajo', '', 'inherit', 'open', 'closed', '', 'banner-3-abajo', '', '', '2018-01-18 18:56:05', '2018-01-18 18:56:05', '', 28, 'http://localhost/cranius/wp-content/uploads/2018/01/banner-3-abajo.png', 0, 'attachment', 'image/png', 0),
(118, 1, '2018-01-18 18:56:38', '2018-01-18 18:56:38', 'GAIA © Nuestra herramienta que descubre cuantos subtargets se esconden en la audiencia.', 'Nuestra Metodología', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-01-18 18:56:38', '2018-01-18 18:56:38', '', 28, 'http://localhost/cranius/2018/01/18/28-revision-v1/', 0, 'revision', '', 0),
(119, 1, '2018-01-18 20:36:27', '2018-01-18 20:36:27', 'GAIA © Nuestra herramienta que descubre cuantos subtargets se esconden en la audiencia.', 'Nuestra Metodología', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-01-18 20:36:27', '2018-01-18 20:36:27', '', 28, 'http://localhost/cranius/2018/01/18/28-revision-v1/', 0, 'revision', '', 0),
(120, 1, '2018-01-18 20:46:57', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-01-18 20:46:57', '0000-00-00 00:00:00', '', 0, 'http://localhost/cranius/?post_type=acf-field-group&p=120', 0, 'acf-field-group', '', 0),
(121, 1, '2018-01-18 20:47:29', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-01-18 20:47:29', '0000-00-00 00:00:00', '', 0, 'http://localhost/cranius/?post_type=acf-field-group&p=121', 0, 'acf-field-group', '', 0),
(122, 1, '2018-01-18 20:48:02', '2018-01-18 20:48:02', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:13:\"servicios.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'servicios', 'servicios', 'publish', 'closed', 'closed', '', 'group_5a6107f480976', '', '', '2018-01-18 20:56:55', '2018-01-18 20:56:55', '', 0, 'http://localhost/cranius/?post_type=acf-field-group&#038;p=122', 0, 'acf-field-group', '', 0),
(123, 1, '2018-01-18 20:49:38', '2018-01-18 20:49:38', 'Con más de 27 millones de piezas de contenido social publicados diariamente, reinventar la forma de hacer contenido se hace una necesidad. En solo diez años, el panorama ha cambiado drásticamente. El ritmo y la velocidad de los cambios es abrumador, lo sabemos Contenidos Inteligentes ofrece a las marcas el potencial de escala, alcance y orientación a través de un algoritmo sofisticado, para poner el contenido adecuado frente al público adecuado.', 'Nuestros Servicios', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2018-01-18 20:49:38', '2018-01-18 20:49:38', '', 43, 'http://localhost/cranius/2018/01/18/43-revision-v1/', 0, 'revision', '', 0),
(124, 1, '2018-01-18 20:50:24', '2018-01-18 20:50:24', '', 'banner-servicios', '', 'inherit', 'open', 'closed', '', 'banner-servicios', '', '', '2018-01-18 20:50:24', '2018-01-18 20:50:24', '', 43, 'http://localhost/cranius/wp-content/uploads/2018/01/banner-servicios.jpg', 0, 'attachment', 'image/jpeg', 0),
(125, 1, '2018-01-18 20:51:39', '2018-01-18 20:51:39', '<h2>hacemos contenidos</h2>\nEn Cranius creamos Contenidos Inteligentes con la ayuda de inteligencia algorítmica, que permite a través de datos, dirigir, canalizar e incrementar la personalización de contenido en las redes sociales de forma eficiente', 'Nuestros Servicios', '', 'inherit', 'closed', 'closed', '', '43-autosave-v1', '', '', '2018-01-18 20:51:39', '2018-01-18 20:51:39', '', 43, 'http://localhost/cranius/2018/01/18/43-autosave-v1/', 0, 'revision', '', 0),
(126, 1, '2018-01-18 20:52:29', '2018-01-18 20:52:29', 'En Cranius creamos Contenidos Inteligentes con la ayuda de inteligencia algorítmica, que permite a través de datos, dirigir, canalizar e incrementar la personalización de contenido en las redes sociales de forma eficiente', 'Nuestros Servicios', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2018-01-18 20:52:29', '2018-01-18 20:52:29', '', 43, 'http://localhost/cranius/2018/01/18/43-revision-v1/', 0, 'revision', '', 0),
(127, 1, '2018-01-18 20:52:59', '2018-01-18 20:52:59', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'p-titulo', 'p-titulo', 'publish', 'closed', 'closed', '', 'field_5a61090fa2983', '', '', '2018-01-18 20:52:59', '2018-01-18 20:52:59', '', 122, 'http://localhost/cranius/?post_type=acf-field&p=127', 0, 'acf-field', '', 0),
(128, 1, '2018-01-18 20:53:07', '2018-01-18 20:53:07', 'En Cranius creamos Contenidos Inteligentes con la ayuda de inteligencia algorítmica, que permite a través de datos, dirigir, canalizar e incrementar la personalización de contenido en las redes sociales de forma eficiente', 'Nuestros Servicios', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2018-01-18 20:53:07', '2018-01-18 20:53:07', '', 43, 'http://localhost/cranius/2018/01/18/43-revision-v1/', 0, 'revision', '', 0),
(129, 1, '2018-01-18 20:55:02', '2018-01-18 20:55:02', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:58:\"agregar aquí los servicios con su respectiva información\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'serv-item', 'serv-item', 'publish', 'closed', 'closed', '', 'field_5a61093c6df51', '', '', '2018-01-18 20:55:02', '2018-01-18 20:55:02', '', 122, 'http://localhost/cranius/?post_type=acf-field&p=129', 1, 'acf-field', '', 0),
(130, 1, '2018-01-18 20:55:02', '2018-01-18 20:55:02', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'campo', 'campo', 'publish', 'closed', 'closed', '', 'field_5a6109796df52', '', '', '2018-01-18 20:55:02', '2018-01-18 20:55:02', '', 129, 'http://localhost/cranius/?post_type=acf-field&p=130', 0, 'acf-field', '', 0),
(131, 1, '2018-01-18 20:55:02', '2018-01-18 20:55:02', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'nombre', 'nombre', 'publish', 'closed', 'closed', '', 'field_5a6109846df53', '', '', '2018-01-18 20:55:02', '2018-01-18 20:55:02', '', 129, 'http://localhost/cranius/?post_type=acf-field&p=131', 1, 'acf-field', '', 0),
(132, 1, '2018-01-18 20:55:03', '2018-01-18 20:55:03', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'descripcion', 'descripcion', 'publish', 'closed', 'closed', '', 'field_5a61098d6df54', '', '', '2018-01-18 20:56:55', '2018-01-18 20:56:55', '', 129, 'http://localhost/cranius/?post_type=acf-field&#038;p=132', 2, 'acf-field', '', 0),
(133, 1, '2018-01-18 20:55:03', '2018-01-18 20:55:03', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'imagen', 'imagen', 'publish', 'closed', 'closed', '', 'field_5a61099c6df55', '', '', '2018-01-18 20:55:03', '2018-01-18 20:55:03', '', 129, 'http://localhost/cranius/?post_type=acf-field&p=133', 3, 'acf-field', '', 0),
(134, 1, '2018-01-18 20:56:00', '2018-01-18 20:56:00', '', 'fichas-servicios', '', 'inherit', 'open', 'closed', '', 'fichas-servicios', '', '', '2018-01-18 20:56:02', '2018-01-18 20:56:02', '', 43, 'http://localhost/cranius/wp-content/uploads/2018/01/fichas-servicios.png', 0, 'attachment', 'image/png', 0),
(135, 1, '2018-01-18 20:56:06', '2018-01-18 20:56:06', 'En Cranius creamos Contenidos Inteligentes con la ayuda de inteligencia algorítmica, que permite a través de datos, dirigir, canalizar e incrementar la personalización de contenido en las redes sociales de forma eficiente', 'Nuestros Servicios', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2018-01-18 20:56:06', '2018-01-18 20:56:06', '', 43, 'http://localhost/cranius/2018/01/18/43-revision-v1/', 0, 'revision', '', 0),
(137, 1, '2018-01-19 00:47:43', '2018-01-19 00:47:43', 'En Cranius creamos Contenidos Inteligentes con la ayuda de inteligencia algorítmica, que permite a través de datos, dirigir, canalizar e incrementar la personalización de contenido en las redes sociales de forma eficiente', 'Nuestros Servicios', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2018-01-19 00:47:43', '2018-01-19 00:47:43', '', 43, 'http://localhost/cranius/2018/01/19/43-revision-v1/', 0, 'revision', '', 0),
(138, 1, '2018-01-19 01:37:21', '2018-01-19 01:37:21', '<h3>Un equipo multidisciplinario</h3>\r\n<p class=\"roboto-regular\">Liderado por un Ingeniero Doctor en Matemática, un CEO de Agencias de Medios, una Matemática Estadística, un Creativo y una Estratega, con la única aspiración de ayudar a los clientes a tomar mejores decisiones guiados por la inmensa cantidad de data disponible que se genera en las plataformas sociales</p>', 'Equipo', '', 'publish', 'closed', 'closed', '', 'equipo', '', '', '2018-01-19 14:37:17', '2018-01-19 14:37:17', '', 0, 'http://localhost/cranius/?page_id=138', 0, 'page', '', 0),
(143, 1, '2018-01-19 01:19:48', '2018-01-19 01:19:48', 'En Cranius creamos Contenidos Inteligentes con la ayuda de inteligencia algorítmica, que permite a través de datos, dirigir, canalizar e incrementar la personalización de contenido en las redes sociales de forma eficiente', 'Nuestros Servicios', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2018-01-19 01:19:48', '2018-01-19 01:19:48', '', 43, 'http://localhost/cranius/2018/01/19/43-revision-v1/', 0, 'revision', '', 0),
(144, 1, '2018-01-19 01:37:21', '2018-01-19 01:37:21', '', 'Equipo', '', 'inherit', 'closed', 'closed', '', '138-revision-v1', '', '', '2018-01-19 01:37:21', '2018-01-19 01:37:21', '', 138, 'http://localhost/cranius/2018/01/19/138-revision-v1/', 0, 'revision', '', 0),
(147, 1, '2018-01-19 01:45:18', '2018-01-19 01:45:18', ' ', '', '', 'publish', 'closed', 'closed', '', '147', '', '', '2018-01-19 01:45:18', '2018-01-19 01:45:18', '', 0, 'http://localhost/cranius/?p=147', 3, 'nav_menu_item', '', 0),
(148, 1, '2018-01-19 01:47:03', '2018-01-19 01:47:03', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"post_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"team.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'equipo', 'equipo', 'publish', 'closed', 'closed', '', 'group_5a614dfa17e19', '', '', '2018-01-19 01:51:07', '2018-01-19 01:51:07', '', 0, 'http://localhost/cranius/?post_type=acf-field-group&#038;p=148', 0, 'acf-field-group', '', 0),
(149, 1, '2018-01-19 01:51:06', '2018-01-19 01:51:06', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:33:\"colocar los datos de cada miembro\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'equipo', 'equipo', 'publish', 'closed', 'closed', '', 'field_5a614e537af62', '', '', '2018-01-19 01:51:06', '2018-01-19 01:51:06', '', 148, 'http://localhost/cranius/?post_type=acf-field&p=149', 0, 'acf-field', '', 0),
(150, 1, '2018-01-19 01:51:06', '2018-01-19 01:51:06', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'imagen', 'imagen', 'publish', 'closed', 'closed', '', 'field_5a614e987af63', '', '', '2018-01-19 01:51:06', '2018-01-19 01:51:06', '', 149, 'http://localhost/cranius/?post_type=acf-field&p=150', 0, 'acf-field', '', 0),
(151, 1, '2018-01-19 01:51:06', '2018-01-19 01:51:06', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'nombre', 'nombre', 'publish', 'closed', 'closed', '', 'field_5a614ede7af64', '', '', '2018-01-19 01:51:06', '2018-01-19 01:51:06', '', 149, 'http://localhost/cranius/?post_type=acf-field&p=151', 1, 'acf-field', '', 0),
(152, 1, '2018-01-19 01:51:07', '2018-01-19 01:51:07', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'descripcion', 'descripcion', 'publish', 'closed', 'closed', '', 'field_5a614eea7af65', '', '', '2018-01-19 01:51:07', '2018-01-19 01:51:07', '', 149, 'http://localhost/cranius/?post_type=acf-field&p=152', 2, 'acf-field', '', 0),
(154, 1, '2018-01-19 01:55:05', '2018-01-19 01:55:05', '<h3>Un equipo multidisciplinario</h3>\r\n<p class=\"roboto-regular\">Liderado por un Ingeniero Doctor en Matemática, un CEO de Agencias de Medios, una Matemática Estadística, un Creativo y una Estratega, con la única aspiración de ayudar a los clientes a tomar mejores decisiones guiados por la inmensa cantidad de data disponible que se genera en las plataformas sociales</p>', 'Equipo', '', 'inherit', 'closed', 'closed', '', '138-revision-v1', '', '', '2018-01-19 01:55:05', '2018-01-19 01:55:05', '', 138, 'http://localhost/cranius/2018/01/19/138-revision-v1/', 0, 'revision', '', 0),
(159, 1, '2018-01-19 02:32:13', '2018-01-19 02:32:13', '<h3>Un equipo multidisciplinario</h3>\r\n<p class=\"roboto-regular\">Liderado por un Ingeniero Doctor en Matemática, un CEO de Agencias de Medios, una Matemática Estadística, un Creativo y una Estratega, con la única aspiración de ayudar a los clientes a tomar mejores decisiones guiados por la inmensa cantidad de data disponible que se genera en las plataformas sociales</p>', 'Equipo', '', 'inherit', 'closed', 'closed', '', '138-revision-v1', '', '', '2018-01-19 02:32:13', '2018-01-19 02:32:13', '', 138, 'http://localhost/cranius/2018/01/19/138-revision-v1/', 0, 'revision', '', 0),
(160, 1, '2018-01-19 14:21:48', '2018-01-19 14:21:48', '', 'Jhon', '', 'inherit', 'open', 'closed', '', 'jhon', '', '', '2018-01-19 14:21:50', '2018-01-19 14:21:50', '', 138, 'http://localhost/cranius/wp-content/uploads/2018/01/Jhon.png', 0, 'attachment', 'image/png', 0),
(161, 1, '2018-01-19 14:22:35', '2018-01-19 14:22:35', '', 'Macarena', '', 'inherit', 'open', 'closed', '', 'macarena', '', '', '2018-01-19 14:23:04', '2018-01-19 14:23:04', '', 138, 'http://localhost/cranius/wp-content/uploads/2018/01/Macarena.png', 0, 'attachment', 'image/png', 0),
(162, 1, '2018-01-19 14:22:40', '2018-01-19 14:22:40', '', 'Juan', '', 'inherit', 'open', 'closed', '', 'juan', '', '', '2018-01-19 14:23:14', '2018-01-19 14:23:14', '', 138, 'http://localhost/cranius/wp-content/uploads/2018/01/Juan.png', 0, 'attachment', 'image/png', 0),
(163, 1, '2018-01-19 14:22:41', '2018-01-19 14:22:41', '', 'Juan2', '', 'inherit', 'open', 'closed', '', 'juan2', '', '', '2018-01-19 14:23:22', '2018-01-19 14:23:22', '', 138, 'http://localhost/cranius/wp-content/uploads/2018/01/Juan2.png', 0, 'attachment', 'image/png', 0),
(164, 1, '2018-01-19 14:23:42', '2018-01-19 14:23:42', '', 'banner-servicios', '', 'inherit', 'open', 'closed', '', 'banner-servicios-2', '', '', '2018-01-19 14:23:42', '2018-01-19 14:23:42', '', 138, 'http://localhost/cranius/wp-content/uploads/2018/01/banner-servicios-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(165, 1, '2018-01-19 14:23:48', '2018-01-19 14:23:48', '<h3>Un equipo multidisciplinario</h3>\r\n<p class=\"roboto-regular\">Liderado por un Ingeniero Doctor en Matemática, un CEO de Agencias de Medios, una Matemática Estadística, un Creativo y una Estratega, con la única aspiración de ayudar a los clientes a tomar mejores decisiones guiados por la inmensa cantidad de data disponible que se genera en las plataformas sociales</p>', 'Equipo', '', 'inherit', 'closed', 'closed', '', '138-revision-v1', '', '', '2018-01-19 14:23:48', '2018-01-19 14:23:48', '', 138, 'http://localhost/cranius/2018/01/19/138-revision-v1/', 0, 'revision', '', 0),
(166, 1, '2018-01-19 14:48:23', '2018-01-19 14:48:23', '<h3 class=\"roboto-black texto-verde align-center\">¿Curioso?</h3>\r\n                    \r\n                    <div class=\"empty-space col-xs-b25 col-sm-b50\"></div>\r\n                    <div class=\"sa col-xs-b10\">\r\n                        <b class=\"roboto-black texto-azul\">Tel.:</b> <a href=\"tel:+34911836303\" class=\"roboto-regular texto-azul\">+34 91 183 63 03</a><br/>\r\n                    </div>\r\n                    <div class=\"sa col-xs-b10\">\r\n                        <b class=\"roboto-black texto-azul\">Direcci&oacute;n:</b> <span class=\"roboto-regular texto-azul\">Calle Hortaleza, 108 (DCollab) 28004 Madrid </span><br/>\r\n                    </div>\r\n                    <div class=\"sa col-xs-b10\">\r\n                        <b class=\"roboto-black texto-azul\">Email:</b> <a href=\"andrehyna@cranius.es\" class=\"roboto-regular texto-azul\"> andrehyna@cranius.es</a>\r\n                    </div>\r\n[contact-form-7 id=\"53\" title=\"Contact form 1\"]', 'hablemos', '', 'inherit', 'closed', 'closed', '', '50-revision-v1', '', '', '2018-01-19 14:48:23', '2018-01-19 14:48:23', '', 50, 'http://localhost/cranius/2018/01/19/50-revision-v1/', 0, 'revision', '', 0),
(167, 1, '2018-01-19 14:51:08', '2018-01-19 14:51:08', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:11:\"contact.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'contacto', 'contacto', 'trash', 'closed', 'closed', '', 'group_5a620555de9f1__trashed', '', '', '2018-01-19 14:52:55', '2018-01-19 14:52:55', '', 0, 'http://localhost/cranius/?post_type=acf-field-group&#038;p=167', 0, 'acf-field-group', '', 0),
(168, 1, '2018-01-19 14:51:08', '2018-01-19 14:51:08', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:29:\"colocar el shortcode del mapa\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'formulario', 'formulario', 'trash', 'closed', 'closed', '', 'field_5a62055a84a96__trashed', '', '', '2018-01-19 14:52:55', '2018-01-19 14:52:55', '', 167, 'http://localhost/cranius/?post_type=acf-field&#038;p=168', 0, 'acf-field', '', 0),
(169, 1, '2018-01-19 14:51:08', '2018-01-19 14:51:08', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:40:\"colocar el shortcode del plugin de mapas\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'mapa', 'mapa', 'trash', 'closed', 'closed', '', 'field_5a6205a584a97__trashed', '', '', '2018-01-19 14:52:56', '2018-01-19 14:52:56', '', 167, 'http://localhost/cranius/?post_type=acf-field&#038;p=169', 1, 'acf-field', '', 0),
(170, 1, '2018-01-19 14:51:21', '2018-01-19 14:51:21', '<h3 class=\"roboto-black texto-verde align-center\">¿Curioso?</h3>\r\n                    \r\n                    <div class=\"empty-space col-xs-b25 col-sm-b50\"></div>\r\n                    <div class=\"sa col-xs-b10\">\r\n                        <b class=\"roboto-black texto-azul\">Tel.:</b> <a href=\"tel:+34911836303\" class=\"roboto-regular texto-azul\">+34 91 183 63 03</a><br/>\r\n                    </div>\r\n                    <div class=\"sa col-xs-b10\">\r\n                        <b class=\"roboto-black texto-azul\">Direcci&oacute;n:</b> <span class=\"roboto-regular texto-azul\">Calle Hortaleza, 108 (DCollab) 28004 Madrid </span><br/>\r\n                    </div>\r\n                    <div class=\"sa col-xs-b10\">\r\n                        <b class=\"roboto-black texto-azul\">Email:</b> <a href=\"andrehyna@cranius.es\" class=\"roboto-regular texto-azul\"> andrehyna@cranius.es</a>\r\n                    </div>\r\n', 'hablemos', '', 'inherit', 'closed', 'closed', '', '50-revision-v1', '', '', '2018-01-19 14:51:21', '2018-01-19 14:51:21', '', 50, 'http://localhost/cranius/2018/01/19/50-revision-v1/', 0, 'revision', '', 0),
(171, 1, '2018-01-19 14:51:27', '2018-01-19 14:51:27', '<h3 class=\"roboto-black texto-verde align-center\">¿Curioso?</h3>\r\n                    \r\n                    <div class=\"empty-space col-xs-b25 col-sm-b50\"></div>\r\n                    <div class=\"sa col-xs-b10\">\r\n                        <b class=\"roboto-black texto-azul\">Tel.:</b> <a href=\"tel:+34911836303\" class=\"roboto-regular texto-azul\">+34 91 183 63 03</a><br/>\r\n                    </div>\r\n                    <div class=\"sa col-xs-b10\">\r\n                        <b class=\"roboto-black texto-azul\">Direcci&oacute;n:</b> <span class=\"roboto-regular texto-azul\">Calle Hortaleza, 108 (DCollab) 28004 Madrid </span><br/>\r\n                    </div>\r\n                    <div class=\"sa col-xs-b10\">\r\n                        <b class=\"roboto-black texto-azul\">Email:</b> <a href=\"andrehyna@cranius.es\" class=\"roboto-regular texto-azul\"> andrehyna@cranius.es</a>\r\n                    </div>\r\n', 'hablemos', '', 'inherit', 'closed', 'closed', '', '50-revision-v1', '', '', '2018-01-19 14:51:27', '2018-01-19 14:51:27', '', 50, 'http://localhost/cranius/2018/01/19/50-revision-v1/', 0, 'revision', '', 0),
(172, 1, '2018-01-19 19:12:12', '2018-01-19 19:12:12', '<h3 class=\"roboto-black texto-verde align-center\">¿Curioso?</h3>\n<div class=\"empty-space col-xs-b25 col-sm-b50\"></div>\n<div class=\"sa col-xs-b10\"><b class=\"roboto-black texto-azul\">Tel.:</b> <a class=\"roboto-regular texto-azul\" href=\"tel:+34911836303\">+34 637 54 89+34 91 183 63 03</a></div>\n<div class=\"sa col-xs-b10\"><b class=\"roboto-black texto-azul\">Dirección:</b> <span class=\"roboto-regular texto-azul\">Calle Hortaleza, 108 (DCollab) 28004 Madrid </span></div>\n<div class=\"sa col-xs-b10\"><b class=\"roboto-black texto-azul\">Email:</b> <a class=\"roboto-regular texto-azul\" href=\"andrehyna@cranius.es\"> andrehyna@cranius.es</a></div>', 'hablemos', '', 'inherit', 'closed', 'closed', '', '50-autosave-v1', '', '', '2018-01-19 19:12:12', '2018-01-19 19:12:12', '', 50, 'http://localhost/cranius/2018/01/19/50-autosave-v1/', 0, 'revision', '', 0),
(173, 1, '2018-01-19 19:12:37', '2018-01-19 19:12:37', '<h3 class=\"roboto-black texto-verde align-center\">¿Curioso?</h3>\r\n<div class=\"empty-space col-xs-b25 col-sm-b50\"></div>\r\n<div class=\"sa col-xs-b10\"><b class=\"roboto-black texto-azul\">Tel.:</b> <a class=\"roboto-regular texto-azul\" href=\"tel:+34 637 54 89\">+34 637 54 89</a></div>\r\n<div class=\"sa col-xs-b10\"><b class=\"roboto-black texto-azul\">Dirección:</b> <span class=\"roboto-regular texto-azul\">Calle Hortaleza, 108 (DCollab) 28004 Madrid </span></div>\r\n<div class=\"sa col-xs-b10\"><b class=\"roboto-black texto-azul\">Email:</b> <a class=\"roboto-regular texto-azul\" href=\"andrehyna@cranius.es\"> andrehyna@cranius.es</a></div>', 'hablemos', '', 'inherit', 'closed', 'closed', '', '50-revision-v1', '', '', '2018-01-19 19:12:37', '2018-01-19 19:12:37', '', 50, 'http://localhost/cranius/2018/01/19/50-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'main-menu', 'main-menu', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(59, 2, 0),
(60, 2, 0),
(61, 2, 0),
(62, 2, 0),
(65, 2, 0),
(147, 2, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'light'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'locale', ''),
(11, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(12, 1, 'wp_user_level', '10'),
(13, 1, 'dismissed_wp_pointers', ''),
(14, 1, 'show_welcome_panel', '0'),
(16, 1, 'wp_dashboard_quick_press_last_post_id', '89'),
(17, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:2:\"::\";}'),
(18, 1, 'wp_user-settings', 'editor=html&libraryContent=browse&hidetb=1'),
(19, 1, 'wp_user-settings-time', '1516389156'),
(20, 1, 'closedpostboxes_slide', 'a:2:{i:0;s:11:\"postexcerpt\";i:1;s:10:\"postcustom\";}'),
(21, 1, 'metaboxhidden_slide', 'a:2:{i:0;s:23:\"acf-group_59bab2262e609\";i:1;s:7:\"slugdiv\";}'),
(22, 1, 'closedpostboxes_trabajos', 'a:2:{i:0;s:11:\"postexcerpt\";i:1;s:10:\"postcustom\";}'),
(23, 1, 'metaboxhidden_trabajos', 'a:2:{i:0;s:23:\"acf-group_59bab2262e609\";i:1;s:7:\"slugdiv\";}'),
(24, 1, 'closedpostboxes_equipo', 'a:2:{i:0;s:11:\"postexcerpt\";i:1;s:10:\"postcustom\";}'),
(25, 1, 'metaboxhidden_equipo', 'a:3:{i:0;s:23:\"acf-group_59bbd074e6cfd\";i:1;s:23:\"acf-group_59bab2262e609\";i:2;s:7:\"slugdiv\";}'),
(26, 1, 'meta-box-order_page', 'a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:36:\"submitdiv,pageparentdiv,postimagediv\";s:6:\"normal\";s:142:\"acf-group_59bbd074e6cfd,acf-group_59bab2262e609,acf-group_59bbe4f4aec6d,revisionsdiv,postcustom,commentstatusdiv,commentsdiv,slugdiv,authordiv\";s:8:\"advanced\";s:0:\"\";}'),
(27, 1, 'screen_layout_page', '2'),
(28, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),
(29, 1, 'metaboxhidden_nav-menus', 'a:5:{i:0;s:19:\"add-post-type-slide\";i:1;s:22:\"add-post-type-trabajos\";i:2;s:20:\"add-post-type-equipo\";i:3;s:12:\"add-post_tag\";i:4;s:8:\"add-type\";}'),
(30, 1, 'wpcf7_hide_welcome_panel_on', 'a:1:{i:0;s:3:\"4.9\";}'),
(31, 1, 'session_tokens', 'a:3:{s:64:\"f4cd4a2d5e0ef8249d54f6ef9d73156e0e217b5a5f6a2c9ace4fea8e44055f47\";a:4:{s:10:\"expiration\";i:1516391722;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36\";s:5:\"login\";i:1516218922;}s:64:\"5d2d60f6779482f9e2ed76420abaad0ab6ece70d7280f16e1ad8236e70713d23\";a:4:{s:10:\"expiration\";i:1516491347;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36\";s:5:\"login\";i:1516318547;}s:64:\"e7ab110605a88ebd11288449c728e07a6866e5a6d200387db1d62d525e20224e\";a:4:{s:10:\"expiration\";i:1516544281;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36\";s:5:\"login\";i:1516371481;}}'),
(32, 2, 'nickname', 'maye'),
(33, 2, 'first_name', 'Mayela'),
(34, 2, 'last_name', 'duarte'),
(35, 2, 'description', ''),
(36, 2, 'rich_editing', 'true'),
(37, 2, 'comment_shortcuts', 'false'),
(38, 2, 'admin_color', 'fresh'),
(39, 2, 'use_ssl', '0'),
(40, 2, 'show_admin_bar_front', 'true'),
(41, 2, 'locale', ''),
(42, 2, 'wp_capabilities', 'a:1:{s:6:\"editor\";b:1;}'),
(43, 2, 'wp_user_level', '7'),
(44, 2, 'dismissed_wp_pointers', ''),
(45, 1, 'nav_menu_recently_edited', '2'),
(46, 1, 'closedpostboxes_servicios', 'a:2:{i:0;s:11:\"postexcerpt\";i:1;s:10:\"postcustom\";}'),
(47, 1, 'metaboxhidden_servicios', 'a:8:{i:0;s:23:\"acf-group_59bbd074e6cfd\";i:1;s:23:\"acf-group_59bbf458f40d0\";i:2;s:23:\"acf-group_59bab2262e609\";i:3;s:23:\"acf-group_59bbe4f4aec6d\";i:4;s:23:\"acf-group_59bc2d0cafa7b\";i:5;s:11:\"postexcerpt\";i:6;s:10:\"postcustom\";i:7;s:7:\"slugdiv\";}'),
(48, 1, 'closedpostboxes_slides', 'a:1:{i:0;s:11:\"postexcerpt\";}'),
(49, 1, 'metaboxhidden_slides', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(50, 1, 'acf_user_settings', 'a:0:{}'),
(51, 1, 'closedpostboxes_acf-field-group', 'a:0:{}'),
(52, 1, 'metaboxhidden_acf-field-group', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(53, 1, 'closedpostboxes_page', 'a:0:{}'),
(54, 1, 'metaboxhidden_page', 'a:7:{i:0;s:23:\"acf-group_5a60b81d31417\";i:1;s:23:\"acf-group_5a60e076337cf\";i:2;s:12:\"revisionsdiv\";i:3;s:16:\"commentstatusdiv\";i:4;s:11:\"commentsdiv\";i:5;s:7:\"slugdiv\";i:6;s:9:\"authordiv\";}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$B5z/9R0hrp1kJ4YVg0j4SDxoewGReQ.', 'admin', 'jose.avila@hacemosloquenosgusta.com', 'http://joseavila.tk', '2017-09-14 13:04:43', '', 0, 'admin'),
(2, 'maye', '$P$BZW/3j71WFtFKsq7jixcq1q4avAxFQ/', 'maye', 'mayela@cranius.es', '', '2017-09-15 19:00:43', '1505502044:$P$Bs9QdopYxqDJg0jkjXqhM.Ct2Umgcm/', 0, 'Mayela duarte');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_wpgmza`
--

CREATE TABLE `wp_wpgmza` (
  `id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  `address` varchar(700) NOT NULL,
  `description` mediumtext NOT NULL,
  `pic` varchar(700) NOT NULL,
  `link` varchar(700) NOT NULL,
  `icon` varchar(700) NOT NULL,
  `lat` varchar(100) NOT NULL,
  `lng` varchar(100) NOT NULL,
  `anim` varchar(3) NOT NULL,
  `title` varchar(700) NOT NULL,
  `infoopen` varchar(3) NOT NULL,
  `category` varchar(500) NOT NULL,
  `approved` tinyint(1) DEFAULT '1',
  `retina` tinyint(1) DEFAULT '0',
  `type` tinyint(1) DEFAULT '0',
  `did` varchar(500) NOT NULL,
  `other_data` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `wp_wpgmza`
--

INSERT INTO `wp_wpgmza` (`id`, `map_id`, `address`, `description`, `pic`, `link`, `icon`, `lat`, `lng`, `anim`, `title`, `infoopen`, `category`, `approved`, `retina`, `type`, `did`, `other_data`) VALUES
(2, 1, 'Calle Hortaleza, 108 (DCollab) 28004 Madrid', '', '', '', '', '40.425567', '-3.697367', '0', '', '0', '', 1, 0, 0, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_wpgmza_categories`
--

CREATE TABLE `wp_wpgmza_categories` (
  `id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `category_icon` varchar(700) NOT NULL,
  `retina` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_wpgmza_category_maps`
--

CREATE TABLE `wp_wpgmza_category_maps` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_wpgmza_maps`
--

CREATE TABLE `wp_wpgmza_maps` (
  `id` int(11) NOT NULL,
  `map_title` varchar(55) NOT NULL,
  `map_width` varchar(6) NOT NULL,
  `map_height` varchar(6) NOT NULL,
  `map_start_lat` varchar(700) NOT NULL,
  `map_start_lng` varchar(700) NOT NULL,
  `map_start_location` varchar(700) NOT NULL,
  `map_start_zoom` int(10) NOT NULL,
  `default_marker` varchar(700) NOT NULL,
  `type` int(10) NOT NULL,
  `alignment` int(10) NOT NULL,
  `directions_enabled` int(10) NOT NULL,
  `styling_enabled` int(10) NOT NULL,
  `styling_json` mediumtext NOT NULL,
  `active` int(1) NOT NULL,
  `kml` varchar(700) NOT NULL,
  `bicycle` int(10) NOT NULL,
  `traffic` int(10) NOT NULL,
  `dbox` int(10) NOT NULL,
  `dbox_width` varchar(10) NOT NULL,
  `listmarkers` int(10) NOT NULL,
  `listmarkers_advanced` int(10) NOT NULL,
  `filterbycat` tinyint(1) NOT NULL,
  `ugm_enabled` int(10) NOT NULL,
  `ugm_category_enabled` tinyint(1) NOT NULL,
  `fusion` varchar(100) NOT NULL,
  `map_width_type` varchar(3) NOT NULL,
  `map_height_type` varchar(3) NOT NULL,
  `mass_marker_support` int(10) NOT NULL,
  `ugm_access` int(10) NOT NULL,
  `order_markers_by` int(10) NOT NULL,
  `order_markers_choice` int(10) NOT NULL,
  `show_user_location` int(3) NOT NULL,
  `default_to` varchar(700) NOT NULL,
  `other_settings` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `wp_wpgmza_maps`
--

INSERT INTO `wp_wpgmza_maps` (`id`, `map_title`, `map_width`, `map_height`, `map_start_lat`, `map_start_lng`, `map_start_location`, `map_start_zoom`, `default_marker`, `type`, `alignment`, `directions_enabled`, `styling_enabled`, `styling_json`, `active`, `kml`, `bicycle`, `traffic`, `dbox`, `dbox_width`, `listmarkers`, `listmarkers_advanced`, `filterbycat`, `ugm_enabled`, `ugm_category_enabled`, `fusion`, `map_width_type`, `map_height_type`, `mass_marker_support`, `ugm_access`, `order_markers_by`, `order_markers_choice`, `show_user_location`, `default_to`, `other_settings`) VALUES
(1, 'My first map', '100', '70', '40.425567', '-3.697367', '40.425567,-3.6973669999999856', 17, '0', 1, 1, 1, 0, '', 0, '', 2, 2, 1, '100', 0, 0, 0, 0, 0, '', '\\%', '\\%', 1, 0, 1, 2, 0, '', 'a:13:{s:21:\"store_locator_enabled\";i:2;s:22:\"store_locator_distance\";i:2;s:28:\"store_locator_default_radius\";s:2:\"10\";s:31:\"store_locator_not_found_message\";s:52:\"No results found in this location. Please try again.\";s:20:\"store_locator_bounce\";i:1;s:26:\"store_locator_query_string\";s:14:\"ZIP / Address:\";s:29:\"store_locator_default_address\";s:0:\"\";s:29:\"wpgmza_store_locator_restrict\";s:0:\"\";s:12:\"map_max_zoom\";s:1:\"1\";s:15:\"transport_layer\";i:2;s:17:\"wpgmza_theme_data\";s:113:\"[ \\\"visibility\\\", \\\"invert_lightness\\\", \\\"color\\\", \\\"weight\\\", \\\"hue\\\", \\\"saturation\\\", \\\"lightness\\\", \\\"gamma\\\"]\";s:22:\"wpgmza_theme_selection\";i:0;s:30:\"wpgmza_show_points_of_interest\";i:1;}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_wpgmza_polygon`
--

CREATE TABLE `wp_wpgmza_polygon` (
  `id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  `polydata` longtext NOT NULL,
  `innerpolydata` longtext NOT NULL,
  `linecolor` varchar(7) NOT NULL,
  `lineopacity` varchar(7) NOT NULL,
  `fillcolor` varchar(7) NOT NULL,
  `opacity` varchar(3) NOT NULL,
  `title` varchar(250) NOT NULL,
  `link` varchar(700) NOT NULL,
  `ohfillcolor` varchar(7) NOT NULL,
  `ohlinecolor` varchar(7) NOT NULL,
  `ohopacity` varchar(3) NOT NULL,
  `polyname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_wpgmza_polylines`
--

CREATE TABLE `wp_wpgmza_polylines` (
  `id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  `polydata` longtext NOT NULL,
  `linecolor` varchar(7) NOT NULL,
  `linethickness` varchar(3) NOT NULL,
  `opacity` varchar(3) NOT NULL,
  `polyname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indices de la tabla `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indices de la tabla `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indices de la tabla `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indices de la tabla `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indices de la tabla `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indices de la tabla `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indices de la tabla `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indices de la tabla `wp_wpgmza`
--
ALTER TABLE `wp_wpgmza`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `wp_wpgmza_categories`
--
ALTER TABLE `wp_wpgmza_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `wp_wpgmza_category_maps`
--
ALTER TABLE `wp_wpgmza_category_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `wp_wpgmza_maps`
--
ALTER TABLE `wp_wpgmza_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `wp_wpgmza_polygon`
--
ALTER TABLE `wp_wpgmza_polygon`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `wp_wpgmza_polylines`
--
ALTER TABLE `wp_wpgmza_polylines`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=376;
--
-- AUTO_INCREMENT de la tabla `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=759;
--
-- AUTO_INCREMENT de la tabla `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;
--
-- AUTO_INCREMENT de la tabla `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT de la tabla `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `wp_wpgmza`
--
ALTER TABLE `wp_wpgmza`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `wp_wpgmza_categories`
--
ALTER TABLE `wp_wpgmza_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `wp_wpgmza_category_maps`
--
ALTER TABLE `wp_wpgmza_category_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `wp_wpgmza_maps`
--
ALTER TABLE `wp_wpgmza_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `wp_wpgmza_polygon`
--
ALTER TABLE `wp_wpgmza_polygon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `wp_wpgmza_polylines`
--
ALTER TABLE `wp_wpgmza_polylines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
