<?php
  //standarts
		global $wp_rewrite;
		$wp_rewrite->flush_rules();
		define( 'THEMEPATH',  get_bloginfo('stylesheet_directory'));
		define( 'IMAGES', THEMEPATH. "/img/");
		define( 'SCRIPTS', THEMEPATH. "/js/");
		function new_excerpt_length($length) {
		    return 5;
		}

		add_filter('excerpt_length', 'new_excerpt_length');
		add_filter('widget_text', 'do_shortcode');
		add_theme_support( 'post-thumbnails' );

		function cc_mime_types($mimes) {
			$mimes['svg'] = 'image/svg+xml';
			return $mimes;
		}
		add_filter('upload_mimes', 'cc_mime_types');

	// paginacion
		function pagination_bar() {
				global $wp_query;
		
				$total_pages = $wp_query->max_num_pages;
		
				if ($total_pages > 1){
						$current_page = max(1, get_query_var('paged'));
		
						echo paginate_links(array(
								'base' => get_pagenum_link(1) . '%_%',
								'format' => '/page/%#%',
								'current' => $current_page,
								'total' => $total_pages,
						));
				}
		}
	
	
	// sidebar
		add_action( 'widgets_init', 'theme_slug_widgets_init' );
		function theme_slug_widgets_init() {
			register_sidebar( array(
					'name' => __( 'main', 'theme-slug' ),
					'id' => 'sidebar-1',
					'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
					'class' => 'sidebar-entry',
					'before_widget' => '<div id="%1$s" class="sidebar-entry %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<div class="sidebar-title h6">',
					'after_title'   => '</div>',
				) );
		}

		add_action( 'widgets_init2', 'foot_area_one' );

		

  //menus
		add_action( 'init', 'register_my_menus' );
		function register_my_menus() {
			register_nav_menus(
				array(
					'main-nav' => __( 'Primary Menu' ),
					'foot-nav' => __( 'Sec Menu' )
				)
			);
		}
		

  // menu principal
		function menu(){

			wp_nav_menu( array(
					'theme_location' => 'main-nav',
					'menu' => '',
					'container' => 'nav',
					'container_class' => 'main-menu',
					'container_id' => '',
					'menu_class' => 'nav-menu',
					'menu_id' => '',
					'echo' => true,
					'fallback_cb' => 'wp_page_menu',
					'before' => '',
					'after' => '',
					'link_before' => '',
					'link_after' => '',
					'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
					'depth' => 0,
					'walker' => ''
			));

		}
		add_shortcode('main-nav', 'menu');

  // menu secundario
    function footmenu(){

      wp_nav_menu( array(
          'theme_location' => 'foot-nav',
          'menu' => '',
          'container' => '',
          'container_class' => '',
          'container_id' => '',
          'menu_class' => 'nav-menu',
          'menu_id' => '',
          'echo' => true,
          'fallback_cb' => 'wp_page_menu',
          'before' => '',
          'after' => '',
          'link_before' => '',
          'link_after' => '',
          'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
          'depth' => 0,
          'walker' => ''
      ));

    }
		add_shortcode('foot-nav',  'footmenu');

	// register scripts & styles
		// styles
			function add_styles(){
				wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css');
				wp_enqueue_style('bootext', get_stylesheet_directory_uri() . '/css/bootstrap.extension.css');
				wp_enqueue_style('swiper', get_stylesheet_directory_uri() . '/css/swiper.css');
				wp_enqueue_style('awesome', get_stylesheet_directory_uri() . '/css/font-awesome.min.css');
				wp_enqueue_style('playfair', 'https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900|Roboto:400,500,700,900');
				wp_enqueue_style('montserrat', 'https://fonts.googleapis.com/css?family=Montserrat:400,500,700,900');
			}
		add_action( 'wp_enqueue_scripts', 'add_styles' );

		// scripts
				function my_scripts_method() {
					//wp_deregister_script('jquery');
					wp_enqueue_script('jqlib', '//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js', array('jquery'), '', true);
					wp_enqueue_script('swiper', get_stylesheet_directory_uri() . '/js/swiper.jquery.min.js', array('jquery'), '', true);
					wp_enqueue_script('global', get_stylesheet_directory_uri() . '/js/global.js', array('jquery'), '', true);

				}
		add_action( 'wp_enqueue_scripts', 'my_scripts_method' );
		


	// register slides
		function slide_post_type() {
			$labels = array(
				'name'                => __( 'slides', 'slides', 'text_domain' ),
				'singular_name'       => __( 'slide', 'slide', 'text_domain' ),
				'menu_name'           => __( 'slides', 'text_domain' ),
				'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
				'all_items'           => __( 'Todas los slides', 'text_domain' ),
				'view_item'           => __( 'Ver slide', 'text_domain' ),
				'add_new_item'        => __( 'Agregar slide', 'text_domain' ),
				'add_new'             => __( 'Nuevo slide', 'text_domain' ),
				'edit_item'           => __( 'Edit', 'text_domain' ),
				'update_item'         => __( 'Actualizar slide', 'text_domain' ),
				'search_items'        => __( 'Buscar slide', 'text_domain' ),
				'not_found'           => __( 'no encontrado', 'text_domain' ),
				'not_found_in_trash'  => __( 'no se encontro en la papelera', 'text_domain' ),

			);

			$args = array(

				'label'               => __( 'slides', 'text_domain' ),
				'description'         => __( 'slides', 'text_domain' ),
				'labels'              => $labels,
				'supports'            => array( 'title', 'editor', 'thumbnail', 'comments', 'custom-fields', 'page-attributes' ),
				'taxonomies'          => array(),
				'hierarchical'        => false,
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_nav_menus'   => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => 4,
				'menu_icon'           => 'dashicons-slides',
				'can_export'          => true,
				'has_archive'         => 'post',
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'capability_type'     => 'post',



			);

			register_post_type( 'slides', $args );



		}
		// Hook into the 'init' action
		add_action( 'init', 'slide_post_type', 0 );


// social share

function share_buttons($content) {
	$shortURL = get_permalink();
	
	// Get current page title
	$shortTitle = get_the_title();
	
	// Construct sharing URL without using any script
	$twitterURL = 'https://twitter.com/intent/tweet?text='.$shortTitle.'&amp;url='.$shortURL.'&amp;via=somoscranius';
	$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$shortURL;
	$googleURL = 'https://plus.google.com/share?url='.$shortURL;
	$bufferURL = 'https://bufferapp.com/add?url='.$shortURL.'&amp;text='.$shortTitle;

	// Add sharing button at the end of page/page content
	$content .= '<div class="share follow style-1">';
	$content .= '<div class="title h6">Comparte este articulo! </div> <a class="entry" href="'. $twitterURL .'" target="_blank"><i class="fa fa-twitter"></i></a>';
	$content .= '<a class="entry" href="'.$facebookURL.'" target="_blank"><i class="fa fa-facebook"></i></a>';
	$content .= '</div>';
	return $content;
};
add_shortcode( 'social_share', 'share_buttons');

?>