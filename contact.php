<?php /*Template name: contacto*/?>
<?php get_header();?>
  <div class="container-fluid wide">
    <?php if( have_posts() ) : while( have_posts() ) : the_post();?>
    <div class="row">
        <div class="col-md-6 col-lg-4 col-lg-offset-1">
            <div class="cell-view full-screen-height text-center">
                <div class="empty-space col-xs-b30"></div>
                <article class="sa">
                    <?php the_content(); ?>
                </article>
                    <div class="empty-space col-xs-b30"></div>
                    <div class="follow style-1">
                        <a class="entry" href="https://www.instagram.com/somoscranius" target="_blank"><i class="fa fa-instagram"></i></a>
                        <a class="entry" href="https://www.facebook.com/somoscranius" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a class="entry" href="https://twitter.com/somoscranius" target="_blank"><i class="fa fa-twitter"></i></a>
                    
                    </div>
                <div class="empty-space col-xs-b25 col-sm-b50"></div>
                <!-- formulario --> 
                <div class="contact-form">
                    <?php echo do_shortcode('[contact-form-7 id="53" title="Contact form 1"]')?>
                </div>

                <div class="empty-space col-xs-b30"></div>
            </div>
        </div>
    </div>

    <!-- mapa -->
    <div class="map-inst style-1" id="map-canvas" data-lat="40.425558" data-lng="-3.697371" data-zoom="19"></div>
    <div class="addresses-block hidden" data-rel="map-canvas">
        <a class="marker" data-lat="40.425558" data-lng="-3.697371" data-string="1. Dcollab Espacio de coworking..."></a>
    </div>
    <div class="map-inst style-1" id="map-canvas">
        <?php echo do_shortcode('[wpgmza id="1"]')?>
    </div>
    <?php endwhile; endif;?>
  </div>
<?php get_footer();?>