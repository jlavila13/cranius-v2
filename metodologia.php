<?php /*Template name: metodologia*/ ?>
<?php get_header(); ?>
    <?php if(have_posts() ) : while(have_posts() ) : the_post();?>
    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
    <div class="fixed-background" style="background-image: url(<?php echo $url ?>);">
            <div class="banner-shortcode">
                <div class="banner-frame border-image" style="border-image-source: url(<?php print IMAGES;?>assets/frame-rojo.jpg);"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="align">
                                <h1 class="montserrat-regular texto-blanco">Nuestra <br> <p class="roboto-black texto-blanco">Metodología</p></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="empty-space col-xs-b45 col-sm-b90"></div>

            <div class="row">
                <div class="col-xs-12 col-sm-5">
                    <a href="#" class="client-logo mouseover-2">
                        <img src="<?php print IMAGES;?>assets/cuadro-1-metodologia.png" alt="Logo Metodologia 1" />
                    </a>
                </div>

                <div class="col-xs-12 col-sm-2">
                    <a href="#" class="client-logo mouseover-2">
                        <img class="flecha-right-mobile" src="<?php print IMAGES;?>assets/flecha-right.png" alt="Logo metodologia Flecha" />
                    </a>
                </div>


                <div class="col-xs-12 col-sm-5">
                    <a href="#" class="client-logo mouseover-2">
                        <img src="<?php print IMAGES;?>assets/cuadro-2-metodologia.png" alt="Logo Metodologia 2" />
                    </a>
                </div>

            </div>    
                <!-- div class="row col-xs-12 col-sm-12 mt-3 mb-3">


                    <div class="col-xs-12 col-sm-7">
                        <a href="#" class="client-logo mouseover-2"><img src="<?php print IMAGES;?>assets/cuadro-3-metodologia.png" alt="Logo Metodologia 3" /></a>
                    </div>

                    <div class="col-xs-12 col-sm-5">
                        <a href="#" class="client-logo mouseover-2"><img src="<?php print IMAGES;?>assets/cuadro-4-metodologia.png" alt="Logo Metodologia 4" /></a>
                    </div>
                </div> -->

            <div class="row">

                    <div class="col-xs-12 col-sm-3">
                        <a href="#" class="client-logo mouseover-2"><img src="<?php print IMAGES;?>assets/cuadro-5-metodologia.png" alt="Logo Metodologia 5" /></a>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <a href="#" class="client-logo mouseover-2"><img src="<?php print IMAGES;?>assets/cuadro-8-metodologia.png" alt="Logo Metodologia 6" /></a>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <a href="#" class="client-logo mouseover-2"><img src="<?php print IMAGES;?>assets/cuadro-6-metodologia.png" alt="Logo Metodologia 7" /></a>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <a href="#" class="client-logo mouseover-2"><img src="<?php print IMAGES;?>assets/cuadro-7-metodologia.png" alt="Logo Metodologia 8" /></a>
                    </div>
            </div>

        </div>
    </div>

    <div class="empty-space col-xs-b60 col-sm-b120"></div>

    <div class="empty-space col-xs-b45 col-sm-b90"></div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <article class="sa">
                    <h3>Algoritmo de grupo</h3>

                </article>
                <div class="empty-space col-xs-b25 col-sm-b50"></div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">

            <div class="col-xs-12 col-sm-3">
                <span onclick="javascript:;" class="client-logo mouseover-2">
                    <img src="<?php print IMAGES;?>assets/G-metodologia.png" alt="Letra G" />
                </span>
                <div class="description align-center">
                    <h6 class="roboto-black">GROUPING</h6>

                </div>
            </div>

            <div class="col-xs-12 col-sm-3">
                <span onclick="javascript:;" class="client-logo mouseover-2">
                    <img src="<?php print IMAGES;?>assets/A-metodologia.png" alt="Letra A" />
                </span>
                <div class="description align-center">
                    <h6 class="roboto-black">AUDIENCE</h6>

                </div>
            </div>

             <div class="col-xs-12 col-sm-3">
                <span onclick="javascript:;" class="client-logo mouseover-2">
                        <img src="<?php print IMAGES;?>assets/I-metodologia.png" alt="Letra I" />
                </span>
                 <div class="description align-center">
                    <h6 class="roboto-black">INTELLIGENCE</h6>

                </div>
            </div>

             <div class="col-xs-12 col-sm-3">
                <span onclick="javascript:;" class="client-logo mouseover-2">
                    <img src="<?php print IMAGES;?>assets/A2-metodologia.png" alt="Letra A" />
                </span>
                <div class="description align-center">
                    <h6 class="roboto-black">ALGORITHYM</h6>

                </div>
            </div>

        
        </div>


        <div class="empty-space col-xs-b45 col-sm-b90"></div>

        <div class="swiper-slide">
            <div class="row vertical-aligned-columns">
                <div class="col-sm-7 col-sm-push-5 col-xs-b30 col-sm-b0">
                    <div class="thumbnail-shortcode-6">
                        <div class="content">
                            <div class="layer-1 border border-image" style="border-image-source: url(<?php print IMAGES;?>assets/frame-rojo.jpg);"></div>
                            <div class="layer-2 background" style="background-image: url(<?php the_field('c-img'); ?>);"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-pull-7">
                    <div class="sa">

                        <p class="roboto-regular texto-gris"><?php the_content(); ?></p>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slide repeater -->
    <div class="container">
        <div class="empty-space col-xs-b45 col-sm-b90"></div>
        <div class="swiper-entry">
            <div class="swiper-button-prev visible-lg"></div>
            <div class="swiper-button-next visible-lg"></div>
            <div class="swiper-container" data-space="30">
                    <div class="swiper-wrapper">
                        <?php if (have_rows('m-slide')) : while (have_rows('m-slide')) : the_row();?>
                        <div class="swiper-slide">
                            <div class="row vertical-aligned-columns slide-holder">
                                <div class="col-sm-7  col-xs-b30 col-sm-b0">
                                    <div class="thumbnail-shortcode-6">
                                        <div class="content">
                                            <div class="layer-1 border border-image" style="border-image-source: url(<?php print IMAGES;?>assets/frame-rojo.jpg);"></div>
                                            <div class="layer-2 background" style="background-image: url(<?php the_sub_field('imagen'); ?>);"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="sa">
                                        <h3 class="roboto-black texto-azul"><?php the_sub_field('titulo'); ?></h3>

                                        <p class="roboto-regular texto-gris"><?php the_sub_field('contenido'); ?></p>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; endif; ?>
                        <div class="swiper-pagination relative-pagination hidden-lg"></div>
                    </div>
            </div>
        </div>
    </div>
    <!-- footer -->
    <?php endwhile; endif; ?>
<?php get_footer(); ?>