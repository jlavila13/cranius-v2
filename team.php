<?php /*Template name: equipo*/?>
<?php get_header(); ?>
<?php if( have_posts() ) : while( have_posts() ) : the_post();?>
    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
  <div class="fixed-background" style="background-image: url(<?php echo $url ?>">
      <div class="banner-shortcode">
          <div class="banner-frame border-image" style="border-image-source: url(<?php print IMAGES; ?>assets/frame-rojo.jpg);"></div>
          <div class="container">
              <div class="row">
                  <div class="col-md-8 col-md-offset-2">
                      <div class="align">
                          <h1 class="montserrat-regular texto-blanco">CONOCE A <br> <p class="roboto-black">NUESTRO TEAM</p></h1>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <div class="container">
      <div class="empty-space col-xs-b60 col-sm-b120"></div>
      <div class="row vertical-aligned-columns">
          <div class="col-sm-7 col-sm-push-5 col-xs-b30 col-sm-b0">
              <div class="thumbnail-shortcode-4">
                  <div class="content">
                      <div class="layer-1 background" style="background-image: url(<?php print IMAGES; ?>assets/binary-equipo.png);"></div>
                      <div class="layer-2 border border-image" style="border-image-source: url(<?php print IMAGES; ?>assets/frame-rojo.jpg);"></div>
                      <div class="layer-3 background" style="background-image: url(<?php print IMAGES; ?>assets/hola-equipo.png);"></div>
                  </div>
              </div>
          </div>
          <div class="col-sm-5 col-sm-pull-7">
              <div class="sa">
                  
                  <p class="roboto-regular">
                    <?php the_content();?>
                  </p>
              </div>
          </div>
      </div>
      <div class="empty-space col-xs-b45 col-sm-b90"></div>
      <div class="row">
          <div class="col-md-12 text-center">
              <article class="sa">
                  <h3 class="roboto-bold">Nuestros talentos</h3>
              </article>
              <div class="empty-space col-xs-b25 col-sm-b50"></div>
          </div>
      </div>
      <div class="swiper-entry">
          <div class="swiper-container" data-breakpoints="1" data-xs-slides="1" data-sm-slides="2" data-md-slides="2" data-lt-slides="3" data-slides-per-view="3" data-space="30">
						<div class="swiper-button-prev hidden"></div>
						<div class="swiper-button-next hidden"></div>
						<div class="swiper-wrapper">
								<?php if(have_rows('equipo') ) : while(have_rows('equipo') ) : the_row(); ?>
								<div class="swiper-slide"> 
										<div class="thumbnail-shortcode-5">
											<div class="content">

													<div class="layer-2"><img src="<?php the_sub_field('imagen'); ?>" alt="<?php the_sub_field('nombre'); ?>" /></div>
											</div>
											<div class="description">
													<h6 class="roboto-black texto-azul"><?php the_sub_field('nombre'); ?></h6>
													<div class="sa small montserrat-regular texto-gris"><?php the_sub_field('descripcion'); ?></div>
											</div>
											<div class="animation follow style-1">
												<!--<a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>-->
												<!--<a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>-->
													<!--<a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>-->
													<!-- <a class="entry" href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a> -->
											</div>
										</div>
								</div>
								<?php endwhile; endif; ?>
						</div>
					</div>
      <div class="empty-space col-xs-b45 col-sm-b90"></div>
      <div class="empty-space col-xs-b45 col-sm-b90"></div>
      <div class="row">
          <div class="col-md-12 text-center">
              <article class="sa">
                  <h3 class="roboto-bold">Con experiencia en</h3>
                  <p class="roboto-regular">Lima, Madrid, Bogotá, Caracas, Santiago de Chile, Ciudad de México</p>
                  
              </article>
              <div class="empty-space col-xs-b25 col-sm-b50"></div>
          </div>
      </div>
      <div class="row">

          <div class="equipo-shortcode-1">
              <div class="preview-wrapper col-xs-12 col-sm-12">
                  <div class="icon"></div>
                  <div class="icon2"></div>
                  <div class="icon3"></div>
                  <div class="icon4"></div>
                  <div class="preview">
                      <span class="text-mask mouseover-2" style="background-image: url(<?php print IMAGES; ?>assets/mapa-team.png);"></span>
                  </div> <!-- aqui -->
              </div>
          </div>

          
      </div>
  </div>
<?php endwhile; endif;?>
<?php get_footer(); ?>