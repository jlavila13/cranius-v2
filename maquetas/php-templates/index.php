<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
    
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.extension.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/swiper.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900|Roboto:400,500,700,900" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.ico" />
  	<title>Cranius - Home</title>
</head>
<body>

    <!-- LOADER -->
    <div id="loader-wrapper"></div>

    <!-- HEADER -->
    <header class="type-3">
        <div class="header-wrapper">
            <a id="logo" href="index.php"><img src="img/assets/logo.png" alt="Logo Verde de Cranius" title="Logo Verde de Cranius en Header" /></a>
            <div class="hamburger-icon">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>

            <div class="follow style-1">
                <a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
                <a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                <a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                
            </div>
        </div>
        <div class="navigation-wrapper">
            <nav>
                <ul>
                    <li>
                        <a href="index.php">Inicio</a>
                       <!--  <div class="toggle-menu"></div> -->
                       
                    </li>
                    <li>
                        <a href="metodologia.php">Metodolog&iacute;a</a>
                   <!--      <div class="toggle-menu"></div> -->
                
                    </li>

                    <li><a href="equipo.php">Equipo</a></li>
                    
                    <li><a href="servicios.php">Servicios</a></li>
                    <!-- <li>
                        <a href="portfolio.php">Portafolio</a>
                        <div class="toggle-menu"></div>
                       
                    </li> -->
                  <!--   <li>
                        <a href="blog1.html">Blog</a>
                        <div class="toggle-menu"></div>
                        
                    </li> -->
                    <li>
                        <a href="contact.php">Contacto</a>
                        <!-- <div class="toggle-menu"></div> -->
                        
                    </li>

                </ul>
            </nav>
        </div>
    </header>

    <div id="content-block">



        <div class="swiper-entry slider-5">
            <div class="swiper-container" data-slides-per-view="2" data-initialslide="1" data-center="1" data-speed="1000">
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-wrapper">
                    <div class="swiper-slide"> 
                        <div class="align full-screen-height">
                            <div class="slider-5-entry">
                                <div class="icon"></div>
                                <div class="icon2"></div>
                                <div class="icon3"></div>
                                <div class="icon4"></div>
                                <div class="sl-wrapper">
                                    <div class="sl">Equipo</div>
                                </div>
                            <div class="preview">
                                    <span class="text-mask logo-mobile" style="background-image: url(img/assets/slide-1-home.png);"></span>
                                </div>
                                <div class="title-wrapper">
                                   
                                    <div class="h3 title" style="color: #005a38;"><span class="ht-1"><a href="#">Somos una agencia  digital que utiliza  inteligencia algorítmica para crear contenido.</a></span></div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide"> 
                        <div class="align full-screen-height">
                            <div class="slider-5-entry">
                                <div class="icon"></div>
                                <div class="icon2"></div>
                                <div class="icon3"></div>
                                <div class="icon4"></div>
                                <div class="sl-wrapper">
                                    <div class="sl">Servicios</div>
                                </div>
                                <div class="preview">
                                    <span class="">
                                        <img class="text-mask bombillo-mobile" style="background-image: url(img/assets/bombillo.png);">
                                    </span>
                                </div>
                                <div class="title-wrapper">
                    
                                    <div class="h3 title" style="color: #005a38;"><span class="ht-1"><a href="#">Hacemos <br> contenidos <br> inteligentes.</a></span></div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="swiper-slide"> 
                        <div class="align full-screen-height">
                            <div class="slider-5-entry">
                               <div class="icon"></div>
                                <div class="icon2"></div>
                                <div class="icon3"></div>
                                <div class="icon4"></div>
                                <div class="sl-wrapper">
                                    <div class="sl">Metodolog&iacute;a</div>
                                </div>
                                <div class="preview">
                                    <span class="text-mask rubik-mobile" style="background-image: url(img/assets/slide-3-home.png);"></span>
                                </div>
                                <div class="title-wrapper">
                            
                                    <div class="h3 title" style="color: #005a38;"><span class="ht-1"><a href="#">Nuestra metodología  es un juego entre  matemáticas,  creatividad, y sentido común.</a></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
                <div class="swiper-pagination relative-pagination visible-xs visible-sm"></div>
                <div class="swiper-pager-wrapper">
                    <div class="align full-screen-height">
                        <div class="swiper-pager-container">
                            <div class="swiper-pager-content">
                                <div class="swiper-pager">
                                    <div class="swiper-pager-current">01</div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- FOOTER -->
        <footer>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-xs-text-center col-md-text-left">
                        <div class="copyright sa small">Cranius © 2017 All rights reserved.<a href="https://themeforest.net/user/unionagency" target="_blank"><b></b></a></div>  
                        <div class="col-xs-b15 visible-xs visible-sm"></div>
                    </div>
                    <div class="col-md-6 col-xs-text-center col-md-text-right">
                        <div class="follow style-1">
                            <a class="entry" href="https://www.instagram.com/SomosCranius" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a class="entry" href="https://www.facebook.com/somoscranius" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a class="entry" href="https://twitter.com/SomosCranius" target="_blank"><i class="fa fa-twitter"></i></a>
  
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <div class="popup-wrapper">
        <div class="bg-layer"></div>

        <div class="popup-content" data-rel="1">
            <div class="layer-close"></div>
            <div class="popup-container size-1">
                <div class="popup-align">
                    <h3 class="h3 text-center">Log in</h3>
                    <div class="empty-space col-xs-b30"></div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" value="" type="text" placeholder="Your email" />
                        <span></span>
                    </div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" type="password" value="" placeholder="Enter password" />
                        <span></span>
                    </div>
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-b10 col-sm-b0">
                            <div class="empty-space col-sm-b5"></div>
                            <a class="simple-link open-popup" data-rel="3">Forgot password?</a>
                            <div class="empty-space col-xs-b5"></div>
                            <a class="simple-link open-popup" data-rel="2">Register now</a>
                        </div>
                        <div class="col-sm-6 text-right">
                            <div class="button">submit<input type="submit"/></div>  
                        </div>
                    </div>
                </div>
                <div class="button-close"></div>
            </div>
        </div>

        <div class="popup-content" data-rel="2">
            <div class="layer-close"></div>
            <div class="popup-container size-1">
                <div class="popup-align">
                    <h3 class="h3 text-center">Register</h3>
                    <div class="empty-space col-xs-b30"></div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" value="" type="text" placeholder="Your name" />
                        <span></span>
                    </div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" value="" type="text" placeholder="Your email" />
                        <span></span>
                    </div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" type="password" value="" placeholder="Enter password" />
                        <span></span>
                    </div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" type="password" value="" placeholder="Repeat password" />
                        <span></span>
                    </div>
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-b10 col-sm-b0">
                            <div>
                                <label class="sc">
                                    <input type="checkbox"><span>I agree with <a href="#">terms of use</a></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6 text-right">
                            <div class="button">register<input type="submit"/></div>  
                        </div>
                    </div>
                </div>
                <div class="button-close"></div>
            </div>
        </div>

        <div class="popup-content" data-rel="3">
            <div class="layer-close"></div>
            <div class="popup-container size-1">
                <div class="popup-align">
                    <h3 class="h3 text-center">Reset your password</h3>
                    <div class="empty-space col-xs-b30"></div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" value="" type="text" placeholder="Your email" />
                        <span></span>
                    </div>
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <div class="text-center">
                        <div class="button">reset<input type="submit"/></div> 
                    </div>
                </div>
                <div class="button-close"></div>
            </div>
        </div>

    </div>

    <div class="phone-marker visible-xs"></div><div class="tablet-marker visible-sm"></div>

    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/swiper.jquery.min.js"></script>
    <script src="js/global.js"></script>

</body>
</html>
