<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
    
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.extension.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/swiper.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900|Roboto:400,500,700,900" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.ico" />
    <title>Cranius - Metodologia</title>
</head>
<body>

    <!-- LOADER -->
    <div id="loader-wrapper"></div>

    <!-- HEADER -->
    <header class="type-3 transparent">
        <div class="header-wrapper">
            <a id="logo" href="index.php"><img src="img/assets/logo-blanco.png" title="Logo Metodologia Blanco" alt="Logo Metodologia Blanco" /></a>
            <div class="hamburger-icon">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div class="follow style-1">
                <a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
                <a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                <a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                
            </div>
        </div>
        
        <div class="navigation-wrapper">
            <nav>
                <ul>
                    <li>
                        <a href="index.php">Inicio</a>
                        <!--         <div class="toggle-menu"></div> -->

                    </li>
                    <li>
                        <a href="metodologia.php">Metodolog&iacute;a</a>
                        <!--      <div class="toggle-menu"></div> -->

                    </li>

                    <li><a href="equipo.php">Equipo</a></li>
                    
                    <li><a href="servicios.php">Servicios</a></li>
                    <!-- <li>
                        <a href="portfolio.php">Portafolio</a>
                        <div class="toggle-menu"></div>
                       
                    </li> -->
                  <!--   <li>
                        <a href="blog1.html">Blog</a>
                        <div class="toggle-menu"></div>
                        
                    </li> -->
                    <li>
                        <a href="contact.php">Contacto</a>
                        <!--  <div class="toggle-menu"></div> -->
                        
                    </li>

                </ul>
            </nav>
        </div>
    </header>

    <div id="content-block">

        <div class="fixed-background" style="background-image: url(img/assets/banner-metodologia.png);">
            <div class="banner-shortcode">
                <div class="banner-frame border-image" style="border-image-source: url(img/assets/frame-rojo.jpg);"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="align">
                                <h1 class="montserrat-regular texto-blanco">Nuestra <br> <p class="roboto-black texto-blanco">Metodología</p></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="empty-space col-xs-b45 col-sm-b90"></div>

            <div class="row">
                <div class="col-xs-12 col-sm-5">
                    <a href="#" class="client-logo mouseover-2">
                        <img src="img/assets/cuadro-1-metodologia.png" alt="Logo Metodologia 1" />
                    </a>
                </div>

                <div class="col-xs-12 col-sm-2">
                    <a href="#" class="client-logo mouseover-2">
                        <img class="flecha-right-mobile" src="img/assets/flecha-right.png" alt="Logo metodologia Flecha" />
                    </a>
                </div>


                <div class="col-xs-12 col-sm-5">
                    <a href="#" class="client-logo mouseover-2"><img src="img/assets/cuadro-2-metodologia.png" alt="Logo Metodologia 2" /></a>
                </div>

                
                <div class="row col-xs-12 col-sm-12 mt-3 mb-3">


                    <div class="col-xs-12 col-sm-7">
                        <a href="#" class="client-logo mouseover-2"><img src="img/assets/cuadro-3-metodologia.png" alt="Logo Metodologia 3" /></a>
                    </div>

                    <div class="col-xs-12 col-sm-5">
                        <a href="#" class="client-logo mouseover-2"><img src="img/assets/cuadro-4-metodologia.png" alt="Logo Metodologia 4" /></a>
                    </div>
                </div>


                <div class="container-fluid">
                    <div class="row">

                    <div class="col-xs-12 col-sm-3">
                        <a href="#" class="client-logo mouseover-2"><img src="img/assets/cuadro-5-metodologia.png" alt="Logo Metodologia 5" /></a>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <a href="#" class="client-logo mouseover-2"><img src="img/assets/cuadro-6-metodologia.png" alt="Logo Metodologia 6" /></a>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <a href="#" class="client-logo mouseover-2"><img src="img/assets/cuadro-7-metodologia.png" alt="Logo Metodologia 7" /></a>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <a href="#" class="client-logo mouseover-2"><img src="img/assets/cuadro-8-metodologia.png" alt="Logo Metodologia 8" /></a>
                    </div>
                </div>
            </div>



        </div>
    </div>

    <div class="empty-space col-xs-b60 col-sm-b120"></div>

    <div class="empty-space col-xs-b45 col-sm-b90"></div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <article class="sa">
                    <h3>Algoritmo de grupo</h3>

                </article>
                <div class="empty-space col-xs-b25 col-sm-b50"></div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">

            <div class="col-xs-12 col-sm-3">
                <a href="#" class="client-logo mouseover-2"><img src="img/assets/G-metodologia.png" alt="Letra G" /></a>
                <div class="description align-center">
                    <h6 class="roboto-black">GROUPING</h6>

                </div>
            </div>

            <div class="col-xs-12 col-sm-3">
                <a href="#" class="client-logo mouseover-2"><img src="img/assets/A-metodologia.png" alt="Letra A" /></a>
                <div class="description align-center">
                    <h6 class="roboto-black">AUDIENCE</h6>

                </div>
            </div>

             <div class="col-xs-12 col-sm-3">
                <a href="#" class="client-logo mouseover-2"><img src="img/assets/I-metodologia.png" alt="Letra I" /></a>
                 <div class="description align-center">
                    <h6 class="roboto-black">INTELLIGENCE</h6>

                </div>
            </div>

             <div class="col-xs-12 col-sm-3">
                <a href="#" class="client-logo mouseover-2"><img src="img/assets/A2-metodologia.png" alt="Letra A" /></a>
                <div class="description align-center">
                    <h6 class="roboto-black">ALGORITHYM</h6>

                </div>
            </div>

        
        </div>


        <div class="empty-space col-xs-b45 col-sm-b90"></div>

        <div class="swiper-slide">
            <div class="row vertical-aligned-columns">
                <div class="col-sm-7 col-sm-push-5 col-xs-b30 col-sm-b0">
                    <div class="thumbnail-shortcode-6">
                        <div class="content">
                            <div class="layer-1 border border-image" style="border-image-source: url(img/assets/frame-rojo.jpg);"></div>
                            <div class="layer-2 background" style="background-image: url(img/assets/computer.png);"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-pull-7">
                    <div class="sa">

                        <p class="roboto-regular texto-gris">GAIA © Nuestra herramienta que descubre cuantos subtargets se esconden en la audiencia.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="empty-space col-xs-b45 col-sm-b90"></div>
        <div class="swiper-entry">
            <div class="swiper-button-prev visible-lg"></div>
            <div class="swiper-button-next visible-lg"></div>
            <div class="swiper-container" data-space="30">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="row vertical-aligned-columns">
                            <div class="col-sm-7  col-xs-b30 col-sm-b0">
                                <div class="thumbnail-shortcode-6">
                                    <div class="content">
                                        <div class="layer-1 border border-image" style="border-image-source: url(img/assets/frame-rojo.jpg);"></div>
                                        <div class="layer-2 background" style="background-image: url(img/assets/sublime-metodologia.png);"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="sa">
                                    <h3 class="roboto-black texto-azul">Usamos data accionable</h3>

                                    <p class="roboto-regular texto-gris">La primera agencia digital que utiliza algoritmos propios para crear y distribuir contenido.    

                                    Con una metodología integral, combinamos estrategia, análisis y creatividad para llevar el  mensaje correcto a la audiencia correcta y así maximizar el resultado.</p>


                                </div>
                            </div>
                        </div>



                    </div>

                    <div class="swiper-slide">
                        <div class="row vertical-aligned-columns">
                            <div class="col-sm-7 col-sm-push-5 col-xs-b30 col-sm-b0">
                                <div class="thumbnail-shortcode-6">
                                    <div class="content">
                                        <div class="layer-1 border border-image" style="border-image-source: url(img/assets/frame-rojo.jpg);"></div>
                                        <div class="layer-2 background" style="background-image: url(img/assets/banner-2-abajo.png);"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-pull-7">
                                <div class="sa">
                                    <h3 class="roboto-black texto-azul">Competir por la atención del consumidor es nuestra obsesión</h3>
                                    <p class="roboto-regular texto-gris">La primera agencia digital que utiliza algoritmos propios para crear y distribuir contenido.    

                                    Con una metodología integral, combinamos estrategia, análisis y creatividad para llevar el  mensaje correcto a la audiencia correcta y así maximizar el resultado.</p>


                                </div>
                            </div>
                        </div>



                    </div>


                    <div class="swiper-slide">
                        <div class="row vertical-aligned-columns">
                            <div class="col-sm-7 col-xs-b30 col-sm-b0">
                                <div class="thumbnail-shortcode-6">
                                    <div class="content">
                                        <div class="layer-1 border border-image" style="border-image-source: url(img/assets/frame-rojo.jpg);"></div>
                                        <div class="layer-2 background" style="background-image: url(img/assets/banner-3-abajo.png);"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="sa">
                                    <h3 class="roboto-black texto-azul">ROI Social</h3>
                                    <p class="roboto-regular texto-gris">Para impulsar los resultados de negocio a través de las 
                                    estrategias de Social Media, nuestros algoritmos permiten 
                                    una optimización de la inversión, obteniendo un mejor 
                                    desempeño de los KPI’s.</p>


                                </div>
                            </div>
                        </div>



                    </div>

                    <div class="swiper-pagination relative-pagination hidden-lg"></div>
                </div>
            </div>
        </div>

    </div>





    <!-- FOOTER -->
    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-xs-text-center col-md-text-left">
                    <div class="copyright sa small">Cranius © 2017 All rights reserved.<a href="https://themeforest.net/user/unionagency" target="_blank"><b></b></a></div>  
                    <div class="col-xs-b15 visible-xs visible-sm"></div>
                </div>
                <div class="col-md-6 col-xs-text-center col-md-text-right">
                    <div class="follow style-1">
                        <a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
                        <a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>

                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<div class="popup-wrapper">
    <div class="bg-layer"></div>

    <div class="popup-content" data-rel="1">
        <div class="layer-close"></div>
        <div class="popup-container size-1">
            <div class="popup-align">
                <h3 class="h3 text-center">Log in</h3>
                <div class="empty-space col-xs-b30"></div>
                <div class="simple-input-wrapper">
                    <input class="simple-input" value="" type="text" placeholder="Your email" />
                    <span></span>
                </div>
                <div class="simple-input-wrapper">
                    <input class="simple-input" type="password" value="" placeholder="Enter password" />
                    <span></span>
                </div>
                <div class="empty-space col-xs-b10 col-sm-b20"></div>
                <div class="row">
                    <div class="col-sm-6 col-xs-b10 col-sm-b0">
                        <div class="empty-space col-sm-b5"></div>
                        <a class="simple-link open-popup" data-rel="3">Forgot password?</a>
                        <div class="empty-space col-xs-b5"></div>
                        <a class="simple-link open-popup" data-rel="2">Register now</a>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="button">submit<input type="submit"/></div>  
                    </div>
                </div>
            </div>
            <div class="button-close"></div>
        </div>
    </div>

    <div class="popup-content" data-rel="2">
        <div class="layer-close"></div>
        <div class="popup-container size-1">
            <div class="popup-align">
                <h3 class="h3 text-center">Register</h3>
                <div class="empty-space col-xs-b30"></div>
                <div class="simple-input-wrapper">
                    <input class="simple-input" value="" type="text" placeholder="Your name" />
                    <span></span>
                </div>
                <div class="simple-input-wrapper">
                    <input class="simple-input" value="" type="text" placeholder="Your email" />
                    <span></span>
                </div>
                <div class="simple-input-wrapper">
                    <input class="simple-input" type="password" value="" placeholder="Enter password" />
                    <span></span>
                </div>
                <div class="simple-input-wrapper">
                    <input class="simple-input" type="password" value="" placeholder="Repeat password" />
                    <span></span>
                </div>
                <div class="empty-space col-xs-b10 col-sm-b20"></div>
                <div class="row">
                    <div class="col-sm-6 col-xs-b10 col-sm-b0">
                        <div>
                            <label class="sc">
                                <input type="checkbox"><span>I agree with <a href="#">terms of use</a></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="button">register<input type="submit"/></div>  
                    </div>
                </div>
            </div>
            <div class="button-close"></div>
        </div>
    </div>

    <div class="popup-content" data-rel="3">
        <div class="layer-close"></div>
        <div class="popup-container size-1">
            <div class="popup-align">
                <h3 class="h3 text-center">Reset your password</h3>
                <div class="empty-space col-xs-b30"></div>
                <div class="simple-input-wrapper">
                    <input class="simple-input" value="" type="text" placeholder="Your email" />
                    <span></span>
                </div>
                <div class="empty-space col-xs-b10 col-sm-b20"></div>
                <div class="text-center">
                    <div class="button">reset<input type="submit"/></div> 
                </div>
            </div>
            <div class="button-close"></div>
        </div>
    </div>

</div>

<div class="phone-marker visible-xs"></div><div class="tablet-marker visible-sm"></div>

<script src="js/jquery-2.2.4.min.js"></script>
<script src="js/swiper.jquery.min.js"></script>
<script src="js/global.js"></script>

<script src="js/isotope.pkgd.min.js"></script>

</body>
</html>
