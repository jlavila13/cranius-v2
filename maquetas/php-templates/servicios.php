<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.extension.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/swiper.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900|Roboto:400,500,700,900" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.ico" />
    <title>Cranius - Servicios</title>
</head>
<body>

    <!-- LOADER -->
    <div id="loader-wrapper"></div>

    <!-- HEADER -->

    <header class="type-3 transparent">
        <div class="header-wrapper">
            <a id="logo" href="index.php"><img src="img/assets/logo-blanco.png" alt="Logo Blanc Cranius" /></a>
            <div class="hamburger-icon">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div class="follow style-1">
                <a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
                <a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                <a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                
            </div>
        </div>
       <div class="navigation-wrapper">
            <nav>
                <ul>
                    <li>
                        <a href="index.php">Inicio</a>
                       <!--  <div class="toggle-menu"></div> -->
                       
                    </li>
                    <li>
                        <a href="metodologia.php">Metodolog&iacute;a</a>
                        <!-- <div class="toggle-menu"></div> -->
                
                    </li>

                    <li><a href="equipo.php">Equipo</a></li>
                    
                    <li><a href="servicios.php">Servicios</a></li>
                    <!-- <li>
                        <a href="portfolio.php">Portafolio</a>
                        <div class="toggle-menu"></div>
                       
                    </li> -->
                  <!--   <li>
                        <a href="blog1.html">Blog</a>
                        <div class="toggle-menu"></div>
                        
                    </li> -->
                    <li>
                        <a href="contact.php">Contacto</a>
                        <!-- <div class="toggle-menu"></div> -->
                        
                    </li>

                </ul>
            </nav>
        </div>
    </header>

    <div id="content-block">

        <div class="fixed-background" style="background-image: url(img/assets/banner-servicios.png);">
            <div class="banner-shortcode">
                <div class="banner-frame border-image" style="border-image-source: url(img/assets/frame-rojo.jpg);"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="align">
                                <h1 class="montserrat-regular texto-blanco">Nuestros <p class="roboto-black">Servicios</p></h1>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="empty-space col-xs-b60 col-sm-b120"></div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <article class="sa">
                        <h3 class="roboto-bold texto-azul">Hacemos contenidos inteligentes</h3>
                        <p class="roboto-regular texto-gris">En Cranius creamos Contenidos Inteligentes con la ayuda de inteligencia algorítmica, que permite a través de datos, dirigir, canalizar e incrementar la personalización de contenido en las redes sociales de forma eficiente</p>
                    </article>
                    <div class="empty-space col-xs-b25 col-sm-b50"></div>
                </div>
            </div>
            <div class="empty-space col-xs-b45 col-sm-b90"></div>
            <div class="services-shortcode-1">
                <div class="preview-wrapper">
                    <div class="icon"></div>
                    <div class="icon2"></div>
                    <div class="icon3"></div>
                    <div class="icon4"></div>
                    <div class="preview">
                        <span class="text-mask" style="background-image: url(img/assets/fichas-servicios.png);"></span>
                    </div>
                </div>
                <div class="content">
                    <div class="align">
                        <div class="sl roboto-regular texto-rojo">Estrategia</div>
                        <div class="sa">
                            <h4 class="roboto-black texto-azul">Pilares</h4>
                            <p class="roboto-regular texto-gris">Sabemos que el entorno digital es complejo y cambiante, por eso nuestra metodología consiste en combinar herramientas propias para crear una visión de marca relevante y provocadora, a través de un profundo entendimiento del comportamiento de sus consumidores en los espacios digitales.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="services-shortcode-1 style-1">
                <div class="preview-wrapper">
                    <div class="icon"></div>
                    <div class="icon2"></div>
                    <div class="icon3"></div>
                    <div class="icon4"></div>
                    <div class="preview">
                        <span class="text-mask" style="background-image: url(img/assets/computer.png);"></span>
                    </div>
                </div>
                <div class="content">
                    <div class="align">
                        <div class="sl roboto-regular texto-rojo"> Hipersegmentación </div>
                        <div class="sa">
                            <h4 class="roboto-black texto-azul"> Clusterización de Audiencias </h4>
                            <p class="roboto-regular texto-gris">Ofrecemos una metodología 100% algorítmica, que gracias a nuestra herramienta GAIA reconoce dentro de las audiencias digitales, sub-targets específicos a los que podemos dirigir mensajes de manera personalizada.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="services-shortcode-1">
                <div class="preview-wrapper">
                    <div class="icon"></div>
                    <div class="icon2"></div>
                    <div class="icon3"></div>
                    <div class="icon4"></div>
                    <div class="preview">
                        <span class="text-mask" style="background-image: url(img/assets/lupa-servicios.png);"></span>
                    </div>
                </div>
                <div class="content">
                    <div class="align">
                        <div class="sl roboto-regular texto-rojo"> Machine Learning - AI </div>
                        <div class="sa">
                            <h4 class="roboto-black texto-azul"> Semiótica de Clústeres </h4>
                            <p class="roboto-regular texto-gris">Hoy sabemos más del consumidor de lo que jamás sabíamos. Nuestra metodología combina el reconocimiento del ecosistema digital y el análisis de comportamiento del contenido social para entender los aspectos culturales, simbólicos y sociales que se transmiten en las piezas de contenido y así aprender, conectar y persuadir con contenidos inteligentes.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="services-shortcode-1 style-1">
                <div class="preview-wrapper">
                    <div class="icon"></div>
                    <div class="icon2"></div>
                    <div class="icon3"></div>
                    <div class="icon4"></div>
                    <div class="preview">
                        <span class="text-mask" style="background-image: url(img/assets/bombillo-servicios.png);"></span>
                    </div>
                </div>


                <div class="content">
                    <div class="align">
                        <div class="sl roboto-regular texto-rojo"> Creatividad </div>
                        <div class="sa">
                            <h4 class="roboto-black texto-azul"> Contenidos Inteligentes </h4>
                            <p class="roboto-regular texto-gris">Nuestras historias buscan la manera de acercar la verdad de las marcas a la verdad de los consumidores, y lo hacemos guiados por los datos que obtenemos a través de las miles de interacciones diarias que los usuarios tienen en las plataformas sociales.</p>
                        </div>
                    </div>
                </div>
            </div>



            <div class="services-shortcode-1">
                <div class="preview-wrapper">
                    <div class="icon"></div>
                    <div class="icon2"></div>
                    <div class="icon3"></div>
                    <div class="icon4"></div>
                    <div class="preview">
                        <span class="text-mask" style="background-image: url(img/assets/celu-servicios.png);"></span>
                    </div>
                </div>

                <div class="content">
                    <div class="align">
                        <div class="sl roboto-regular texto-rojo"> Formato y Entrega de Contenido </div>
                        <div class="sa">
                            <h4 class="roboto-black texto-azul"> Distribución de Contenido </h4>
                            <p class="roboto-regular texto-gris">La distribución del contenido en las plataformas digitales adecuadas es esencial para el éxito. Medios propios, pagados, y ganados se planifican juntos aunque se ejecuten por separado. Buscamos que cada pieza de contenido llegue a las audiencias correctas, a través de la plataforma, formato y momento ideal.</p>
                        </div>
                    </div>
                </div>
            </div>



             <div class="services-shortcode-1 style-1">
                <div class="preview-wrapper">
                    <div class="icon"></div>
                    <div class="icon2"></div>
                    <div class="icon3"></div>
                    <div class="icon4"></div>
                    <div class="preview">
                        <span class="text-mask" style="background-image: url(img/assets/tablet-servicios.png);"></span>
                    </div>
                </div>


                <div class="content">
                    <div class="align">
                        <div class="sl roboto-regular texto-rojo"> Ánalisis de Resultados </div>
                        <div class="sa">
                            <h4 class="roboto-black texto-azul"> Métricas y Analíticas </h4>
                            <p class="roboto-regular texto-gris">Estamos constantemente midiendo el desempeño de nuestras propuestas. El objetivo es optimizar el rendimiento de la inversión para mejorar los resultados.</p>
                        </div>
                    </div>
                </div>
            </div>



           <!--  <div class="row">
                <div class="col-md-12 text-center">
                    <article class="sa">
                        <h3>What we can offer</h3>
                        <p>Nemo enim ipsam voluptatem, quia voluptas sit</p>
                    </article>
                    <div class="empty-space col-xs-b25 col-sm-b50"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="services-shortcode-2">
                        <div class="icon"><img src="img/icon-2.png" alt="" /></div>
                        <div class="sl">Design / Branding</div>
                        <div class="content">
                            <div class="sa middle">
                                <h6>Development process</h6>
                                <p>Lorem ipsum dolor sit amet, amit consecte tur adipiscing elit, sed do eiusmod tempor incididunt ut sit labore et dolore magna.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="services-shortcode-2">
                        <div class="icon"><img src="img/icon-3.png" alt="" /></div>
                        <div class="sl">Design / Branding</div>
                        <div class="content">
                            <div class="sa middle">
                                <h6>Development process</h6>
                                <p>Lorem ipsum dolor sit amet, amit consecte tur adipiscing elit, sed do eiusmod tempor incididunt ut sit labore et dolore magna.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="services-shortcode-2">
                        <div class="icon"><img src="img/icon-4.png" alt="" /></div>
                        <div class="sl">Design / Branding</div>
                        <div class="content">
                            <div class="sa middle">
                                <h6>Development process</h6>
                                <p>Lorem ipsum dolor sit amet, amit consecte tur adipiscing elit, sed do eiusmod tempor incididunt ut sit labore et dolore magna.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
      
        </div>

        <!-- FOOTER -->
        <footer>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-xs-text-center col-md-text-left">
                        <div class="copyright sa small">Cranius © 2017 All rights reserved.<a href="https://themeforest.net/user/unionagency" target="_blank"><b></b></a></div>  
                        <div class="col-xs-b15 visible-xs visible-sm"></div>
                    </div>
                    <div class="col-md-6 col-xs-text-center col-md-text-right">
                        <div class="follow style-1">
                            <a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
  
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <div class="popup-wrapper">
        <div class="bg-layer"></div>

        <div class="popup-content" data-rel="1">
            <div class="layer-close"></div>
            <div class="popup-container size-1">
                <div class="popup-align">
                    <h3 class="h3 text-center">Log in</h3>
                    <div class="empty-space col-xs-b30"></div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" value="" type="text" placeholder="Your email" />
                        <span></span>
                    </div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" type="password" value="" placeholder="Enter password" />
                        <span></span>
                    </div>
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-b10 col-sm-b0">
                            <div class="empty-space col-sm-b5"></div>
                            <a class="simple-link open-popup" data-rel="3">Forgot password?</a>
                            <div class="empty-space col-xs-b5"></div>
                            <a class="simple-link open-popup" data-rel="2">Register now</a>
                        </div>
                        <div class="col-sm-6 text-right">
                            <div class="button">submit<input type="submit"/></div>  
                        </div>
                    </div>
                </div>
                <div class="button-close"></div>
            </div>
        </div>

        <div class="popup-content" data-rel="2">
            <div class="layer-close"></div>
            <div class="popup-container size-1">
                <div class="popup-align">
                    <h3 class="h3 text-center">Register</h3>
                    <div class="empty-space col-xs-b30"></div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" value="" type="text" placeholder="Your name" />
                        <span></span>
                    </div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" value="" type="text" placeholder="Your email" />
                        <span></span>
                    </div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" type="password" value="" placeholder="Enter password" />
                        <span></span>
                    </div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" type="password" value="" placeholder="Repeat password" />
                        <span></span>
                    </div>
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-b10 col-sm-b0">
                            <div>
                                <label class="sc">
                                    <input type="checkbox"><span>I agree with <a href="#">terms of use</a></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6 text-right">
                            <div class="button">register<input type="submit"/></div>  
                        </div>
                    </div>
                </div>
                <div class="button-close"></div>
            </div>
        </div>

        <div class="popup-content" data-rel="3">
            <div class="layer-close"></div>
            <div class="popup-container size-1">
                <div class="popup-align">
                    <h3 class="h3 text-center">Reset your password</h3>
                    <div class="empty-space col-xs-b30"></div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" value="" type="text" placeholder="Your email" />
                        <span></span>
                    </div>
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <div class="text-center">
                        <div class="button">reset<input type="submit"/></div> 
                    </div>
                </div>
                <div class="button-close"></div>
            </div>
        </div>

    </div>

    <div class="phone-marker visible-xs"></div><div class="tablet-marker visible-sm"></div>

    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/swiper.jquery.min.js"></script>
    <script src="js/global.js"></script>

    <script src="js/isotope.pkgd.min.js"></script>

</body>
</html>
