<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.extension.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/swiper.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900|Roboto:400,500,700,900" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.ico" />
    <title>Cranius - Contacto</title>
</head>
<body>

    <!-- LOADER -->
    <div id="loader-wrapper"></div>

    <!-- HEADER -->
    <header class="type-3">
        <div class="header-wrapper">
            <a id="logo" href="index.php"><img src="img/assets/logo.png" alt="Logo Verde de Cranius" title="Logo Verde de Cranius en Header" /></a>
            <div class="hamburger-icon">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>

            <div class="follow style-1">
                <a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
                <a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                <a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                
            </div>
        </div>
        <div class="navigation-wrapper">
            <nav>
                <ul>
                    <li>
                        <a href="index.php">Inicio</a>
                       <!--  <div class="toggle-menu"></div> -->
                       
                    </li>
                    <li>
                        <a href="metodologia.php">Metodolog&iacute;a</a>
                   <!--      <div class="toggle-menu"></div> -->
                
                    </li>

                    <li><a href="equipo.php">Equipo</a></li>
                    
                    <li><a href="servicios.php">Servicios</a></li>
                    <!-- <li>
                        <a href="portfolio.php">Portafolio</a>
                        <div class="toggle-menu"></div>
                       
                    </li> -->
                  <!--   <li>
                        <a href="blog1.html">Blog</a>
                        <div class="toggle-menu"></div>
                        
                    </li> -->
                    <li>
                        <a href="contact.php">Contacto</a>
                        <!-- <div class="toggle-menu"></div> -->
                        
                    </li>

                </ul>
            </nav>
        </div>
    </header>

    <div id="content-block">

        <div class="container-fluid wide">
            <div class="row">
                <div class="col-md-6 col-lg-4 col-lg-offset-1">
                    <div class="cell-view full-screen-height text-center">
                        <div class="empty-space col-xs-b30"></div>
                        <article class="sa">
                            <h3 class="roboto-black texto-verde align-center">¿Curioso?</h3>
                           
                        </article>
                        <div class="empty-space col-xs-b25 col-sm-b50"></div>
                        <div class="sa col-xs-b10">
                            <b class="roboto-black texto-azul">Tel.:</b> <a href="tel:+34911836303" class="roboto-regular texto-azul">+34 91 183 63 03</a><br/>
                        </div>
                        <div class="sa col-xs-b10">
                            <b class="roboto-black texto-azul">Direcci&oacute;n:</b> <span class="roboto-regular texto-azul">Calle Hortaleza, 108 (DCollab) 28004 Madrid </span><br/>
                        </div>
                        <div class="sa col-xs-b10">
                            <b class="roboto-black texto-azul">Email:</b> <a href="andrehyna@cranius.es" class="roboto-regular texto-azul"> andrehyna@cranius.es</a>
                        </div>
                        <div class="empty-space col-xs-b30"></div>
                        <div class="follow style-1">
                            <a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                          
                        </div>
                        <div class="empty-space col-xs-b25 col-sm-b50"></div>
                        <form class="contact-form">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="simple-input-wrapper">
                                        <input class="simple-input texto-gris2 roboto-regular" value="" type="text" placeholder="Nombre" name="nombre" />
                                        <span></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="simple-input-wrapper">
                                        <input class="simple-input texto-gris2 roboto-regular" value="" type="text" placeholder="E-mail" name="email" />
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                          <!--   <div class="simple-input-wrapper">
                                <input class="simple-input" value="" type="text" placeholder="Su mensaje" name="subject" />
                                <span></span>
                            </div> -->
                            <div class="simple-input-wrapper">
                                <textarea class="simple-input texto-gris2 roboto-regular" placeholder="Su mensaje" name="mensaje"></textarea>
                                <span></span>
                            </div>
                            <div class="empty-space col-xs-b10"></div>
                            <div class="texto-azul roboto-black align-left">Enviar<input type="submit"/></div>
                        </form>
                        <div class="empty-space col-xs-b30"></div>
                    </div>
                </div>
            </div>
    
         
            <div class="map-inst style-1" id="map-canvas" data-lat="40.425558" data-lng="-3.697371" data-zoom="19"></div>
            <div class="addresses-block hidden" data-rel="map-canvas">
                <a class="marker" data-lat="40.425558" data-lng="-3.697371" data-string="1. Dcollab Espacio de coworking..."></a>
            </div>
        </div>

        <!-- FOOTER -->
        <footer>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-xs-text-center col-md-text-left">
                        <div class="copyright sa small">Cranius © 2017 All rights reserved.<a href="https://themeforest.net/user/unionagency" target="_blank"><b></b></a></div>  
                        <div class="col-xs-b15 visible-xs visible-sm"></div>
                    </div>
                    <div class="col-md-6 col-xs-text-center col-md-text-right">
                        <div class="follow style-1">
                            <a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
  
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

   

    <div class="phone-marker visible-xs"></div><div class="tablet-marker visible-sm"></div>

    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/swiper.jquery.min.js"></script>
    <script src="js/global.js"></script>

    <!-- MAP -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpeleesrD3184KQkDlQq8kWY9LoF29x1M"
    type="text/javascript"></script>
    <script src="js/map.js"></script>


    <!-- CONTACT -->
    <script src="js/contact.form.js"></script>

</body>
</html>
