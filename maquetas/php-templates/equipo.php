<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
    
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.extension.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/swiper.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900|Roboto:400,500,700,900" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.ico" />
    <title>Cranius - Equipo</title>
</head>
<body>

    <!-- LOADER -->
    <div id="loader-wrapper"></div>

    
     <header class="type-3 transparent">
        <div class="header-wrapper">
            <a id="logo" href="index.php"><img src="img/assets/logo-blanco.png" title="Logo Metodologia Blanco" alt="Logo Metodologia Blanco" /></a>
            <div class="hamburger-icon">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div class="follow style-1">
                <a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
                <a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                <a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                
            </div>
        </div>
        
        <div class="navigation-wrapper">
            <nav>
                <ul>
                    <li>
                        <a href="index.php">Inicio</a>
                      <!--   <div class="toggle-menu"></div> -->
                       
                    </li>
                    <li>
                        <a href="metodologia.php">Metodolog&iacute;a</a>
                    <!--     <div class="toggle-menu"></div>
 -->                
                    </li>

                    <li><a href="equipo.php">Equipo</a></li>
                    
                    <li><a href="servicios.php">Servicios</a></li>
                    <!-- <li>
                        <a href="portfolio.php">Portafolio</a>
                        <div class="toggle-menu"></div>
                       
                    </li> -->
                  <!--   <li>
                        <a href="blog1.html">Blog</a>
                        <div class="toggle-menu"></div>
                        
                    </li> -->
                    <li>
                        <a href="contact.php">Contacto</a>
                        <!-- <div class="toggle-menu"></div> -->
                        
                    </li>

                </ul>
            </nav>
        </div>
    </header>

    <div id="content-block">

        <div class="fixed-background" style="background-image: url(img/assets/banner-equipo.png);">
            <div class="banner-shortcode">
                <div class="banner-frame border-image" style="border-image-source: url(img/assets/frame-rojo.jpg);"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="align">
                                <h1 class="montserrat-regular texto-blanco">CONOCE A <br> <p class="roboto-black">NUESTRO TEAM</p></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="empty-space col-xs-b60 col-sm-b120"></div>
            <div class="row vertical-aligned-columns">
                <div class="col-sm-7 col-sm-push-5 col-xs-b30 col-sm-b0">
                    <div class="thumbnail-shortcode-4">
                        <div class="content">
                            <div class="layer-1 background" style="background-image: url(img/assets/binary-equipo.png);"></div>
                            <div class="layer-2 border border-image" style="border-image-source: url(img/assets/frame-rojo.jpg);"></div>
                            <div class="layer-3 background" style="background-image: url(img/assets/hola-equipo.png);"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-pull-7">
                    <div class="sa">
                        <h3>Un equipo multidisciplinario</h3>
                        <p class="roboto-regular">Liderado por un Ingeniero Doctor en Matemática, un CEO de Agencias de Medios, una Matemática Estadística, un Creativo y una Estratega, con la única aspiración de ayudar a los clientes a tomar mejores decisiones guiados por la inmensa cantidad de data disponible que se genera en las plataformas sociales.</p>

                
                    </div>
                </div>
            </div>
            <div class="empty-space col-xs-b45 col-sm-b90"></div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <article class="sa">
                        <h3 class="roboto-bold">Nuestros talentos</h3>
                    </article>
                    <div class="empty-space col-xs-b25 col-sm-b50"></div>
                </div>
            </div>
            <div class="swiper-entry">
                <div class="swiper-container" data-breakpoints="1" data-xs-slides="1" data-sm-slides="2" data-md-slides="2" data-lt-slides="3" data-slides-per-view="3" data-space="30">
                    <div class="swiper-button-prev hidden"></div>
                    <div class="swiper-button-next hidden"></div>
                    <div class="swiper-wrapper">
                        <div class="swiper-slide"> 
                            <div class="thumbnail-shortcode-5">
                                <div class="content">
                                    <div class="layer-2"><img src="img/assets/Jhon.png" alt="Jhon Da Silva" /></div>
                                </div>
                                <div class="description">
                                    <h6 class="roboto-black texto-azul">JHON DA SILVA</h6>
                                    <div class="sa small montserrat-regular texto-gris">Miembro del Consejo Asesor de Cranius.</div>
                                </div>
                                <div class="animation follow style-1">
<!--                                    <!--<a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>-->
<!--                                    <!--<a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>-->
<!--                                    <!--<a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>-->
                                   <!--  <a class="entry" href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a> -->
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide"> 
                            <div class="thumbnail-shortcode-5">
                                <div class="content">
        
                                    <div class="layer-2"><img src="img/assets/Macarena.png" alt="Macarena Estevez" /></div>
                                </div>
                                <div class="description">
                                    <h6 class="roboto-black texto-azul">MACARENA ESTEVEZ</h6>
                                    <div class="sa small montserrat-regular texto-gris">Miembro del Consejo Asesor de Cranius.</div>
                                </div>
                                <div class="animation follow style-1">
<!--                                    <!--<a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>-->
<!--                                    <!--<a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>-->
<!--                                    <!--<a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>-->
                                    <!-- <a class="entry" href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a> -->
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide"> 
                            <div class="thumbnail-shortcode-5">
                                <div class="content">
                                    
                                    <div class="layer-2"><img src="img/assets/Juan.png" alt="JUAN CARLOS" /></div>
                                </div>
                                <div class="description">
                                    <h6 class="roboto-black texto-azul">JUAN CARLOS MARTÍNEZ</h6>
                                    <div class="sa small montserrat-regular texto-gris">Fundador y CEO de Cranius</div>
                                </div>
                                <div class="animation follow style-1">
<!--                                    <!--<a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>-->
<!--                                    <!--<a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>-->
<!--                                    <!--<a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>-->
                                   <!--  <a class="entry" href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a> -->
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide"> 
                            <div class="thumbnail-shortcode-5">
                                <div class="content">
                                    
                                    <div class="layer-2"><img src="img/assets/Juan2.png" alt="JUAN ALEGRE" /></div>
                                </div>
                                <div class="description">
                                    <h6 class="roboto-black texto-azul">JUAN ALEGRE</h6>
                                    <div class="sa small montserrat-regular texto-gris">Director de Tecnología y Analytics</div>
                                </div>
                                <div class="animation follow style-1">
<!--                                    <!--<a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>-->
<!--                                    <!--<a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>-->
<!--                                    <!--<a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>-->
                                   <!--  <a class="entry" href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a> -->
                                </div>
                            </div>
                        </div>


                

                       
         
                </div>
            </div>
            <div class="empty-space col-xs-b45 col-sm-b90"></div>
        
            
            <div class="empty-space col-xs-b45 col-sm-b90"></div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <article class="sa">
                        <h3 class="roboto-bold">Con experiencia en</h3>
                        <p class="roboto-regular">Lima, Madrid, Bogotá, Caracas, Santiago de Chile, Ciudad de México</p>
                       
                    </article>
                    <div class="empty-space col-xs-b25 col-sm-b50"></div>
                </div>
            </div>
            <div class="row">

                <div class="equipo-shortcode-1">
                    <div class="preview-wrapper col-xs-12 col-sm-12">
                        <div class="icon"></div>
                        <div class="icon2"></div>
                        <div class="icon3"></div>
                        <div class="icon4"></div>
                        <div class="preview">
                            <span class="text-mask mouseover-2" style="background-image: url(img/assets/mapa-team.png);"></span>
                        </div>
                    </div>
                </div>

               
            </div>
        </div>

        <!-- FOOTER -->
       <footer>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-xs-text-center col-md-text-left">
                        <div class="copyright sa small">Cranius © 2017 All rights reserved.<a href="https://themeforest.net/user/unionagency" target="_blank"><b></b></a></div>  
                        <div class="col-xs-b15 visible-xs visible-sm"></div>
                    </div>
                    <div class="col-md-6 col-xs-text-center col-md-text-right">
                        <div class="follow style-1">
                            <a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>

                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <div class="popup-wrapper">
        <div class="bg-layer"></div>

        <div class="popup-content" data-rel="1">
            <div class="layer-close"></div>
            <div class="popup-container size-1">
                <div class="popup-align">
                    <h3 class="h3 text-center">Log in</h3>
                    <div class="empty-space col-xs-b30"></div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" value="" type="text" placeholder="Your email" />
                        <span></span>
                    </div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" type="password" value="" placeholder="Enter password" />
                        <span></span>
                    </div>
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-b10 col-sm-b0">
                            <div class="empty-space col-sm-b5"></div>
                            <a class="simple-link open-popup" data-rel="3">Forgot password?</a>
                            <div class="empty-space col-xs-b5"></div>
                            <a class="simple-link open-popup" data-rel="2">Register now</a>
                        </div>
                        <div class="col-sm-6 text-right">
                            <div class="button">submit<input type="submit"/></div>  
                        </div>
                    </div>
                </div>
                <div class="button-close"></div>
            </div>
        </div>

        <div class="popup-content" data-rel="2">
            <div class="layer-close"></div>
            <div class="popup-container size-1">
                <div class="popup-align">
                    <h3 class="h3 text-center">Register</h3>
                    <div class="empty-space col-xs-b30"></div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" value="" type="text" placeholder="Your name" />
                        <span></span>
                    </div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" value="" type="text" placeholder="Your email" />
                        <span></span>
                    </div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" type="password" value="" placeholder="Enter password" />
                        <span></span>
                    </div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" type="password" value="" placeholder="Repeat password" />
                        <span></span>
                    </div>
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-b10 col-sm-b0">
                            <div>
                                <label class="sc">
                                    <input type="checkbox"><span>I agree with <a href="#">terms of use</a></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6 text-right">
                            <div class="button">register<input type="submit"/></div>  
                        </div>
                    </div>
                </div>
                <div class="button-close"></div>
            </div>
        </div>

        <div class="popup-content" data-rel="3">
            <div class="layer-close"></div>
            <div class="popup-container size-1">
                <div class="popup-align">
                    <h3 class="h3 text-center">Reset your password</h3>
                    <div class="empty-space col-xs-b30"></div>
                    <div class="simple-input-wrapper">
                        <input class="simple-input" value="" type="text" placeholder="Your email" />
                        <span></span>
                    </div>
                    <div class="empty-space col-xs-b10 col-sm-b20"></div>
                    <div class="text-center">
                        <div class="button">reset<input type="submit"/></div> 
                    </div>
                </div>
                <div class="button-close"></div>
            </div>
        </div>

    </div>

    <div class="phone-marker visible-xs"></div><div class="tablet-marker visible-sm"></div>

    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/swiper.jquery.min.js"></script>
    <script src="js/global.js"></script>

    <script src="js/isotope.pkgd.min.js"></script>

</body>
</html>
