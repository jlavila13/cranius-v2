<?php /*Template name: servicios final*/ ?>
<?php get_header(); ?>
  <script>
    jQuery(document).ready(function(){
			jQuery('.container .services-shortcode-1:nth-child(odd)').addClass('style-1');
    });
  </script>
  <?php if(have_posts() ) : while(have_posts() ) : the_post(); ?>
  <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
  <div class="fixed-background" style="background-image: url(<?php echo $url ?>);">
      <div class="banner-shortcode">
          <div class="banner-frame border-image" style="border-image-source: url(<?php print IMAGES; ?>assets/frame-rojo.jpg);"></div>
          <div class="container">
              <div class="row">
                  <div class="col-md-12">
                      <div class="align">
                          <h1 class="montserrat-regular texto-blanco">Nuestros <p class="roboto-black">Servicios</p></h1>

                      </div>

                  </div>
              </div>
          </div>
      </div>
  </div>

  <div class="container">
      <div class="empty-space col-xs-b60 col-sm-b120"></div>
      <div class="row">
          <div class="col-md-12 text-center">
              <article class="sa">
                  <h3 class="roboto-bold texto-azul"><?php the_field('p-titulo');?></h3>
                  <p class="roboto-regular texto-gris"><?php the_content();?></p>
              </article>
              <div class="empty-space col-xs-b25 col-sm-b50"></div>
          </div>
      </div>
      <div class="empty-space col-xs-b45 col-sm-b90"></div>
      <?php if(have_rows('serv-item') ) : while(have_rows('serv-item') ) : the_row();?>
        <div class="services-shortcode-1">
            <div class="preview-wrapper">
                <div class="icon"></div>
                <div class="icon2"></div>
                <div class="icon3"></div>
                <div class="icon4"></div>
                <div class="preview">
                    <span class="text-mask" style="background-image: url(<?php the_sub_field('imagen');?>);"></span>
                </div>
            </div>
            <div class="content">
                <div class="align">
                    <div class="sl roboto-regular texto-rojo"><?php the_sub_field('campo');?></div>
                    <div class="sa">
                        <h4 class="roboto-black texto-azul"><?php the_sub_field('nombre');?></h4>
                        <p class="roboto-regular texto-gris"><?php the_sub_field('descripcion');?></p>
                    </div>
                </div>
            </div>
        </div>
      <?php endwhile; endif; ?>
  </div>

  <?php endwhile; endif;?>

<?php get_footer(); ?>
