<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
    <link rel="shortcut icon" href="<?php print IMAGES;?>favicon.png" />
  	<title><?php wp_title( '&laquo;', true, 'right' ); ?><?php bloginfo('name'); ?></title>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css">
    <meta name="theme-color" content="#00b188">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#00b188">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#00b188">
    <!-- wp header -->
    <?php wp_head();?>
    <!-- Global Site Tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106940448-1"></script>

		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments)};
			gtag('js', new Date());
			gtag('config', 'UA-106940448-1');
		</script>
   
</head>
<body>

  <!-- LOADER -->
  <!-- <div id="loader-wrapper"></div> -->

  <!-- HEADER -->
  <header class="type-3">
      <div class="header-wrapper">
          <a id="logo" href="<?php echo get_option('home'); ?>"><img src="<?php print IMAGES;?>assets/logo.png" alt="Cranius" title="Logo Verde de Cranius en Header" /></a>
          <div class="hamburger-icon fa fa-bars">
              <?php echo do_shortcode('[main-nav]'); ?>
          </div>

          <div class="follow style-1">
              <a class="entry" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
              <a class="entry" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
              <a class="entry" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
              
          </div>
      </div>
      <div class="navigation-wrapper <?php if(is_page_template('index.php')){echo 'home-menu';}elseif(is_page_template('contact.php')){echo 'home-menu';}?>">
          <?php echo do_shortcode('[main-nav]'); ?>
      </div>
  </header>
  <div id="content-block"class="<?php if(is_page_template('index.php')){echo 'top-spacer';} elseif(is_page_template('contact.php')){echo 'top-spacer';}?>">
