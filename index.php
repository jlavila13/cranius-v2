<?php /* Template name: home*/ ?>
<?php get_header(); ?>

  <div class="swiper-entry slider-5">
      <div class="swiper-container" data-slides-per-view="2" data-initialslide="1" data-center="1" data-speed="1000">
          <div class="swiper-button-prev"></div>
          <div class="swiper-button-next"></div>
          <div class="swiper-wrapper">
            <?php
              $args = array('post_type' => 'slides', 'orderby' => array( 'menu_order' => 'ASC' ));
              $servs = new WP_Query( $args );
              while($servs->have_posts() ) : $servs->the_post();
            ?>
              <div class="swiper-slide"> 
                  <div class="align full-screen-height">
                      <div class="slider-5-entry">
                          <div class="icon"></div>
                          <div class="icon2"></div>
                          <div class="icon3"></div>
                          <div class="icon4"></div>
                          <div class="sl-wrapper">
                              <div class="sl"><?php the_title(); ?></div>
                          </div>
                      <div class="preview">
                              <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                              <span class="text-mask logo-mobile img-slide" style="background-image: url(<?php echo $url ?>);"></span>
                          </div>
                          <div class="title-wrapper">
                              <div class="h3 title" style="color: #005a38;"><span class="ht-1"><a href="<?php echo get_site_url() ?>/<?php the_field('enlace');?>"><?php the_content(); ?></a></span></div>
                          </div>
                          
                      </div>
                  </div>
              </div>
            <?php endwhile; wp_reset_postdata();?>
             
          </div>
          <div class="swiper-pagination relative-pagination visible-xs visible-sm"></div>
          <div class="swiper-pager-wrapper">
              <div class="align full-screen-height">
                  <div class="swiper-pager-container">
                      <div class="swiper-pager-content">
                          <div class="swiper-pager">
                              <div class="swiper-pager-current">01</div>
                            
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

<?php get_footer(); ?>